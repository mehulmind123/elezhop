//
//  APIRequest.h
//  EdSmart
//
//  Created by mac-0007 on 08/03/16.
//  Copyright © 2016 mac-0007. All rights reserved.
//





/*===========================================================
 ===========================================================*/

//********** Server

#define AppStoreURL         @"https://itunes.apple.com/us/app/elezhop/id1267755805?ls=1&mt=8"

#define DebitCreditCardURL  @"http://176.34.158.219:81/about-us.html"




//#define BASEURL           @"http://180.211.104.102:3003/api/app/v1/"
//#define BASEURL           @"http://www.itrainacademy.in:3011/api/app/v1/"


#define BASEURL             @"http://api-dev.elezhop.com/api/app/v1/"      //..... Client server URL





//********** Request, Response Constant

#define CVersion    @"1"
#define COffset     @0
#define CLimit      @20

#define CJsonStatus             @"status_code"
#define CJsonMessage            @"message"
#define CJsonTitle              @"title"
#define CJsonData               @"data"
#define CJsonResponse           @"response"
#define CJsonNewTimestamp       @"new-timestamp"
#define CJsonNewOffset          @"new_offset"

#define CStatusZero             0
#define CStatusOne              1
#define CStatusFive             5
#define CStatusNine             9
#define CStatusTen              10
#define CStatus200              200
#define CStatus500              500
#define CStatus555              555
#define CStatus400              400
#define CStatus401              401



//********** API Tag


typedef void(^SocialUserInfoBlock)(id userInfo, NSError *error);



/*===========================================================
 ===========================================================*/



#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>


@interface APIRequest : NSObject

+ (id)request;

- (void)failureWithAPI:(NSString *)api andError:(NSError *)error andHandler:(void(^)(BOOL retry))handler;

- (NSError *)errorFromDataTask:(NSURLSessionDataTask *)task;

- (BOOL)isJSONDataValidWithResponse:(id)response;

- (BOOL)isJSONStatusValidWithResponse:(id)response;



//..... API TAG ......

#define CAPITagUserDetail                                   @"user_detail"


#define CAPITagSocialLogin                                  @"social_login"


#define CAPITagFeaturedOffer                                @"featured_offers"
#define CAPITagCategoryList                                 @"category_list"
#define CAPITagRaffle                                       @"raffle"
#define CAPITagRaffleWinnerList                             @"raffle_winner"


#define CAPITagStoreList                                    @"store_list"
#define CAPITagStoreDetail                                  @"store_detail"
#define CAPITagStoreDetailOfferList                         @"store_detail_offers"
#define CAPITagStoreDetailReviewList                        @"store_detail_reviews"
#define CAPITagUploadBill                                   @"upload_bill"


#define CAPITagGiftVoucherList                              @"gift_vouchers"
#define CAPITagGetGiftVoucher                               @"get_gift_voucher"
#define CAPITagMyBillList                                   @"my_bills"
#define CAPITagHistoryHopsEarnedRedeemedRaffle              @"history_hops_earned_redeem_raffle"
#define CAPITagContactUs                                    @"contact_us"
#define CAPITagUploadProfileImage                           @"upload_profile_image"
#define CAPITagEditProfile                                  @"edit_profile"
#define CAPITagCMS                                          @"cms"


#define CAPITagNotificationSetting                          @"notifications_on_off"
#define CAPITagNotificationList                             @"notifications_list"


#define CAPITagRegisterDeviceToken                          @"device_register"







# pragma mark
# pragma mark - Social Login

- (void)signUpWithFacebookFromViewController:(UIViewController *)vc withCompletion:(SocialUserInfoBlock)uBlock;

- (void)signUpWithGoogleFromViewController:(UIViewController *)vc withCompletion:(SocialUserInfoBlock)uBlock;

- (void)socialLoginWithSocialID:(NSString *)socialID andLoginType:(NSInteger)loginType andEmail:(NSString *)email andDateOfBirth:(NSString *)dateOfBirth andPhoneNumber:(NSString *)phoneNumber andFullName:(NSString *)fullName andProfileImage:(NSString *)profileImage completed:(void (^)(id responseObject, NSError *error))completion;






# pragma mark
# pragma mark - Home

- (void)featuredOffer:(void(^)(id resonseObject, NSError *error))completion;

- (void)categroyList:(void(^)(id resonseObject, NSError *error))completion;

- (void)raffle:(void(^)(id responseObject, NSError *error))completion;

- (void)raffleWinnerListWithRaffleID:(NSString *)raffleID andOffset:(NSInteger)offset completed:(void(^)(id responseObject, NSError *error))completion;





# pragma mark
# pragma mark - Store

- (NSURLSessionDataTask *)storeListWithCategoryID:(NSString *)categoryID andOffset:(NSInteger)offset andSearchText:(NSString *)searchTxt andSortBY:(NSInteger)sortBY completed:(void(^)(id responseObject, NSError *error))completion;

- (void)storeDetailWithStoreID:(NSString *)storeID completed:(void(^)(id responseObject, NSError *error))completion;

- (void)storeDetailOfferListWithStoreID:(NSString *)storeID andOffset:(NSInteger)offset completed:(void(^)(id responseObject, NSError *error))completion;

- (void)storeDetailReviewListWithStoreID:(NSString *)storeID andOffset:(NSInteger)offset completed:(void(^)(id responseObject, NSError *error))completion;






# pragma mark
# pragma mark - Upload Bill

- (void)uploadBill:(NSData *)imgData andStoreID:(NSString *)storeID andStoreName:(NSString *)storeName andBillAmount:(NSString *)billAmount andRate:(float)rate andReview:(NSString *)review completed:(void (^)(id responseObject, NSError *error))completion;







# pragma mark
# pragma mark - Side Menu

- (void)giftVoucherListWithOffset:(NSInteger)offset completed:(void(^)(id responseObject, NSError *error))completion;

- (void)getGiftVoucher:(NSString *)giftID completed:(void(^)(id responseObject, NSError *error))completion;

- (void)myBillsWithOffset:(NSInteger)offset completed:(void(^)(id responseObject, NSError *error))completion;

- (void)historyHopsEarnedRedeemedRaffleWithType:(NSInteger)type andOffset:(NSInteger)offset andSortBY:(NSInteger)sortBY completed:(void(^)(id responseObject, NSError *error))completion;

- (void)contactUsWithName:(NSString *)name andEmail:(NSString *)email andPhoneNumber:(NSString *)phoneNumber andDescription:(NSString *)description completed:(void(^)(id responseObject, NSError *error))completion;

- (void)uploadProfileImage:(NSData *)imgData completed:(void (^)(id responseObject, NSError *error))completion;

- (void)editProfileWithName:(NSString *)name andDateOfBirth:(NSString *)dateOfBirth andEmail:(NSString *)email andPhoneNumber:(NSString *)phoneNumber andAddress:(NSString *)address completed:(void(^)(id responseObject, NSError *error))completion;

- (void)getCMS:(NSString *)type completed:(void(^)(id responseObject, NSError *error))completion;






# pragma mark
# pragma mark - Notification

- (void)notificationSetting:(NSInteger)status completed:(void(^)(id responseObject, NSError *error))completion;

- (void)notificationListWithOffset:(NSInteger)offset completed:(void(^)(id responseObject, NSError *error))completion;







# pragma mark
# pragma mark - Helper Method

- (void)userDetails:(void(^)(id resonseObject, NSError *error))completion;

- (void)registerDeviceToken:(NSString *)deviceToken andDeviceType:(NSInteger)deviceType andLoginType:(int)loginType completed:(void(^)(id responseObject, NSError *error))completion;





# pragma mark
# pragma mark - Save To Coredata

- (void)saveLoginUserToLocal:(NSDictionary *)dictData;


@end
