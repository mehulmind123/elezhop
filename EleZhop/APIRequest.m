//
//  APIRequest.m
//  EdSmart
//
//  Created by mac-0007 on 08/03/16.
//  Copyright © 2016 mac-0007. All rights reserved.
//

#import "APIRequest.h"

static APIRequest *request = nil;

@interface APIRequest()<GIDSignInUIDelegate, GIDSignInDelegate>
{
    SocialUserInfoBlock uInfoBlock;
    
    UIViewController *currentVC;
    
    BOOL isAPIErrorAlertDisplaying;
}
@end


@implementation APIRequest

+ (id)request
{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^
    {
        request = [[APIRequest alloc] init];
        [[MIAFNetworking sharedInstance] setBaseURL:BASEURL];
        
        NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:[[[[MIAFNetworking sharedInstance] sessionManager] responseSerializer] acceptableStatusCodes]];
        
        [indexSet addIndex:400];
        
        [[[MIAFNetworking sharedInstance] sessionManager] responseSerializer].acceptableStatusCodes = indexSet;
    });
    
    if ([CUserDefaults valueForKey:UserDefaultLoginToken])
        [[[MIAFNetworking sharedInstance] sessionManager].requestSerializer setValue:[CUserDefaults valueForKey:UserDefaultLoginToken] forHTTPHeaderField:@"token"];
    else
        [[[MIAFNetworking sharedInstance] sessionManager].requestSerializer setValue:@"" forHTTPHeaderField:@"token"];
    
    return request;
}

- (void)checkResponseStatus:(id)responseObject
{
    switch ([responseObject intForKey:CJsonStatus])
    {
        case CStatusTen:
        {
            //...Inactive User
            if ([UIApplication userId])
            {
                [appDelegate logoutUser];
                [CustomAlertView iOSAlert:@"Inactive User" withMessage:nil onView:[UIApplication topMostController]];
            }
            break;
        }
        case CStatusNine:
        {
            //...Under Maintenance
        }
        default:
            break;
    }
}

- (void)checkLoginUserStatus:(NSString *)message
{
    if ([UIApplication userId])
    {
        [appDelegate logoutUser];
        [CustomAlertView iOSAlert:@"" withMessage:message onView:[UIApplication topMostController]];
    }
}

- (void)failureWithAPI:(NSString *)api andError:(NSError *)error andHandler:(void(^)(BOOL retry))handler
{
    [[MILoader sharedInstance] stopAnimation];
    
    NSLog(@"%@ API Error == %@", api, error);
    
    if (error.code != -999 && !isAPIErrorAlertDisplaying)
    {
        isAPIErrorAlertDisplaying = YES;
        
        [[UIApplication topMostController] alertWithAPIErrorTitle:@"ERROR!" message:@"An error has occured. Please check your network connection or try again." handler:^(NSInteger index, NSString *btnTitle)
        {
            isAPIErrorAlertDisplaying = NO;
            if (handler) handler(index == 0);
        }];
    }
}

- (NSError *)errorFromDataTask:(NSURLSessionDataTask *)task
{
    return ((NSHTTPURLResponse *)task.response).statusCode == 200 ? nil : [NSError errorWithDomain:@"" code:((NSHTTPURLResponse *)task.response).statusCode userInfo:nil];
}

- (BOOL)isJSONDataValidWithResponse:(id)response
{
    if ([response intForKey:@"status_code"] == CStatus401)
    {
        [self checkLoginUserStatus:[response stringValueForJSON:CJsonMessage]];
        return NO;
    }
    
    id data = [response valueForKey:CJsonData];
    
    if (!data) {
        return NO;
    }
    
    if ([data isEqual:[NSNull null]]) {
        return NO;
    }
    
    if ([data isKindOfClass:[NSString class]]) {
        if (((NSString *)data).length == 0)
            return NO;
    }
    
    if ([data isKindOfClass:[NSArray class]]) {
        if (((NSArray *)data).count == 0)
            return NO;
    }
    
    if ([data isKindOfClass:[NSDictionary class]]) {
        if (((NSDictionary *)data).count == 0)
            return NO;
    }
    
    return [self isJSONStatusValidWithResponse:response];
}

- (BOOL)isJSONStatusValidWithResponse:(id)response
{
    if (!response)
        return NO;
    
    return [response intForKey:CJsonStatus] == CStatus200;
}





# pragma mark
# pragma mark - Social Login

- (void)signUpWithFacebookFromViewController:(UIViewController *)vc withCompletion:(SocialUserInfoBlock)uBlock
{
    uInfoBlock = uBlock;
    
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    loginManager.loginBehavior = FBSDKLoginBehaviorNative;
    [loginManager logOut];
    
    [loginManager logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] fromViewController:vc handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
     {
         if (result.token)
         {
             [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"first_name, last_name, name, picture, email"}] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id userinfo, NSError *error)
              {
                  if (uInfoBlock)
                  {
                      uInfoBlock(userinfo, error);
                      uInfoBlock = nil;
                  }
              }];
         }
         else
         {
             if (result.isCancelled)
                 NSLog(@"Cancelled Facebook Login: %@", result);
             
             if (uInfoBlock)
             {
                 uInfoBlock(nil, error);
                 uInfoBlock = nil;
             }
         }
     }];
}

- (void)signUpWithGoogleFromViewController:(UIViewController *)vc withCompletion:(SocialUserInfoBlock)uBlock
{
    if (vc)
    {
        currentVC = vc;
        uInfoBlock = uBlock;
        
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        GIDSignIn *googleSignIn = [GIDSignIn sharedInstance];
        googleSignIn.shouldFetchBasicProfile = YES;
        googleSignIn.delegate = self;
        googleSignIn.uiDelegate = self;
        
        [googleSignIn signIn];
    }
}

- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [currentVC presentViewController:viewController animated:YES completion:nil];
}

- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController
{
    [currentVC dismissViewControllerAnimated:YES completion:nil];
}

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user  withError:(NSError *)error
{
    if (error == nil)
    {
        GIDAuthentication *authentication = user.authentication;
        FIRAuthCredential *credential =
        [FIRGoogleAuthProvider credentialWithIDToken:authentication.idToken
                                         accessToken:authentication.accessToken];
        
        [[FIRAuth auth] signInWithCredential:credential completion:^(FIRUser *user, NSError *error)
         {
             // ...
             if (error)
             {
                 return;
             }
             else
             {
                 if (uInfoBlock)
                 {
                     uInfoBlock(user, error);
                     uInfoBlock = nil;
                 }
             }
         }];
    }
    
}

- (void)socialLoginWithSocialID:(NSString *)socialID andLoginType:(NSInteger)loginType andEmail:(NSString *)email andDateOfBirth:(NSString *)dateOfBirth andPhoneNumber:(NSString *)phoneNumber andFullName:(NSString *)fullName andProfileImage:(NSString *)profileImage completed:(void (^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dict = @{@"social_id":socialID,
                           @"login_type":[NSNumber numberWithInteger:loginType],
                           @"email":email,
                           @"dob":dateOfBirth,
                           @"full_name":fullName,
                           @"user_image":profileImage,
                           @"phone_no":phoneNumber
                           };
    
    [[MIAFNetworking sharedInstance] POST:CAPITagSocialLogin parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         [appDelegate registerFCMForRemoteNotification];
         
         if ([self isJSONDataValidWithResponse:responseObject])
             [self saveLoginUserToLocal:[responseObject valueForKey:CJsonData]];
         
         if (completion)
             completion(responseObject, nil);
         
         if ([CUserDefaults objectForKey:UserDefaultFCMToken])
         {
             [[APIRequest request] registerDeviceToken:[CUserDefaults objectForKey:UserDefaultFCMToken] andDeviceType:1 andLoginType:[UIApplication userId] ? 1 : -1 completed:nil];
         }
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         [self failureWithAPI:CAPITagSocialLogin andError:error andHandler:^(BOOL retry)
          {
              if (retry) [self socialLoginWithSocialID:socialID andLoginType:(NSInteger)loginType andEmail:email andDateOfBirth:dateOfBirth andPhoneNumber:phoneNumber andFullName:fullName andProfileImage:profileImage completed:completion];
          }];
         
         if (completion)
             completion(nil, error);
     }];
}





# pragma mark
# pragma mark - Home

- (void)featuredOffer:(void(^)(id responseObject, NSError *error))completion
{
    [[MIAFNetworking sharedInstance] POST:CAPITagFeaturedOffer parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion (responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}


- (void)categroyList:(void(^)(id responseObject, NSError *error))completion
{
    [[MIAFNetworking sharedInstance] POST:CAPITagCategoryList parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion (responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}


- (void)raffle:(void(^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    [[MIAFNetworking sharedInstance] POST:CAPITagRaffle parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion (responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(nil, error);
     }];
}


- (void)raffleWinnerListWithRaffleID:(NSString *)raffleID andOffset:(NSInteger)offset completed:(void(^)(id responseObject, NSError *error))completion
{
    if (offset == 0)
        [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dictParam = @{
                                @"raffle_id":raffleID,
                                @"offset":[NSNumber numberWithInteger:offset],
                                @"limit":CLimit
                                };
    
    
    [[MIAFNetworking sharedInstance] POST:CAPITagRaffleWinnerList parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion (responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(nil, error);
     }];
}





# pragma mark
# pragma mark - Store

- (NSURLSessionDataTask *)storeListWithCategoryID:(NSString *)categoryID andOffset:(NSInteger)offset andSearchText:(NSString *)searchTxt andSortBY:(NSInteger)sortBY completed:(void(^)(id responseObject, NSError *error))completion
{
    if (offset == 0 && [searchTxt isEqualToString:@""])
        [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dictParam = @{
                                @"category_id":categoryID,
                                @"offset":[NSNumber numberWithInteger:offset],
                                @"limit":CLimit,
                                @"user_latitude":[NSString stringWithFormat:@"%f", appDelegate.current_lat],
                                @"user_longitude":[NSString stringWithFormat:@"%f", appDelegate.current_long],
                                @"search_text":searchTxt,
                                @"sortby":[NSNumber numberWithInteger:sortBY]
                                };
    
    return [[MIAFNetworking sharedInstance] POST:CAPITagStoreList parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion (responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(nil, error);
     }];
}


- (void)storeDetailWithStoreID:(NSString *)storeID completed:(void(^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{
                                @"store_id":storeID,
                                @"user_latitude":[NSString stringWithFormat:@"%f", appDelegate.current_lat],
                                @"user_longitude":[NSString stringWithFormat:@"%f", appDelegate.current_long]
                                };
    
    [[MIAFNetworking sharedInstance] POST:CAPITagStoreDetail parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion (responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}


- (void)storeDetailOfferListWithStoreID:(NSString *)storeID andOffset:(NSInteger)offset completed:(void(^)(id responseObject, NSError *error))completion
{
    if (offset == 0)
        [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dictParam = @{
                                @"store_id":storeID,
                                @"offset":[NSNumber numberWithInteger:offset],
                                @"limit":CLimit
                                };
    
    [[MIAFNetworking sharedInstance] POST:CAPITagStoreDetailOfferList parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion (responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(nil, error);
     }];
}


- (void)storeDetailReviewListWithStoreID:(NSString *)storeID andOffset:(NSInteger)offset completed:(void(^)(id responseObject, NSError *error))completion
{
    if (offset == 0)
        [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dictParam = @{
                                @"store_id":storeID,
                                @"offset":[NSNumber numberWithInteger:offset],
                                @"limit":CLimit
                                };
    
    [[MIAFNetworking sharedInstance] POST:CAPITagStoreDetailReviewList parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion (responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(nil, error);
     }];
}





# pragma mark
# pragma mark - Upload Bill

- (void)uploadBill:(NSData *)imgData andStoreID:(NSString *)storeID andStoreName:(NSString *)storeName andBillAmount:(NSString *)billAmount andRate:(float)rate andReview:(NSString *)review completed:(void (^)(id responseObject, NSError *error))completion
{
    if (imgData)
    {
        NSDictionary *dictParam = @{
                                    @"store_id":storeID,
                                    @"store_name":storeName,
                                    @"bill_amount":billAmount,
                                    @"rate":[NSNumber numberWithFloat:rate],
                                    @"review":review
                                    };
        
        [[MILoader sharedInstance] startAnimation];
        
        [[MIAFNetworking sharedInstance] POST:CAPITagUploadBill parameters:dictParam constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
         {
             [formData appendPartWithFileData:imgData name:@"bill_image" fileName:[NSString stringWithFormat:@"%f.jpg", [[NSDate date] timeIntervalSince1970]] mimeType:@"image/jpeg"];
             
         } success:^(NSURLSessionDataTask *task, id responseObject)
         {
             [[MILoader sharedInstance] stopAnimation];
             
             if (completion)
                 completion(responseObject, nil);
             
         } failure:^(NSURLSessionDataTask *task, NSError *error)
         {
             [self failureWithAPI:CAPITagUploadBill andError:error andHandler:^(BOOL retry)
              {
                  if (retry) [self uploadBill:imgData andStoreID:storeID andStoreName:storeName andBillAmount:billAmount andRate:rate andReview:review completed:completion];
              }];
             
             if (completion)
                 completion(nil, error);
         }];
    }
}






# pragma mark
# pragma mark - Side Menu

- (void)giftVoucherListWithOffset:(NSInteger)offset completed:(void(^)(id responseObject, NSError *error))completion
{
    if (offset == 0)
        [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dictParam = @{
                                @"offset":[NSNumber numberWithInteger:offset],
                                @"limit":CLimit
                                };
    
    [[MIAFNetworking sharedInstance] POST:CAPITagGiftVoucherList parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion (responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(nil, error);
     }];
}


- (void)getGiftVoucher:(NSString *)giftID completed:(void(^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dictParam = @{
                                @"gift_id":giftID
                                };
    
    [[MIAFNetworking sharedInstance] POST:CAPITagGetGiftVoucher parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion (responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(nil, error);
     }];
}


- (void)myBillsWithOffset:(NSInteger)offset completed:(void(^)(id responseObject, NSError *error))completion
{
    if (offset == 0)
        [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dictParam = @{
                                @"offset":[NSNumber numberWithInteger:offset],
                                @"limit":CLimit
                                };
    
    [[MIAFNetworking sharedInstance] POST:CAPITagMyBillList parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion (responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(nil, error);
     }];
}


- (void)historyHopsEarnedRedeemedRaffleWithType:(NSInteger)type andOffset:(NSInteger)offset andSortBY:(NSInteger)sortBY completed:(void(^)(id responseObject, NSError *error))completion
{
    if (offset == 0)
        [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dictParam = @{
                                @"type":[NSNumber numberWithInteger:type],
                                @"offset":[NSNumber numberWithInteger:offset],
                                @"limit":CLimit,
                                @"sortby":[NSNumber numberWithInteger:sortBY]
                                };
    
    [[MIAFNetworking sharedInstance] POST:CAPITagHistoryHopsEarnedRedeemedRaffle parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion (responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion(nil, error);
     }];
}


- (void)contactUsWithName:(NSString *)name andEmail:(NSString *)email andPhoneNumber:(NSString *)phoneNumber andDescription:(NSString *)description completed:(void(^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dictParam = @{
                                @"name":name,
                                @"email":email,
                                @"phone_number":phoneNumber,
                                @"description":description
                                };
    
    [[MIAFNetworking sharedInstance] POST:CAPITagContactUs parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion (responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagContactUs andError:error andHandler:^(BOOL retry)
          {
              [self contactUsWithName:name andEmail:email andPhoneNumber:phoneNumber andDescription:description completed:completion];
          }];
         
         if (completion)
             completion(nil, error);
     }];
}


- (void)uploadProfileImage:(NSData *)imgData completed:(void (^)(id responseObject, NSError *error))completion
{
    if (imgData)
    {
        [[MILoader sharedInstance] startAnimation];
        
        [[MIAFNetworking sharedInstance] POST:CAPITagUploadProfileImage parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
         {
             [formData appendPartWithFileData:imgData name:@"image" fileName:[NSString stringWithFormat:@"%f.jpg", [[NSDate date] timeIntervalSince1970]] mimeType:@"image/jpeg"];
             
         } success:^(NSURLSessionDataTask *task, id responseObject)
         {
             [[MILoader sharedInstance] stopAnimation];
             
             [self checkResponseStatus:responseObject];
             
             if (completion)
                 completion(responseObject, nil);
             
         } failure:^(NSURLSessionDataTask *task, NSError *error)
         {
             [[MILoader sharedInstance] stopAnimation];
             
             [self failureWithAPI:CAPITagUploadProfileImage andError:error andHandler:^(BOOL retry)
             {
                 if (retry) [self uploadProfileImage:imgData completed:nil];
             }];
             
             if (completion)
                 completion(nil, error);
         }];
    }
}


- (void)editProfileWithName:(NSString *)name andDateOfBirth:(NSString *)dateOfBirth andEmail:(NSString *)email andPhoneNumber:(NSString *)phoneNumber andAddress:(NSString *)address completed:(void(^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dictParam = @{
                                @"full_name":name,
                                @"dob":dateOfBirth,
                                @"email":email,
                                @"phone_number":phoneNumber,
                                @"address":address
                                };
    
    [[MIAFNetworking sharedInstance] POST:CAPITagEditProfile parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion (responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagEditProfile andError:error andHandler:^(BOOL retry)
          {
              if (retry) [self editProfileWithName:name andDateOfBirth:dateOfBirth andEmail:email andPhoneNumber:phoneNumber andAddress:address completed:completion];
          }];
         
         if (completion)
             completion(nil, error);
     }];
}

- (void)getCMS:(NSString *)type completed:(void(^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{
                                @"type":type
                                };
    
    [[MIAFNetworking sharedInstance] POST:CAPITagCMS parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion (responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}






# pragma mark
# pragma mark - Notification

- (void)notificationSetting:(NSInteger)status completed:(void(^)(id responseObject, NSError *error))completion
{
    [[MILoader sharedInstance] startAnimation];
    
    NSDictionary *dictParam = @{
                                @"status":[NSNumber numberWithInteger:status]
                                };
    
    [[MIAFNetworking sharedInstance] POST:CAPITagNotificationSetting parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [[MILoader sharedInstance] stopAnimation];
         
         if (completion)
             completion (responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self failureWithAPI:CAPITagNotificationSetting andError:error andHandler:^(BOOL retry)
          {
              if (retry) [self notificationSetting:status completed:completion];
          }];
         
         if (completion)
             completion(nil, error);
     }];
}


- (void)notificationListWithOffset:(NSInteger)offset completed:(void(^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{
                                @"offset":[NSNumber numberWithInteger:offset],
                                @"limit":CLimit
                                };
    
    [[MIAFNetworking sharedInstance] POST:CAPITagNotificationList parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
             completion (responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}






# pragma mark
# pragma mark - Helper Method

- (void)userDetails:(void(^)(id responseObject, NSError *error))completion
{
    [[MIAFNetworking sharedInstance] POST:CAPITagUserDetail parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([self isJSONDataValidWithResponse:responseObject])
             [self saveLoginUserToLocal:[responseObject valueForKey:CJsonData]];
         
         if (completion)
             completion (responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
             completion(nil, error);
     }];
}

- (void)registerDeviceToken:(NSString *)deviceToken andDeviceType:(NSInteger)deviceType andLoginType:(int)loginType completed:(void(^)(id responseObject, NSError *error))completion
{
    NSDictionary *dictParam = @{@"device_token":deviceToken,
                                @"device_type":[NSNumber numberWithInteger:deviceType],
                                @"login_type":[NSNumber numberWithInt:loginType]};
    
    [[MIAFNetworking sharedInstance] POST:CAPITagRegisterDeviceToken parameters:dictParam success:^(NSURLSessionDataTask *task, id responseObject)
     {
         NSLog(@"token register success %@", responseObject);
         
         if (loginType == -1)
         {
             [CUserDefaults removeObjectForKey:UserDefaultLoginToken];
             [CUserDefaults synchronize];
         }
         
         if (completion)
             completion (responseObject, nil);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"token register fail");
         
         [self failureWithAPI:CAPITagRegisterDeviceToken andError:error andHandler:^(BOOL retry)
          {
              [self registerDeviceToken:deviceToken andDeviceType:deviceType andLoginType:loginType completed:completion];
          }];
         
         if (completion)
             completion(nil, error);
     }];
}




     

# pragma mark
# pragma mark - Save To Coredata
     
- (void)saveLoginUserToLocal:(NSDictionary *)dictData
{
    appDelegate.loginUser = [self userWithDictionary:dictData];
    
    if (appDelegate.loginUser)
    {
        if ([dictData valueForKey:@"token"])
        {
            [CUserDefaults setObject:[dictData stringValueForJSON:@"token"] forKey:UserDefaultLoginToken];
            [CUserDefaults synchronize];
        }
        
        [UIApplication setUserId:[dictData stringValueForJSON:@"userID"]];
    }
}

- (TBLUser *)userWithDictionary:(NSDictionary *)dictUserData
{
    TBLUser *user;
    
    if (dictUserData)
    {
        user = [TBLUser findOrCreate:@{@"userID":[dictUserData stringValueForJSON:@"userID"]} context:[[Store sharedInstance] mainManagedObjectContext]];
        
        user.userID                 = [dictUserData stringValueForJSON:@"userID"];
        user.email                  = [dictUserData stringValueForJSON:@"email"];
        user.user_profile_image     = [dictUserData stringValueForJSON:@"user_image"];
        user.full_name              = [dictUserData stringValueForJSON:@"full_name"];
        user.phone_number           = [dictUserData stringValueForJSON:@"phone_number"];
        user.hops_earned            = [dictUserData stringValueForJSON:@"hops_earned"];
        user.hops_redeemed          = [dictUserData stringValueForJSON:@"hops_redeemed"];
        user.bills_uploaded         = [dictUserData stringValueForJSON:@"bills_uploaded"];
        user.notification           = [dictUserData booleanForKey:@"notification_status"];
        user.birth_date             = [dictUserData stringValueForJSON:@"birth_date"];
        user.address                = [dictUserData stringValueForJSON:@"address"];
        user.faq                    = [dictUserData stringValueForJSON:@"faq_url"];
        
        NSLog(@"login user == %@", user);
        
        [[[Store sharedInstance] mainManagedObjectContext] save];
    }
    
    return user;
}
     


@end
