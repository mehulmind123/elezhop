//
//  AppDelegate.h
//  EleZhop
//
//  Created by mac-0005 on 6/15/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <CoreLocation/CoreLocation.h>

#import <UserNotifications/UserNotifications.h>
#import "Firebase.h"

#import "MITabBarViewController.h"
#import "TWTSideMenuViewController.h"
#import "MISideMenuViewController.h"
#import "TBLUser+CoreDataClass.h"

#import "BasicAppDelegate.h"


@interface AppDelegate : BasicAppDelegate <UIApplicationDelegate, TWTSideMenuViewControllerDelegate, CLLocationManagerDelegate, UNUserNotificationCenterDelegate, FIRMessagingDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

@property (readonly, strong) MITabBarViewController *tabBarViewController;

@property (nonatomic, strong) TWTSideMenuViewController *sideMenuViewController;

@property (nonatomic, strong) MISideMenuViewController *leftMenuViewController;


@property (nonatomic, assign) double current_lat;
@property (nonatomic, assign) double current_long;
@property (nonatomic, strong) NSString *userAddress;
@property (nonatomic, strong) NSString *userCurrentLocation;
@property (nonatomic,strong) CLLocationManager *locationManager;


//..... Core Data
@property (nonatomic, strong) TBLUser *loginUser;



- (void)saveContext;

- (void)signinUser;
- (BOOL)tabBarViewControllerInitiate;
- (void)logoutUser;
- (MITabBarViewController *)tabBarViewController;
- (void)setWindowRootVC:(UIViewController *)vc animated:(BOOL)animated completion:(void (^)(BOOL finished))completed;


- (void)openLoginVC;
- (void)displayAlert:(UIViewController *)vc;

- (void)resignKeyboard;

- (CAShapeLayer *)setLeftSideRadiusForViewUsingBazierPath:(UIView *)view;
- (CAShapeLayer *)setRightSideRadiusForViewUsingBazierPath:(UIView *)view;



- (void)callMobileNo:(NSString *)strMobileNo;
- (void)openURL:(NSString *)url;
- (void)displayLocationAlert;
- (void)openSettings;
- (void)openUrlInWebviewWithURL:(NSString *)url;


- (void)getCurrentLocation;


- (void)animationForHideAndShow:(UIView *)view andOtion:(UIViewAnimationOptions)option andIsHide:(BOOL)isHide;
- (CAAnimation *)swipeAnimationWithSubType:(NSString *)strSubType;



- (void)loadUserDetailFromServer;

- (void)registerFCMForRemoteNotification;



# pragma mark - Post Notification

- (void)postNotificationForUpdateHomeData;

@end

