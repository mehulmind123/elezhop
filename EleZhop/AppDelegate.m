//
//  AppDelegate.m
//  EleZhop
//
//  Created by mac-0005 on 6/15/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "AppDelegate.h"
#import "MITutorialViewController.h"
#import "MILoginViewController.h"
#import "MIHomeViewController.h"
#import "MILuckyDrawViewController.h"
#import "MIWebViewController.h"
#import "MIAFNetworking.h"

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

NSString *const kGCMMessageIDKey = @"gcm.message_id";

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize tabBarViewController;

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //..... Firebase
    [FIRApp configure];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[MIAFNetworking sharedInstance] setDisableTracing:YES];
    
    //..... FABRIC
    [Fabric with:@[[Crashlytics class]]];

    
    // Google Analytics
    
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // 2
    [[GAI sharedInstance].logger setLogLevel:kGAILogLevelVerbose];
    
    // 3
    [GAI sharedInstance].dispatchInterval = 20;
    
    // 4
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-106507635-1"];
    
    //..... FIREBASE
    [FIRMessaging messaging].delegate = self;
    [GIDSignIn sharedInstance].clientID = [FIRApp defaultApp].options.clientID;
    

    [self getCurrentLocation];
    
    NSDictionary *dictionary = @{@"UserAgent":@"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"};
    [CUserDefaults registerDefaults:dictionary];
    
 
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    
    [self initializeWithLaunchOptions:launchOptions];
    
    // ..... Open Home Screen .....
    
    if (![CUserDefaults valueForKey:UserDefaultTutorialScreen])
    {
        [UIApplication removeUserId];
        
        MITutorialViewController *tutorialVC = [[MITutorialViewController alloc] initWithNibName:@"MITutorialViewController" bundle:nil];
        self.window.rootViewController = tutorialVC;
    }
    else
        [self signinUser];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    
    [self checkLocation];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}




#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"EleZhop"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}





# pragma mark
# pragma mark - General Method

- (void)initializeWithLaunchOptions:(NSDictionary *)launchOptions
{
    // ..... Get Location
    
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSFontAttributeName:CFontGilroyMedium(17), NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [[UINavigationBar appearance] setBarTintColor:ColorBlack_383838];
    [[UINavigationBar appearance] setTintColor:ColorWhite];
    [[UINavigationBar appearance] setTranslucent:NO];
    
    
    
    //.....Monitoring Internet Reachability.....
    AFNetworkActivityIndicatorManager.sharedManager.enabled = YES;
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status)
     {
         if (status == AFNetworkReachabilityStatusNotReachable)
         {
             //...
             //             [[UIApplication topMostController] toastAlertWithMessage:@"Poor network connection..." showDone:NO];
         }
     }];
    
    
    
    // ..... GMS Provide API key
    
    [GMSServices provideAPIKey:CGooglePlaceAPIKey];
    [GMSPlacesClient provideAPIKey:CGooglePlaceAPIKey];
    
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options
{
    if ([[url absoluteString] rangeOfString:@"facebook"].location != NSNotFound)
        return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:[options valueForKey:UIApplicationOpenURLOptionsSourceApplicationKey] annotation:nil];
    else if ([[GIDSignIn sharedInstance] handleURL:url sourceApplication:[options valueForKey:UIApplicationOpenURLOptionsSourceApplicationKey] annotation:nil])
        return YES;
    
    return NO;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([[url absoluteString] rangeOfString:@"facebook"].location != NSNotFound)
        return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    else if ([[GIDSignIn sharedInstance] handleURL:url sourceApplication:sourceApplication annotation:annotation])
        return YES;
    
    return NO;
}

- (void)openLoginVC
{
    MILoginViewController *signUpVC = [[MILoginViewController alloc] initWithNibName:@"MILoginViewController" bundle:nil];
    [appDelegate setWindowRootVC:[UINavigationController navigationControllerWithRootViewController:signUpVC] animated:YES completion:nil];
}

- (void)displayAlert:(UIViewController *)vc
{
    [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:@"" message:CMessageUserNotLogin firstButton:@"Cancel" firstHandler:nil
     secondButton:@"Login Now" secondHandler:^(UIAlertAction *action)
     {
         [self openLoginVC];
     } inView:vc];
}

- (void)resignKeyboard
{
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

- (CAShapeLayer *)setLeftSideRadiusForViewUsingBazierPath:(UIView *)view
{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(CViewHeight(view), CViewHeight(view))];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = view.bounds;
    maskLayer.path  = maskPath.CGPath;
    return maskLayer;
}

- (CAShapeLayer *)setRightSideRadiusForViewUsingBazierPath:(UIView *)view
{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(CViewHeight(view), CViewHeight(view))];
    [UIBezierPath bezierPathWithRoundedRect:view.bounds cornerRadius:CViewHeight(view) / 2];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = view.bounds;
    maskLayer.path  = maskPath.CGPath;
    
    return maskLayer;
}

- (void)callMobileNo:(NSString *)strMobileNo
{
    if (IosVersion >= 10.0)
    {
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",strMobileNo]]])
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@", strMobileNo]] options:@{} completionHandler:nil];
        else
        {
            [CustomAlertView iOSAlert:@"" withMessage:@"Your device does not support calling feature." onView:[UIApplication topMostController]];
        }
    }
    else
    {
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",strMobileNo]]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@", strMobileNo]]];
        }
        else
            [CustomAlertView iOSAlert:@"" withMessage:@"Your device does not support calling feature." onView:[UIApplication topMostController]];
    }
}

- (void)openURL:(NSString *)url
{
    NSURL *urlWebsite;
    
    if ([url.lowercaseString hasPrefix:@"http://"] || [url.lowercaseString hasPrefix:@"https://"])
        urlWebsite = [NSURL URLWithString:url];
    else
        urlWebsite = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",url]];
    
    if ([[UIApplication sharedApplication] canOpenURL: urlWebsite])
    {
        if (IosVersion >= 10)
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", urlWebsite]] options:@{} completionHandler:nil];
        else
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", urlWebsite]]];
        
    }
}

- (void)displayLocationAlert
{
    if (([CLLocationManager authorizationStatus]== kCLAuthorizationStatusRestricted) ||([CLLocationManager authorizationStatus]== kCLAuthorizationStatusDenied))
    {
        [[UIApplication topMostController] alertWithTitle:@"" message:CMessageLocationOFF okTitle:CGoToSettings cancleTitle:@"Cancel" handler:^(NSString *title)
         {
             if ([title isEqualToString:CGoToSettings])
             {
                 [appDelegate openSettings];
             }
         }];
    }
}

- (void)openSettings
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

- (void)openUrlInWebviewWithURL:(NSString *)url
{
    NSURL *urlWebsite;
    
    if ([url.lowercaseString hasPrefix:@"http://"] || [url.lowercaseString hasPrefix:@"https://"])
        urlWebsite = [NSURL URLWithString:url];
    else
        urlWebsite = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",url]];
    
    MIWebViewController *webVC = [[MIWebViewController alloc] initWithNibName:@"MIWebViewController" bundle:nil];
    webVC.iObject = urlWebsite;
    [[UIApplication topMostController] presentViewController:[UINavigationController navigationControllerWithRootViewController:webVC] animated:YES completion:nil];
}





#pragma mark
#pragma mark - Root & Main

- (MITabBarViewController *)tabBarViewController
{
    if (tabBarViewController == nil)
    {
        tabBarViewController = [[MITabBarViewController alloc] init];
        [appDelegate.tabBarViewController.tabBarView btnTabBarClicked:appDelegate.tabBarViewController.tabBarView.btnTab3];
    }
    
    return tabBarViewController;
}

- (BOOL)tabBarViewControllerInitiate
{
    return (tabBarViewController != nil);
}

- (TWTSideMenuViewController *)sideMenuViewController
{
    if (!_sideMenuViewController)
    {
        _sideMenuViewController = [[TWTSideMenuViewController alloc] init];
        _sideMenuViewController.shadowColor = [UIColor blackColor];
        _sideMenuViewController.edgeOffset = (UIOffset) { .horizontal = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? 15.0f : 0.0f };
        self.sideMenuViewController.edgeOffset = (UIOffset) { .vertical = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? Is_iPhone_6 || Is_iPhone_6_PLUS ? 120.0f : 90.0f : 0.0f };
        _sideMenuViewController.zoomScale = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? 0.5634f : 0.85f;
        _sideMenuViewController.delegate = self;
        
        _leftMenuViewController = [[MISideMenuViewController alloc] initWithNibName:@"MISideMenuViewController" bundle:nil];
        _sideMenuViewController.menuViewController = _leftMenuViewController;
    }
    
    return _sideMenuViewController;
}

- (void)signinUser
{
    if ([UIApplication userId])
        _loginUser = (TBLUser *)[TBLUser findOrCreate:@{@"userID":[UIApplication userId]}];
    
    tabBarViewController = nil;
    _sideMenuViewController = nil;
    [self.sideMenuViewController setMainViewController:self.tabBarViewController];
    [self setWindowRootVC:self.sideMenuViewController animated:YES completion:nil];
}

- (void)logoutUser
{
    //.....Google Logout
    if([[GIDSignIn sharedInstance] hasAuthInKeychain])
        [[GIDSignIn sharedInstance] signOut];
    
    
    //.....Facebook Logout
    if ([FBSDKAccessToken currentAccessToken])
        [[[FBSDKLoginManager alloc] init] logOut];
    
    
    //.....General Logout
    [UIApplication removeUserId];
    appDelegate.loginUser = nil;
    
    
    if ([CUserDefaults objectForKey:UserDefaultFCMToken])
    {
        [[APIRequest request] registerDeviceToken:[CUserDefaults objectForKey:UserDefaultFCMToken] andDeviceType:1 andLoginType:-1 completed:nil];
    }
    
    [self postNotificationForUpdateHomeData];
    
    [self signinUser];
}

- (void)setWindowRootVC:(UIViewController *)vc animated:(BOOL)animated completion:(void (^)(BOOL finished))completed
{
    [UIView transitionWithView:self.window
                      duration:animated?0.5:0.0
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^
     {
         BOOL oldState = [UIView areAnimationsEnabled];
         [UIView setAnimationsEnabled:NO];
         
         self.window.rootViewController = vc;
         [UIView setAnimationsEnabled:oldState];
     } completion:^(BOOL finished)
     {
         if (completed)
             completed(finished);
     }];
}






# pragma mark
# pragma mark - API Method

- (void)loadUserDetailFromServer
{
    [[APIRequest request] userDetails:^(id responseObject, NSError *error)
    {
        if (responseObject && !error)
        {
            if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
            {
                [[APIRequest request] saveLoginUserToLocal:[responseObject valueForKey:CJsonData]];
            }
        }
    }];
}






# pragma mark
# pragma mark - Post Notification

- (void)postNotificationForUpdateHomeData
{
    // ..... Notification For Update Home Data
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NotificationHomeDataUpdate object:nil];
}





# pragma mark
# pragma mark - Animation

- (void)animationForHideAndShow:(UIView *)view andOtion:(UIViewAnimationOptions)option andIsHide:(BOOL)isHide
{
    [UIView transitionWithView:view duration:0.4 options:option
                    animations:^{
                        [view hideByHeight:isHide];
                    } completion:NULL];
}

- (CAAnimation *)swipeAnimationWithSubType:(NSString *)strSubType
{
    CATransition *animation = [CATransition animation];
    [animation setType:kCATransitionPush];
    [animation setSubtype:strSubType];
    [animation setDuration:0.5];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseIn]];
    return animation;
}





# pragma mark
# pragma mark - Location Manager Delegate

- (void)checkLocation
{
    if (([CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted) ||([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied))
    {
        [[UIApplication topMostController] alertWithTitle:@"" message:CMessageLocationOFF okTitle:CGoToSettings cancleTitle:@"Cancel" handler:^(NSString *title)
         {
             if ([title isEqualToString:CGoToSettings])
             {
                 [appDelegate openSettings];
             }
         }];
    }
    else
        [self getCurrentLocation];
}

- (void)getCurrentLocation
{
    _locationManager = [[CLLocationManager alloc] init];
    
    [_locationManager setDelegate:self];
    
    if (IS_Ios8)
    {
        [_locationManager requestWhenInUseAuthorization];
        [_locationManager requestAlwaysAuthorization];
    }
    
    NSLog(@"start updating");
    
    _locationManager.distanceFilter = 100;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [_locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSString *errorString;
    [manager stopUpdatingLocation];
    
    switch([error code])
    {
        case kCLErrorDenied:
        {
            // ..... DON'T ALLOW CLICK .....
            
            self.current_lat = 0;
            self.current_long = 0;
            [CUserDefaults removeObjectForKey:UserDefaultCurrentLatitude];
            [CUserDefaults removeObjectForKey:UserDefaultCurrentLongitude];
            [CUserDefaults synchronize];
            
            break;
        }
        case kCLErrorLocationUnknown:
            errorString = @"Location data unavailable";
            break;
        default:
            errorString = @"An unknown error has occurred";
            break;
    }
    NSLog(@"Location Error :: %@",errorString);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"Location updated method called in appdelegate method");
    NSLog(@"Current_lat : %f",  [CUserDefaults doubleForKey:UserDefaultCurrentLatitude]);
    NSLog(@"Current_long : %f",  [CUserDefaults doubleForKey:UserDefaultCurrentLongitude]);
    
    _current_lat = newLocation.coordinate.latitude;
    _current_long = newLocation.coordinate.longitude;
    
    [CUserDefaults setDouble:_current_lat forKey:UserDefaultCurrentLatitude];
    [CUserDefaults setDouble:_current_long forKey:UserDefaultCurrentLongitude];
    [CUserDefaults synchronize];
    
    [manager stopUpdatingLocation];
    
    CLGeocoder *ceo = [[CLGeocoder alloc] init];
    CLLocation *loc = [[CLLocation alloc] initWithLatitude:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude]; //insert your new coordinates
    
    [ceo reverseGeocodeLocation:loc completionHandler:^(NSArray *placemarks, NSError *error)
    {
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        
        if (placemark)
        {
            NSLog(@"placemark %@",placemark);
            //String to hold address
            NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
            
            NSLog(@"addressDictionary %@", placemark.addressDictionary);
            
            //Print the location to console
            NSLog(@"I am currently at %@",locatedAt);
            self.userAddress = locatedAt;
            self.userCurrentLocation = locatedAt;
            
            NSMutableDictionary *dictLocation = [NSMutableDictionary new];
            [dictLocation setValue:locatedAt forKey:@"CurrentLocation"];
            
            _loginUser.address = locatedAt;
            [[[Store sharedInstance] mainManagedObjectContext] save];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:NotificationHomeDataUpdate object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:NotificationCurrentLocation object:nil userInfo:dictLocation];
        }
        else
        {
            NSLog(@"Could not locate");
        }
        
    }
     ];
}





#pragma mark - FireBase remote Notification Register
#pragma mark

- (void)registerFCMForRemoteNotification
{
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max)
    {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    else
    {
        // iOS 10 or later
        #if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
#endif
    }
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

// [START receive_message]
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[Messaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    

}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[Messaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"user info == %@", userInfo);
    
    
    
    NSLog(@"push notification info %@", userInfo);
    
    
    if (userInfo.allKeys.count > 0)
    {
        if ([[userInfo valueForKey:@"type"] isEqualToString:CNotificaionTypeBillApproved] || [[userInfo valueForKey:@"type"] isEqualToString:CNotificaionTypeBillRejected] || [[userInfo valueForKey:@"type"] isEqualToString:CNotificaionTypeGetRaffleCode])
        {
            //..... BILL APPROVE || BILL REJECT || GET RAFFLE CODE
            
            [self.tabBarViewController.tabBarView btnTabBarClicked:appDelegate.tabBarViewController.tabBarView.btnTab2];
            [self.sideMenuViewController setMainViewController:appDelegate.tabBarViewController animated:YES closeMenu:YES];
        }
        else if ([[userInfo valueForKey:@"type"] isEqualToString:CNotificaionTypeWonRafflePrize])
        {
            //..... WON RAFFLE PRIZE
            
            if ([UIApplication userId])
            {
                [self.tabBarViewController.tabBarView btnTabBarClicked:appDelegate.tabBarViewController.tabBarView.btnTab3];
                [self.sideMenuViewController setMainViewController:appDelegate.tabBarViewController animated:YES closeMenu:YES];
                UINavigationController *navigationVC = (UINavigationController *)(appDelegate.tabBarViewController.selectedViewController); //..... Get navigationController
                
                NSArray *arrVC = navigationVC.viewControllers;  //..... Get selected tabBar viewcontroller
                
                NSLog(@"First VC %@", [arrVC firstObject]);
                
                if (arrVC.count > 0)
                {
                    MIHomeViewController *homeVC = (MIHomeViewController *)[arrVC firstObject];
                    
                    MILuckyDrawViewController *luckyDrawVC = [[MILuckyDrawViewController alloc] initWithNibName:@"MILuckyDrawViewController" bundle:nil];
                    [homeVC.navigationController pushViewController:luckyDrawVC animated:YES];
                }
            }
            else
            {
                [self openLoginVC];
            }
        }
        else
        {
            //..... ADD HOPS
            if ([UIApplication userId])
            {
                [appDelegate.tabBarViewController.tabBarView btnTabBarClicked:appDelegate.tabBarViewController.tabBarView.btnTab3];
                [appDelegate.sideMenuViewController setMainViewController:appDelegate.tabBarViewController animated:YES closeMenu:YES];
            }
            else
            {
                [self openLoginVC];
            }
        }
    }
    
    
    
    completionHandler(UIBackgroundFetchResultNewData);
}
// [END receive_message]

// [START ios_10_message_handling]
// Receive displayed notifications for iOS 10 devices.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSDictionary *userInfo = notification.request.content.userInfo;
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[Messaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    
    
    // Change this to your preferred presentation option
    completionHandler(UNNotificationPresentationOptionNone);
}

// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler
{
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"did select == %@", userInfo);
    
    
    completionHandler();
}
#endif
// [END ios_10_message_handling]

// [START refresh_token]
- (void)messaging:(nonnull FIRMessaging *)messaging didRefreshRegistrationToken:(nonnull NSString *)fcmToken
{
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    
    
    NSLog(@"FCM registration token: %@", fcmToken);
    
    // TODO: If necessary send token to application server.
    
    // Won't connect since there is no token
    if (!fcmToken)
        return;
    
    // Disconnect previous FCM connection if it exists.
    [FIRMessaging messaging].shouldEstablishDirectChannel = NO;
    
    [CUserDefaults setObject:fcmToken forKey:UserDefaultFCMToken];
    [CUserDefaults synchronize];
    
    NSLog(@"FCM TOken: %@",[CUserDefaults objectForKey:UserDefaultFCMToken]);
    
    [[APIRequest request] registerDeviceToken:fcmToken andDeviceType:1 andLoginType:[UIApplication userId] ? 1 : -1 completed:nil];
}
// [END refresh_token]

// [START ios_10_data_message]
// Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
// To enable direct data messages, you can set [Messaging messaging].shouldEstablishDirectChannel to YES.

- (void)messaging:(FIRMessaging *)messaging didReceiveMessage:(FIRMessagingRemoteMessage *)remoteMessage
{
    NSLog(@"Received data message: %@", remoteMessage.appData);
}
// [END ios_10_data_message]

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Unable to register for remote notifications: %@", error);
}

// This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
// If swizzling is disabled then this function must be implemented so that the APNs device token can be paired to
// the FCM registration token.

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"APNs token retrieved: %@", deviceToken);
    
    const unsigned *devTokenBytes = (const unsigned *)[deviceToken bytes];
    NSString *token = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                       ntohl(devTokenBytes[0]), ntohl(devTokenBytes[1]), ntohl(devTokenBytes[2]),
                       ntohl(devTokenBytes[3]), ntohl(devTokenBytes[4]), ntohl(devTokenBytes[5]),
                       ntohl(devTokenBytes[6]), ntohl(devTokenBytes[7])];
    token = [token stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"Token: %@", token);
    
    [FIRMessaging messaging].APNSToken = deviceToken;
}

- (void)applicationReceivedRemoteMessage:(nonnull FIRMessagingRemoteMessage *)remoteMessage
{
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    if (notificationSettings.types != UIUserNotificationTypeNone)
    {
        NSLog(@"didRegisterUser");
        [application registerForRemoteNotifications];
    }
}

- (void)disableRemoteNotification
{
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
}


@end
