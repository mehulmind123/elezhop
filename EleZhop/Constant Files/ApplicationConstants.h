//
//  ApplicationConstants.h
//  EdSmart
//
//  Created by mac-0007 on 08/03/16.
//  Copyright © 2016 mac-0007. All rights reserved.
//

#ifndef ApplicationConstants_h
#define ApplicationConstants_h

//#define NSLog(...)

#define GoogleClientID              @"914302861087-m7c6883rpbuu2q4ovqabmh2pm53f48fu.apps.googleusercontent.com"




/*======== WHITE SPACE CHAR SET =========*/
#define CWhitespaceCharSet [NSCharacterSet whitespaceAndNewlineCharacterSet]


/*======== FONT =========*/
#define CFontHelveticaRegular(fontSize) [UIFont fontWithName:@"HelveticaNeue" size:fontSize]
#define CFontHelveticaBold(fontSize)    [UIFont fontWithName:@"HelveticaNeue-Bold" size:fontSize]
#define CFontHelveticaMedium(fontSize)  [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize]
#define CFontHelveticaLight(fontSize)   [UIFont fontWithName:@"HelveticaNeue-Light" size:fontSize]
#define CFontHelveticaItalic(fontSize)  [UIFont fontWithName:@"HelveticaNeue-Italic" size:fontSize]

#define CFontGilroyRegular(fontSize)        [UIFont fontWithName:@"Gilroy-Regular" size:fontSize]
#define CFontGilroyLight(fontSize)          [UIFont fontWithName:@"Gilroy-Light" size:fontSize]
#define CFontGilroySemiBold(fontSize)       [UIFont fontWithName:@"Gilroy-Semibold" size:fontSize]
#define CFontGilroyMedium(fontSize)         [UIFont fontWithName:@"Gilroy-Medium" size:fontSize]
#define CFontGilroyBold(fontSize)           [UIFont fontWithName:@"Gilroy-Bold" size:fontSize]
#define CFontGilroyExtraBold(fontSize)      [UIFont fontWithName:@"Gilroy-ExtraBold" size:fontSize]



/*======== COLOR =========*/

#define ColorNavigationbar              CRGB(3,144,161)
#define ColorGreen_299712               CRGB(41,151,18)
#define ColorBlack_4C4E4F               CRGB(76,78,79)
#define ColorBlack_9C9D9B               CRGB(156,157,155)
#define ColorBlue_3184BC                CRGB(49,132,188)
#define ColorLightGray_B5B4B1           CRGB(181, 180, 177)



#define ColorTabUnselect                CRGB(168, 193, 195)

#define ColorWhite                      CRGB(255, 255, 255)
#define ColorBlack                      CRGB(0, 0, 0)



#define ColorYellow_fdb200              CRGB(253, 178, 0)

#define ColorGreen_99cb48               CRGB(153, 203, 72)
#define ColorRed_9FF454D                CRGB(255, 69, 77)
#define ColorSkyEnable_00B5E7           CRGB(0, 181, 231)
#define ColorSkyDisable_00B5E7          CRGB(173, 228, 242)
#define ColorSkyCommon                  CRGB(25, 178, 235)

#define ColorBlack_383838               CRGB(38, 38, 38)
#define ColorBlack_222222               CRGB(34, 34, 34)
#define ColorBlack_7b7b7b               CRGB(149, 149, 149)

#define ColorOrange                     CRGB(244, 164, 9)
#define ColorWhite_EEEEEE               CRGB(238, 238, 238)





/*======== USER DEFAULT =========*/

#define UserDefaultTutorialScreen               @"TutorialScreen"

#define UserDefaultLoginToken                   @"LoginToken"
#define UserDefaultDeviceToken                  @"DeviceToken"
#define UserDefaultGotIT                        @"gotIT"

#define UserDefaultCurrentLatitude              @"currentLatitude"
#define UserDefaultCurrentLongitude             @"currentLongitude"
#define UserDefaultFCMToken                     @"fcmtoken"





/*======== NOTIFICATION CONSTANTS =========*/

#define NotificationImageUploaded               @"NotificationImageUploaded"
#define NotificationArrive                      @"NotificationArrive"
#define NotificationHomeDataUpdate              @"homeDataUpdate"
#define NotificationAddressUpdate               @"addressupdate"
#define NotificationCurrentLocation             @"currentLocation"


#define CNotificaionTypeBillApproved            @"bill_approved"
#define CNotificaionTypeBillRejected            @"bill_reject"
#define CNotificaionTypeGetRaffleCode           @"get_raffle_code"
#define CNotificaionTypeWonRafflePrize          @"won_raffle_prize"
#define CNotificaionTypeAddHops                 @"add_hops"




/*======== CMS =========*/

#define CFAQ                                    @"FAQ's"
#define CTermsAndCondition                      @"Terms & Conditions"
#define CPrivacyPolicy                          @"Privacy Policy"

#define CFAQRequest                             @"faqs"
#define CTermsAndConditionRequest               @"terms_condition"
#define CPrivacyPolicyRequest                   @"privacy_policy"







/*======== OTHER =========*/
#define CComponentJoinedString                  @", "
#define CGooglePlaceAPIKey                      @"AIzaSyB8AREScQ3QaDq2JvBYE49wN1XgeZOArrY"
#define CFireBaseAPIKey                         @"AIzaSyB_Xgn4Cuo-kmFrTkdsJRJd3WwdsGhCsJ8"


#define CGoToSettings                           @"Go to settings"






/*======== Google Analytics Screen Name =========*/

#define kGAILoginScreen             @"MILoginViewController"
#define kGAIHomeScreen              @"MIHomeViewController"
#define kGAIAllCategoryScreen       @"MIAllCategoryViewController"
#define kGAIStoreDetailScreen       @"MIStoreDetailViewController"
#define kGAICaptureBillScreen       @"MICaptureBillViewController"
#define kGAIUploadBillScreen        @"MIUploadBillViewController"
#define kGAIRateUsScreen            @"MIRateUsViewController"
#define kGAIRaffleScreen            @"MILuckyDrawViewController"
#define kGAIBillPreview             @"MIBillPreviewViewController"
#define kGAIRedeemHopsScreen        @"MIRedeemHopsViewController"
#define kGAIHistoryScreen           @"MIMyHistoryViewController"
#define kGAIWinnerScreen            @"MIWinnerViewController"
#define kGAIContactUsScreen         @"MIContactUsViewController"
#define kGAIEditProfileScreen       @"MIEditProfileViewController"
#define kGAINotificationScreen      @"MINotificationViewController"
#define kGAIMyBillsScreen           @"MIMyBillViewController"
#define kGAISettingsScreen          @"MISettingsViewController"
#define kGAICMSScreen               @"MICMSViewController"






















#endif /* ApplicationConstants_h */
