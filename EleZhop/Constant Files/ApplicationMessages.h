//
//  ApplicationMessages.h
//  EdSmart
//
//  Created by mac-0007 on 08/03/16.
//  Copyright © 2016 mac-0007. All rights reserved.
//

#ifndef ApplicationMessages_h
#define ApplicationMessages_h


/*======== Loader Messages =========*/
#define CMessageCongratulation              @"Congratulations!"
#define CMessageSorry                       @"Sorry!"
#define CMessageLoading                     @"Loading..."
#define CMessageSearching                   @"Searching..."
#define CMessageVerifying                   @"Verifying..."
#define CMessageWait                        @"Please Wait..."
#define CMessageUpdating                    @"Updating..."
#define CMessageAuthenticating              @"Authenticating..."
#define CMessageErrorInternetNotAvailable   @"Intenet Connection Not Available!\n Please Try Again Later."



/*======== Validation Messages =========*/

//.... EDIT PROFILE .....

#define CMessageBlankName                   @"Name can’t be blank."
#define CMessageBlankEmail                  @"Email Address can’t be blank."
#define CMessageInvalidEmail                @"Please enter valid email address."
#define CMessageBlankMobileNumber           @"Mobile number can’t be blank."
#define CMessageBlankAddress                @"Address can’t be blank."
#define CMessageBlankDescription            @"Description can’t be blank."



//.... Upload Bill .....

#define CMessageBlankBillAmount             @"Please enter bill amount."
#define CMessageBlankStoreName              @"Please enter store name."



#define CMessageRedeemGiftVoucher           @"Are you sure want to redeem gift card voucher?"










//....OTHER
#define CMessageUserNotLogin                @"You have not logged in."
#define CMessageLogout                      @"Are you sure you want to logout?"
#define CMessageCallNotSupport              @"Your device does not support calling feature."
#define CMessageLocationOFF                 @"Please turn on location settings to explore store close to your location."




//....NO DATA
#define CMessageNoOffsers                   @"Sorry, no offers available in this store at the moment"
#define CMessageNoReviews                   @"Sorry, no reviews available in this store at the moment"


#endif /* ApplicationMessages_h */
