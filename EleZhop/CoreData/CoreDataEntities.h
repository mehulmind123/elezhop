//
//  CoreDataEntities.h
//  Zubba
//
//  Created by mac-0007 on 19/04/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#ifndef CoreDataEntities_h
#define CoreDataEntities_h

#import "TBLUser+CoreDataClass.h"
#import "TBLUser+CoreDataProperties.h"

#endif /* CoreDataEntities_h */
