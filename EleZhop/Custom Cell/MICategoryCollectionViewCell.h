//
//  MICategoryCollectionViewCell.h
//  EleZhop
//
//  Created by mac-0005 on 6/19/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MICategoryCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgVCategory;
@property (weak, nonatomic) IBOutlet UILabel *lblCategoryName;

@end
