//
//  MICategoryCollectionViewCell.m
//  EleZhop
//
//  Created by mac-0005 on 6/19/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MICategoryCollectionViewCell.h"

@implementation MICategoryCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.layer.cornerRadius = 5;
    
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOffset = CGSizeMake(1, 2);
    self.layer.shadowOpacity = 0.2f;
    self.layer.shadowRadius = 1.0f;
    self.layer.masksToBounds = NO;
}

@end
