//
//  MIDetailStoreTableViewCell.h
//  EleZhop
//
//  Created by mac-0005 on 6/19/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface MIDetailStoreTableViewCell : UITableViewCell<MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewTimeDescription;
@property (weak, nonatomic) IBOutlet UIView *viewTime;
@property (weak, nonatomic) IBOutlet UILabel *lblStoreTime;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet MKMapView *mapVStore;
@property (weak, nonatomic) IBOutlet UILabel *lblStoreAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnMap;


@property (weak, nonatomic) IBOutlet NSDictionary *dictStoreDetails;


- (IBAction)btnWebsiteClicked:(id)sender;
- (IBAction)btnCallClicked:(id)sender;
- (IBAction)btnSendMailClicked:(id)sender;

@end
