//
//  MIDetailStoreTableViewCell.m
//  EleZhop
//
//  Created by mac-0005 on 6/19/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIDetailStoreTableViewCell.h"

@implementation MIDetailStoreTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _viewTimeDescription.layer.shadowColor = [UIColor blackColor].CGColor;
    _viewTimeDescription.layer.shadowOffset = CGSizeMake(0, 2);
    _viewTimeDescription.layer.shadowRadius = 2;
    _viewTimeDescription.layer.shadowOpacity = 0.1;
    _viewTimeDescription.layer.masksToBounds = NO;
    
    [_viewTime hideByHeight:NO];
    
    if (_dictStoreDetails)
    {
        if ([[_dictStoreDetails numberForJson:@"store_type"] boolValue])
            [_viewTime hideByHeight:YES];
    }
}

-(void)setZoomViewByMile:(double)mile latitude:(double)getLatitude longitude:(double)getLongitude mapview:(MKMapView*)mapview
{
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(getLatitude, getLongitude);
    CLLocation *podLocation=[[CLLocation alloc]initWithLatitude:location.latitude longitude:location.longitude];
    double miles = 0.4 * 1.609344;
    double scalingFactor = ABS( (cos(2  *M_PI  *podLocation.coordinate.latitude / 360.0) ));
    
    
    MKCoordinateSpan span;
    
    span.latitudeDelta = miles/69.0;
    span.longitudeDelta = miles/(scalingFactor * 69.0);
    
    MKCoordinateRegion region;
    region.span = span;
    region.center = podLocation.coordinate;
    
    //    [self addCircleWithRadius:(miles/2) addCircleWithCoordinate:podLocation.coordinate];
    [mapview setRegion:region animated:YES];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake([[_dictStoreDetails valueForKey:@"store_latitude"] doubleValue], [[_dictStoreDetails valueForKey:@"store_longitude"] doubleValue]);
    
    MKCoordinateSpan span = MKCoordinateSpanMake(0.8, 0.8);
    MKCoordinateRegion region = {coord, span};
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate:coord];
    
    if ((region.center.latitude >= -90) && (region.center.latitude <= 90) && (region.center.longitude >= -180) && (region.center.longitude <= 180))
    {
        [self setZoomViewByMile:0 latitude:coord.latitude longitude:coord.longitude mapview:_mapVStore];
    }
    
    [_mapVStore addAnnotation:annotation];
}



# pragma mark
# pragma mark - Action Events

- (IBAction)btnWebsiteClicked:(id)sender
{
    if (_dictStoreDetails)
        [appDelegate openUrlInWebviewWithURL:[_dictStoreDetails stringValueForJSON:@"store_website"]];
    else
        [CustomAlertView iOSAlert:@"" withMessage:@"Can't open website." onView:self.viewController];
}

- (IBAction)btnCallClicked:(id)sender
{
    if (_dictStoreDetails)
        [appDelegate callMobileNo:[_dictStoreDetails stringValueForJSON:@"store_phone_number"]];
}

- (IBAction)btnSendMailClicked:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setSubject:@""];
        [controller setMessageBody:@" " isHTML:YES];
        [controller setToRecipients:[NSArray arrayWithObjects:[_dictStoreDetails stringValueForJSON:@"store_email"],nil]];
        [self.viewController presentViewController:controller animated:YES completion:NULL];
    }
    else
        [UIAlertController alertViewWithOneButtonWithTitle:@"" withMessage:@"Your device not supproting mail feature." buttonTitle:@"Ok" withDelegate:nil];
}





# pragma mark
# pragma mark - MFMailComposeViewController Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self.viewController dismissViewControllerAnimated:YES completion:NULL];
}






# pragma mark
# pragma mark - MKMapViewDelegate Methods

-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    MKAnnotationView *annotationView = [_mapVStore dequeueReusableAnnotationViewWithIdentifier:@"PinAnnotationView"];
    
    if (!annotationView)
    {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"MKAnnotationView"];
        annotationView.image = [UIImage imageNamed:@"map_point"];
    }
    
    return annotationView;
}

@end
