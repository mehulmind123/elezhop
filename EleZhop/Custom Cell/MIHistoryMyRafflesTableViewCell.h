//
//  MIHistoryMyRafflesTableViewCell.h
//  EleZhop
//
//  Created by mac-0005 on 6/16/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIHistoryMyRafflesTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewContain;

@property (weak, nonatomic) IBOutlet UILabel *lblRaffleName;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblCouponCode;

@end
