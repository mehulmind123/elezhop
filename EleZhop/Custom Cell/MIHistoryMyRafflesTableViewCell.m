//
//  MIHistoryMyRafflesTableViewCell.m
//  EleZhop
//
//  Created by mac-0005 on 6/16/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIHistoryMyRafflesTableViewCell.h"

@implementation MIHistoryMyRafflesTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _viewContain.layer.shadowColor = [UIColor blackColor].CGColor;
    _viewContain.layer.shadowOffset = CGSizeMake(0, 2);
    _viewContain.layer.shadowRadius = 1;
    _viewContain.layer.shadowOpacity = 0.1;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
