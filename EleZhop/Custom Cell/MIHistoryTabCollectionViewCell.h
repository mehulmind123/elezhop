//
//  MIHistoryTabCollectionViewCell.h
//  EleZhop
//
//  Created by mac-0005 on 6/26/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIHistoryTabCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnHistoryTab;
@property (weak, nonatomic) IBOutlet UIView *viewSelectedTab;

@end
