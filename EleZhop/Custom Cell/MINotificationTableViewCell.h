//
//  MINotificationTableViewCell.h
//  EleZhop
//
//  Created by mac-0005 on 6/17/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MINotificationTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewContain;
@property (weak, nonatomic) IBOutlet UILabel *lblNotificationMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblMessageTime;

@end
