//
//  MINotificationTableViewCell.m
//  EleZhop
//
//  Created by mac-0005 on 6/17/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MINotificationTableViewCell.h"

@implementation MINotificationTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _viewContain.layer.shadowColor = CRGB(224, 224, 224).CGColor;
    _viewContain.layer.shadowOffset = CGSizeMake(0, 2);
    _viewContain.layer.shadowRadius = 1;
    _viewContain.layer.shadowOpacity = 1;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
