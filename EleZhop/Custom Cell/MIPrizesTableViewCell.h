//
//  MIPrizesTableViewCell.h
//  EleZhop
//
//  Created by mac-0005 on 6/20/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIPrizesTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblItemName;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDesc;
@property (weak, nonatomic) IBOutlet UIImageView *imgVItem;

@end
