//
//  MIRaffleWinnerCollectionViewCell.h
//  EleZhop
//
//  Created by mac-0005 on 8/4/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIRaffleWinnerCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgVRaffleWinner;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblWinnerNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblCouponCode;

@end
