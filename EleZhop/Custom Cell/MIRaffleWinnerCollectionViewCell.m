//
//  MIRaffleWinnerCollectionViewCell.m
//  EleZhop
//
//  Created by mac-0005 on 8/4/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIRaffleWinnerCollectionViewCell.h"

@implementation MIRaffleWinnerCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _imgVRaffleWinner.layer.cornerRadius = CViewHeight(_imgVRaffleWinner) / 2;
    _imgVRaffleWinner.layer.masksToBounds = YES;
}

@end
