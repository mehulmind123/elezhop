//
//  MIRedeemTableViewCell.h
//  EleZhop
//
//  Created by mac-0005 on 6/16/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIRedeemTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewRedeemCard;
@property (weak, nonatomic) IBOutlet UILabel *lblGiftName;
@property (weak, nonatomic) IBOutlet UILabel *lblVoucherPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblHops;

@end
