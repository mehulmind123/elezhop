//
//  MIRedeemTableViewCell.m
//  EleZhop
//
//  Created by mac-0005 on 6/16/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIRedeemTableViewCell.h"

@implementation MIRedeemTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _viewRedeemCard.layer.shadowColor = [UIColor blackColor].CGColor;
    _viewRedeemCard.layer.shadowOffset = CGSizeMake(0, 2);
    _viewRedeemCard.layer.shadowRadius = 1;
    _viewRedeemCard.layer.shadowOpacity = 0.1;
    _viewRedeemCard.layer.masksToBounds = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
