//
//  MIStoreTableViewCell.h
//  EleZhop
//
//  Created by mac-0005 on 6/15/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIStoreTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewContain;
@property (weak, nonatomic) IBOutlet UIImageView *imgVStore;
@property (weak, nonatomic) IBOutlet UILabel *lblStoreName;
@property (strong, nonatomic) IBOutlet MIGenericRateView *viewRate;

@property (weak, nonatomic) IBOutlet UILabel *lblRate;
@property (weak, nonatomic) IBOutlet UIButton *btnDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblStoreType;
@property (weak, nonatomic) IBOutlet UIView *viewOffer;

@property (weak, nonatomic) IBOutlet UIButton *btnOffers;
@property (weak, nonatomic) IBOutlet UIButton *btnRuffleTicket;
@property (weak, nonatomic) IBOutlet UIButton *btnBounties;


@end
