//
//  MIStoreTableViewCell.m
//  EleZhop
//
//  Created by mac-0005 on 6/15/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIStoreTableViewCell.h"

@implementation MIStoreTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    // ..... UIView .....
    
    _viewContain.layer.cornerRadius = 4;
    _viewContain.layer.borderWidth = 1;
    _viewContain.layer.borderColor = CRGB(194, 194, 194).CGColor;
    _viewContain.layer.shadowColor = [UIColor blackColor].CGColor;
    _viewContain.layer.shadowOffset = CGSizeMake(2, 2);
    _viewContain.layer.shadowRadius = 3;
    _viewContain.layer.shadowOpacity = 0.3;
    _viewContain.layer.masksToBounds = NO;
    
    
    [_viewRate setRateViewWithoutEditing];
    
    
    // ..... UIImageView .....
//    _imgVStore.layer.cornerRadius = CViewWidth(_imgVStore) / 2;
//    _imgVStore.layer.masksToBounds = YES;
    
    
    // ..... UILable .....
    
    _lblStoreType.layer.mask = [appDelegate setLeftSideRadiusForViewUsingBazierPath:_lblStoreType];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (IBAction)btnBounties:(id)sender {
}

@end
