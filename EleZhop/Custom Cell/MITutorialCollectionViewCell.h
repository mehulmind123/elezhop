//
//  MITutorialCollectionViewCell.h
//  EleZhop
//
//  Created by mac-0005 on 7/13/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MITutorialCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgVScreen;
@property (weak, nonatomic) IBOutlet UIImageView *imgVSceenIcon;
@property (weak, nonatomic) IBOutlet MIGenericLabel *lblScreenTitle;
@property (weak, nonatomic) IBOutlet MIGenericLabel *lblScreenDescription;

@end
