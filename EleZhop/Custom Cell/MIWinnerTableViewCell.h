//
//  MIWinnerTableViewCell.h
//  EleZhop
//
//  Created by mac-0005 on 6/16/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIWinnerTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewContain;
@property (weak, nonatomic) IBOutlet UIImageView *imgVUserProfile;
@property (weak, nonatomic) IBOutlet UILabel *lbluserName;
@property (weak, nonatomic) IBOutlet UILabel *lblCouponCode;

@end
