//
//  MICongratulationView.h
//  EleZhop
//
//  Created by mac-0005 on 6/16/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MICongratulationView : UIView

@property (weak, nonatomic) IBOutlet UILabel *lblCongratulationMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;
@property (weak, nonatomic) IBOutlet UIImageView *imgVBG;


+(id)customCongratulationPopup;

@end
