//
//  MICongratulationView.m
//  EleZhop
//
//  Created by mac-0005 on 6/16/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MICongratulationView.h"

@implementation MICongratulationView

+(id)customCongratulationPopup
{
    MICongratulationView  *customView = [[[NSBundle mainBundle] loadNibNamed:@"MICongratulationView" owner:nil options:nil] lastObject];
    
    customView.frame = CGRectMake(0, 0, 262 , 185);
    
    customView.layer.cornerRadius = 10;
    
    return customView;
}

@end
