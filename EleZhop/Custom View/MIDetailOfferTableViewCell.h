//
//  MIDetailOfferTableViewCell.h
//  EleZhop
//
//  Created by mac-0005 on 6/19/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIDetailOfferTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewContain;
@property (weak, nonatomic) IBOutlet UIImageView *imgVOffer;
@property (weak, nonatomic) IBOutlet UILabel *lblOfferName;

@end
