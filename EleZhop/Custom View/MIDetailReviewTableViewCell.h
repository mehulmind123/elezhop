//
//  MIDetailReviewTableViewCell.h
//  EleZhop
//
//  Created by mac-0005 on 6/19/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIDetailReviewTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgVUserProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet MIGenericRateView *viewRate;
@property (weak, nonatomic) IBOutlet UILabel *lblRate;
@property (weak, nonatomic) IBOutlet UILabel *lblReview;
@property (weak, nonatomic) IBOutlet UIView *viewAverageRate;
@property (weak, nonatomic) IBOutlet MIGenericRateView *viewAverageRating;
@property (weak, nonatomic) IBOutlet UILabel *lblAverageRating;

@end
