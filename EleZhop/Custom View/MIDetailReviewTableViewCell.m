//
//  MIDetailReviewTableViewCell.m
//  EleZhop
//
//  Created by mac-0005 on 6/19/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIDetailReviewTableViewCell.h"

@implementation MIDetailReviewTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _imgVUserProfile.layer.cornerRadius = CViewHeight(_imgVUserProfile) / 2;
    _imgVUserProfile.layer.masksToBounds = YES;
    
    [_viewAverageRating setRateViewWithoutEditing];
    [_viewRate setRateViewWithoutEditing];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
