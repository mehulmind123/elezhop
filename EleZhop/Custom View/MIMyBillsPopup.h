//
//  MIMyBillsPopup.h
//  EleZhop
//
//  Created by mac-0005 on 6/21/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIMyBillsPopup : UIView

@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UIImageView *imgVBill;


+(id)customMyBillPopup;

@end
