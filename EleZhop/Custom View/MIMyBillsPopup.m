//
//  MIMyBillsPopup.m
//  EleZhop
//
//  Created by mac-0005 on 6/21/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIMyBillsPopup.h"

@implementation MIMyBillsPopup

+(id)customMyBillPopup
{
    MIMyBillsPopup  *customView = [[[NSBundle mainBundle] loadNibNamed:@"MIMyBillsPopup" owner:nil options:nil] lastObject];
    
    customView.frame = CGRectMake(0, 0, CScreenWidth - 30 , (500 * CScreenHeight) / 667);
    
    customView.layer.cornerRadius = 10;
    
    return customView;
}


@end
