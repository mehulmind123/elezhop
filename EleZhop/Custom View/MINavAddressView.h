//
//  MINavAddressView.h
//  EleZhop
//
//  Created by mac-0005 on 6/22/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MINavAddressView : UIView

@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblCityState;
@property (weak, nonatomic) IBOutlet UIButton *btnAddress;

@end
