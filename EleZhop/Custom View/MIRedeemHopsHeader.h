//
//  MIRedeemHopsHeader.h
//  EleZhop
//
//  Created by mac-0005 on 6/16/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIRedeemHopsHeader : UIView

@property (weak, nonatomic) IBOutlet UIImageView *imgVUser;

@property (weak, nonatomic) IBOutlet UIButton *btnEdit;

@property (weak, nonatomic) IBOutlet UIView *viewEdit;

@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblHopsEarned;
@property (weak, nonatomic) IBOutlet UILabel *lblHopsRedeemed;
@property (weak, nonatomic) IBOutlet UILabel *lblBillsUploaded;


+(id)customHopsRedeemHeader;

@end
