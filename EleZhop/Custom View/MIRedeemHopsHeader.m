//
//  MIRedeemHopsHeader.m
//  EleZhop
//
//  Created by mac-0005 on 6/16/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIRedeemHopsHeader.h"

@implementation MIRedeemHopsHeader

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [_viewEdit.layer setCornerRadius:CViewHeight(_viewEdit) / 2];
    [_viewEdit.layer setShadowColor:[UIColor blackColor].CGColor];
    [_viewEdit.layer setShadowOpacity:0.5];
    [_viewEdit.layer setShadowRadius:1.0];
    [_viewEdit.layer setShadowOffset:CGSizeZero];
}

+(id)customHopsRedeemHeader
{
    MIRedeemHopsHeader  *customHeader = [[[NSBundle mainBundle] loadNibNamed:@"MIRedeemHopsHeader" owner:nil options:nil] lastObject];
    
    customHeader.frame = CGRectMake(0, 0, CScreenWidth, (CScreenWidth * 235) / 375);
    
    return customHeader;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _imgVUser.layer.cornerRadius = CViewWidth(_imgVUser) / 2;
    _imgVUser.layer.borderWidth = 3;
    _imgVUser.layer.borderColor = ColorNavigationbar.CGColor;
    _imgVUser.layer.masksToBounds = YES;
}

@end
