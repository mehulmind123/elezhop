//
//  MIStoreDetailHeader.h
//  EleZhop
//
//  Created by mac-0005 on 6/19/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIStoreDetailHeader : UIView

@property (strong, nonatomic) IBOutlet UIImageView *imgVStore;

@property (strong, nonatomic) IBOutlet MIGenericRateView *viewRate;

@property (strong, nonatomic) IBOutlet UILabel *lblRate;
@property (strong, nonatomic) IBOutlet UILabel *lblInstore;

@property (strong, nonatomic) IBOutlet UIButton *btnDistance;
@property (strong, nonatomic) IBOutlet UIButton *btnOffers;
@property (strong, nonatomic) IBOutlet UIButton *btnDetails;
@property (strong, nonatomic) IBOutlet UIButton *btnReviews;
@property (strong, nonatomic) IBOutlet UIButton *btnTotalOffers;
@property (strong, nonatomic) IBOutlet UIButton *btnTotalRaffleTickets;
@property (strong, nonatomic) IBOutlet UIButton *btnTotalBouties;

@property (strong, nonatomic) IBOutlet UIView *viewTab;
@property (strong, nonatomic) IBOutlet UIView *viewTabSelected;
@property (strong, nonatomic) IBOutlet UIView *viewBottom;


+(id)customStoreDetailHeader;

@end
