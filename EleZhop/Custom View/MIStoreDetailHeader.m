//
//  MIStoreDetailHeader.m
//  EleZhop
//
//  Created by mac-0005 on 6/19/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIStoreDetailHeader.h"

@implementation MIStoreDetailHeader

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _lblInstore.layer.mask = [appDelegate setLeftSideRadiusForViewUsingBazierPath:_lblInstore];
}

+(id)customStoreDetailHeader
{
    MIStoreDetailHeader  *customHeader = [[[NSBundle mainBundle] loadNibNamed:@"MIStoreDetailHeader" owner:nil options:nil] lastObject];
    
    customHeader.frame = CGRectMake(0, 0, CScreenWidth, (305 * CScreenWidth) / 375);
    
    [customHeader.viewRate setRateViewWithoutEditing];
    
    customHeader.viewTab.layer.shadowColor = customHeader.viewBottom.layer.shadowColor = ColorNavigationbar.CGColor;
    customHeader.viewTab.layer.shadowOffset = customHeader.viewBottom.layer.shadowOffset = CGSizeMake(0, 2);
    customHeader.viewTab.layer.shadowRadius = customHeader.viewBottom.layer.shadowRadius = 1;
    customHeader.viewTab.layer.shadowOpacity = customHeader.viewBottom.layer.shadowOpacity = 0.2;
    customHeader.viewTab.layer.masksToBounds = customHeader.viewBottom.layer.masksToBounds = NO;
    
    
    return customHeader;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
//    _imgVStore.layer.cornerRadius = CViewHeight(_imgVStore) / 2;
//    _imgVStore.layer.borderWidth = 4;
//    _imgVStore.layer.borderColor = [UIColor whiteColor].CGColor;
//    _imgVStore.layer.masksToBounds = YES;
}

@end
