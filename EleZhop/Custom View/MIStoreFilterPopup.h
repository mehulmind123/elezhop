//
//  MIStoreFilterPopup.h
//  EleZhop
//
//  Created by mac-0005 on 6/19/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIStoreFilterPopup : UIView

@property (weak, nonatomic) IBOutlet UIButton *btnPopularity;
@property (weak, nonatomic) IBOutlet UIButton *btnDistance;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle1;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle2;

+(id)customStoreFilterPopup;

@end
