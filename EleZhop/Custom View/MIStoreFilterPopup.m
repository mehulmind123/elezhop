//
//  MIStoreFilterPopup.m
//  EleZhop
//
//  Created by mac-0005 on 6/19/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIStoreFilterPopup.h"

@implementation MIStoreFilterPopup

+(id)customStoreFilterPopup
{
    MIStoreFilterPopup  *customView = [[[NSBundle mainBundle] loadNibNamed:@"MIStoreFilterPopup" owner:nil options:nil] lastObject];
    
    customView.frame = CGRectMake(CScreenWidth - 85, 0, 85 , 320);
    
    return customView;
}

@end
