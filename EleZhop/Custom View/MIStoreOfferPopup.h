//
//  MIStoreOfferPopup.h
//  EleZhop
//
//  Created by mac-0005 on 6/21/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIStoreOfferPopup : UIView

@property (weak, nonatomic) IBOutlet UIImageView *imgVOffer;

@property (weak, nonatomic) IBOutlet UITextView *txtVOfferDescription;

@property (weak, nonatomic) IBOutlet UILabel *lblOfferTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblValidUpto;

@property (weak, nonatomic) IBOutlet UIView *viewBottom;

@property (weak, nonatomic) IBOutlet UIButton *btnOk;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnBuy;


+(id)customStoreOfferPopup;

@end
