//
//  MIStoreOfferPopup.m
//  EleZhop
//
//  Created by mac-0005 on 6/21/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIStoreOfferPopup.h"

@implementation MIStoreOfferPopup

+(id)customStoreOfferPopup
{
    MIStoreOfferPopup  *customView = [[[NSBundle mainBundle] loadNibNamed:@"MIStoreOfferPopup" owner:nil options:nil] lastObject];
    
    customView.frame = CGRectMake(0, 0, CScreenWidth - (CScreenWidth * 45) / 375 , (380 * CScreenHeight) / 667);
    
    customView.layer.cornerRadius = 15;
    
    return customView;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [_txtVOfferDescription setContentOffset:CGPointZero animated:NO];
}

@end
