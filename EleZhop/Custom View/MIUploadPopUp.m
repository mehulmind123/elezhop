//
//  MIUploadPopUp.m
//  EleZhop
//
//  Created by mac-0005 on 6/19/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIUploadPopUp.h"

@implementation MIUploadPopUp

+(id)customUploadPopup
{
    MIUploadPopUp  *customView = [[[NSBundle mainBundle] loadNibNamed:@"MIUploadPopUp" owner:nil options:nil] lastObject];
    
    customView.frame = CGRectMake(0, Is_iPhone_5 ? 100 : 0, 262 , 185);
    
    customView.layer.cornerRadius = 10;
    
    [customView.txtCommon addLeftPaddingWithWidth:5];
    customView.txtCommon.layer.borderWidth = 1;
    customView.txtCommon.layer.borderColor = ColorBlack_9C9D9B.CGColor;
    
    return customView;
}




# pragma mark
# pragma mark - Action Events

- (IBAction)btnCancelClicked:(id)sender
{
    [self.viewController dismissPopUp:self.viewController.view];
}

@end
