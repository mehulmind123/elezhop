//
//  MITabBar.h
//  Zubba
//
//  Created by mac-0007 on 30/11/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MITabBar : UIView

@property (nonatomic, strong) IBOutlet UIButton *btnTab1;
@property (nonatomic, strong) IBOutlet UIButton *btnTab2;
@property (nonatomic, strong) IBOutlet UIButton *btnTab3;
@property (nonatomic, strong) IBOutlet UIButton *btnTab4;
@property (nonatomic, strong) IBOutlet UIButton *btnTab5;

@property (nonatomic, strong) IBOutlet UILabel *lblTab1;
@property (nonatomic, strong) IBOutlet UILabel *lblTab2;
@property (nonatomic, strong) IBOutlet UILabel *lblTab3;
@property (nonatomic, strong) IBOutlet UILabel *lblTab4;
@property (nonatomic, strong) IBOutlet UILabel *lblTab5;

@property (strong, nonatomic) IBOutlet UIView *viewRound;

@property (assign, nonatomic) BOOL *isFromNotification;


+ (id)customTabBar;
- (IBAction)btnTabBarClicked:(UIButton *)tab;

@end
