//
//  MITabBar.m
//  Zubba
//
//  Created by mac-0007 on 30/11/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import "MITabBar.h"

@implementation MITabBar

+ (id)customTabBar
{
    MITabBar *tabBar = [[[NSBundle mainBundle] loadNibNamed:@"MITabBar" owner:nil options:nil] lastObject];
    tabBar.frame = CGRectMake(0, CScreenHeight - 73, CScreenWidth, 73);
    
    
    if (Is_iPhone_4 || Is_iPhone_5)
    {
        tabBar.lblTab1.font = [UIFont fontWithName:tabBar.lblTab1.font.fontName size:(tabBar.lblTab1.font.pointSize - 2)];
        tabBar.lblTab2.font = [UIFont fontWithName:tabBar.lblTab2.font.fontName size:(tabBar.lblTab2.font.pointSize - 2)];
        tabBar.lblTab3.font = [UIFont fontWithName:tabBar.lblTab3.font.fontName size:(tabBar.lblTab3.font.pointSize - 2)];
        tabBar.lblTab4.font = [UIFont fontWithName:tabBar.lblTab4.font.fontName size:(tabBar.lblTab4.font.pointSize - 2)];
        tabBar.lblTab5.font = [UIFont fontWithName:tabBar.lblTab5.font.fontName size:(tabBar.lblTab5.font.pointSize - 2)];
    }
    
    return tabBar;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.viewRound layoutIfNeeded];
    self.viewRound.layer.cornerRadius = CViewWidth(self.viewRound) / 2;
    self.viewRound.layer.borderWidth = 1;
    self.viewRound.layer.borderColor = CRGB(215, 215, 215).CGColor;
    self.viewRound.layer.masksToBounds = YES;
}

- (IBAction)btnTabBarClicked:(UIButton *)tab
{
    if (tab.selected && tab.tag != 4) return;
    
    self.btnTab1.selected =
    self.btnTab2.selected =
    self.btnTab3.selected =
    self.btnTab4.selected =
    self.btnTab5.selected = NO;
    
    self.lblTab1.textColor =
    self.lblTab2.textColor =
    self.lblTab3.textColor =
    self.lblTab4.textColor =
    self.lblTab5.textColor = ColorTabUnselect;
    
    switch (tab.tag)
    {
        case 0: //..... SHARE
        {
            if ([UIApplication userId])
            {
                self.btnTab1.selected = YES;
                self.lblTab1.textColor = ColorNavigationbar;
                
                NSURL *url = [NSURL URLWithString:AppStoreURL];
                NSArray *objectsToShare = @[url];
                
                UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
                
                [[UIApplication topMostController] presentViewController:controller animated:YES completion:nil];
            }
            else
            {
                [appDelegate displayAlert:self.viewController];
            }
            
            break;
        }
        case 1: //..... HISTORY
        {
            if ([UIApplication userId])
            {
                self.btnTab2.selected = YES;
                self.lblTab2.textColor = ColorNavigationbar;
                
                [appDelegate.tabBarViewController setSelectedIndex:0];
            }
            else
            {
                [appDelegate displayAlert:self.viewController];
            }
            
            break;
        }
        case 2: //..... HOME
        {
            self.btnTab3.selected = YES;
            self.lblTab3.textColor = ColorNavigationbar;
            NSLog(@"%@", appDelegate.tabBarViewController.viewControllers);
            [(UINavigationController *)[appDelegate.tabBarViewController.viewControllers objectAtIndex:1] popToRootViewControllerAnimated:false];
            [appDelegate.tabBarViewController setSelectedIndex:1];
            
            break;
        }
        case 3: //..... UPLOAD BILL
        {
            if ([UIApplication userId])
            {
                self.btnTab4.selected = YES;
                self.lblTab4.textColor = ColorNavigationbar;
                
                [appDelegate.tabBarViewController setSelectedIndex:2];
            }
            else
            {
                [appDelegate displayAlert:self.viewController];
            }
            
            break;
        }
        case 4: //..... MORE
        {
            if ([UIApplication userId])
            {
                self.btnTab5.selected = YES;
                self.lblTab5.textColor = ColorNavigationbar;
                
                [appDelegate resignKeyboard];
                [appDelegate.sideMenuViewController openMenuAnimated:YES completion:nil];
            }
            else
            {
                [appDelegate displayAlert:self.viewController];
            }
            
            break;
        }
    }
}

@end
