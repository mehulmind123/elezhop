//
//  MITabBarViewController.h
//  Zubba
//
//  Created by mac-0007 on 18/11/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MITabBar.h"

@interface MITabBarViewController : UITabBarController<UITabBarControllerDelegate>

@property (strong, nonatomic) MITabBar *tabBarView;

@property (nonatomic, assign) NSUInteger selectedIndex;

- (NSUInteger)indexOfViewControllerClass:(Class)cl;

@end
