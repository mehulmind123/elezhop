//
//  MITabBarViewController.m
//  Zubba
//
//  Created by mac-0007 on 18/11/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import "MITabBarViewController.h"
#import "MIMyHistoryViewController.h"
#import "MIHomeViewController.h"
#import "MICaptureBillViewController.h"
#import "MISideMenuViewController.h"
#import "MIRedeemHopsViewController.h"
#import "MIMyBillViewController.h"
#import "MINotificationViewController.h"
#import "MIUploadBillViewController.h"

@interface MITabBarViewController ()

@end

@implementation MITabBarViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark -
#pragma mark - General Method

- (void)initialize
{
    self.tabBarView = [MITabBar customTabBar];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.tabBarView];
    [self.tabBar setHidden:YES];
    
    
    
    //....UIApplicationWillChangeStatusBarFrameNotification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarFrameFrameWillChange) name:UIApplicationWillChangeStatusBarFrameNotification object:nil];
    
    
    //.......Self ViewControllers........
    
    
    MIMyHistoryViewController *historyVC = [[MIMyHistoryViewController alloc] initWithNibName:@"MIMyHistoryViewController" bundle:nil];
    [historyVC setShowTabBar:YES];
    
    
    MIHomeViewController *homeVC = [[MIHomeViewController alloc] initWithNibName:@"MIHomeViewController" bundle:nil];
    [homeVC setShowTabBar:YES];
    
    
    MICaptureBillViewController *captureBillVC = [[MICaptureBillViewController alloc] initWithNibName:@"MICaptureBillViewController" bundle:nil];
    [captureBillVC setShowTabBar:YES];
    
    
    MIRedeemHopsViewController *redeemHopsVC = [[MIRedeemHopsViewController alloc] initWithNibName:@"MIRedeemHopsViewController" bundle:nil];
    [redeemHopsVC setShowTabBar:YES];
    
    
    MIMyBillViewController *myBillVC = [[MIMyBillViewController alloc] initWithNibName:@"MIMyBillViewController" bundle:nil];
    [myBillVC setIsNavigateFromSidePanel:YES];
    [myBillVC setShowTabBar:YES];
    
    
    MINotificationViewController *notificationVC = [[MINotificationViewController alloc] initWithNibName:@"MINotificationViewController" bundle:nil];
    [notificationVC setIsNavigateFromSidePanel:YES];
    [notificationVC setShowTabBar:YES];
    
    
    self.viewControllers = @[
                             [[UINavigationController alloc] initWithRootViewController:historyVC],
                             [[UINavigationController alloc] initWithRootViewController:homeVC],
                             [[UINavigationController alloc] initWithRootViewController:captureBillVC],
                             [[UINavigationController alloc] initWithRootViewController:redeemHopsVC],
                             [[UINavigationController alloc] initWithRootViewController:myBillVC],
                             [[UINavigationController alloc] initWithRootViewController:notificationVC]
                             ];
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex
{
    [super setSelectedIndex:selectedIndex];
    
    if (selectedIndex > 2)
    {
        self.tabBarView.btnTab1.selected =
        self.tabBarView.btnTab2.selected =
        self.tabBarView.btnTab3.selected =
        self.tabBarView.btnTab4.selected =
        self.tabBarView.btnTab5.selected = NO;
    }
}

- (NSUInteger)indexOfViewControllerClass:(Class)cl
{
    __block NSUInteger index = 0;
    [self.viewControllers enumerateObjectsUsingBlock:^(UINavigationController *nav, NSUInteger idx, BOOL *stop)
     {
         if ([[nav.viewControllers firstObject] isKindOfClass:cl])
         {
             index = idx;
             *stop = YES;
         }
     }];
    return index;
}


#pragma mark -
#pragma mark - UIApplicationWillChangeStatusBarFrameNotification

- (void)statusBarFrameFrameWillChange
{
    CViewSetHeight(self.view, CScreenHeight - (CStatusBarHeight - 20));
}

@end
