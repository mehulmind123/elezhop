//
//  MIGenericLabel.m
//  EdSmart
//
//  Created by mac-0007 on 15/07/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import "MIGenericLabel.h"

@implementation MIGenericLabel

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    if(Is_iPhone_4 || Is_iPhone_5)
        self.font = [UIFont fontWithName:self.font.fontName size:(self.font.pointSize - 2)];
    else if(Is_iPhone_6_PLUS)
        self.font = [UIFont fontWithName:self.font.fontName size:(self.font.pointSize + 1)];
}

@end
