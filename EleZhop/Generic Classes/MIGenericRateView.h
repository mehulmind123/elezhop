//
//  MIGenericRateView.h
//  EleZhop
//
//  Created by mac-0005 on 7/13/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "EDStarRating.h"

@interface MIGenericRateView : EDStarRating

- (void)setRateViewWithEditing;

- (void)setRateViewWithoutEditing;

@end
