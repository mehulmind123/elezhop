//
//  MIGenericRateView.m
//  EleZhop
//
//  Created by mac-0005 on 7/13/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIGenericRateView.h"

@implementation MIGenericRateView

- (void)setRateViewWithEditing
{
    self.starImage = [[UIImage imageNamed:@"rateus_start_unselected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.starHighlightedImage = [[UIImage imageNamed:@"rateus_start_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.horizontalMargin = 0;
    self.displayMode = EDStarRatingDisplayHalf;
    self.editable = YES;
}

- (void)setRateViewWithoutEditing
{
    self.starImage = [[UIImage imageNamed:@"store_star_unselect"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.starHighlightedImage = [[UIImage imageNamed:@"store_star_select"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.horizontalMargin = 1.0f;
    self.displayMode = EDStarRatingDisplayHalf;
    self.editable = NO;
}

@end
