//
//  MIGradientLayer.h
//  Labari
//
//  Created by mac-0006 on 20/03/2017.
//  Copyright © 2017 mind. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface MIGradientLayer : CAGradientLayer

+(id)gradientLayerWithFrame:(CGRect)frame;
+(id)gradientLayerWithFrameSeperator:(CGRect)frame;
+(id)gradientLayerWithFrameForBorder:(CGRect)frame;

@end
