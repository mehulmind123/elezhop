//
//  MIGradientLayer.m
//  Labari
//
//  Created by mac-0006 on 20/03/2017.
//  Copyright © 2017 mind. All rights reserved.
//

#import "MIGradientLayer.h"

@implementation MIGradientLayer

+(id)gradientLayerWithFrame:(CGRect)frame
{
    MIGradientLayer *gradient = [MIGradientLayer layer];
    gradient.frame = frame;
    
    
    gradient.colors = [NSArray arrayWithObjects:
                       (id)(ColorNavigationbar.CGColor),
                       (id)(CRGB(12, 154, 171).CGColor),
                        nil];
    
    gradient.startPoint = CGPointMake(0.0,0.3);
    gradient.endPoint = CGPointMake(0.0, 0.7);
    
    return gradient;
}

+(id)gradientLayerWithFrameSeperator:(CGRect)frame
{
    MIGradientLayer *gradient = [MIGradientLayer layer];;
    gradient.frame = frame;
    
    gradient.colors = [NSArray arrayWithObjects:
                       (id)([UIColor colorWithRed:(255/255.0) green:(240/255.0) blue:(5/255.0) alpha:1.000].CGColor),
                       (id)([UIColor colorWithRed:(255/255.0) green:(186/255.0) blue:(0/255.0) alpha:1.000].CGColor), nil];
    
    gradient.startPoint = CGPointMake(0.2,0.0);
    gradient.endPoint = CGPointMake(0.8, 0.0);
    
    return gradient;
}


+(id)gradientLayerWithFrameForBorder:(CGRect)frame
{
    MIGradientLayer *gradient = [self gradientLayerWithFrame:frame];
    CAShapeLayer *shapeLayer = [[CAShapeLayer alloc] init];
    shapeLayer.lineWidth = 1.5;
    shapeLayer.path = [UIBezierPath bezierPathWithRoundedRect:frame cornerRadius:50].CGPath;
    shapeLayer.fillColor = nil;
    shapeLayer.strokeColor = [UIColor blackColor].CGColor;
    gradient.mask = shapeLayer;
    gradient.cornerRadius = frame.size.height/2;
    return gradient;
    
}

@end
