//
//  TBLCountry+CoreDataProperties.h
//  EdSmart
//
//  Created by mac-0005 on 8/20/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TBLCountry.h"

NS_ASSUME_NONNULL_BEGIN

@interface TBLCountry (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *countryCode;
@property (nullable, nonatomic, retain) NSNumber *countryID;
@property (nullable, nonatomic, retain) NSString *countryName;
@property (nullable, nonatomic, retain) NSNumber *status;

@end

NS_ASSUME_NONNULL_END
