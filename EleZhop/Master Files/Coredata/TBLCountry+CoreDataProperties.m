//
//  TBLCountry+CoreDataProperties.m
//  EdSmart
//
//  Created by mac-0005 on 8/20/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TBLCountry+CoreDataProperties.h"

@implementation TBLCountry (CoreDataProperties)

@dynamic countryCode;
@dynamic countryID;
@dynamic countryName;
@dynamic status;

@end
