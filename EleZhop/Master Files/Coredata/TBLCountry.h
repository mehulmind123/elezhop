//
//  TBLCountry.h
//  EdSmart
//
//  Created by mac-0005 on 8/20/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface TBLCountry : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "TBLCountry+CoreDataProperties.h"
