//
//  NSDate+MIDate.h
//  Engage
//
//  Created by mac-0007 on 16/11/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (MIDate)

+ (NSDate *)dateFromString:(NSString *)string withFormat:(NSString *)format;
+ (NSString *)stringFromDate:(NSDate *)date withFormat:(NSString *)format;
+ (NSString *)stringFromTimeStamp:(double)timeStamp withFormat:(NSString *)format;
+ (NSString *)hmsSegmentFromTimeInterval:(NSInteger)interval;
+ (NSString *)hoursFromInterval:(NSInteger)interval;

+ (NSString *)timeAgoFromTimestamp:(NSString *)timestamp;
+ (NSString *)dayOrDateFromTimestamp:(NSString *)timestamp;

- (BOOL)date:(NSDate *)dateA isTheSameDayThan:(NSDate *)dateB;
- (BOOL)date:(NSDate *)dateA isEqualOrBefore:(NSDate *)dateB;
- (BOOL)date:(NSDate *)dateA isEqualOrAfter:(NSDate *)dateB;
- (BOOL)date:(NSDate *)date isEqualOrAfter:(NSDate *)startDate andEqualOrBefore:(NSDate *)endDate;
@end
