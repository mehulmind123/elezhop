//
//  NSDate+MIDate.m
//  Engage
//
//  Created by mac-0007 on 16/11/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import "NSDate+MIDate.h"

@implementation NSDate (MIDate)


+ (NSDateFormatter *)dateFormatter
{
    static dispatch_once_t onceToken;
    static NSDateFormatter *dateFormatter;
    dispatch_once(&onceToken, ^{
        dateFormatter = [NSDateFormatter new];
        dateFormatter.timeZone = [NSTimeZone localTimeZone];
        dateFormatter.locale = [NSLocale currentLocale];
    });
    
    return dateFormatter;
}

+ (NSCalendar *)calendar
{
    static dispatch_once_t onceToken;
    static NSCalendar *calendar;
    dispatch_once(&onceToken, ^{
        calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        calendar.timeZone = [NSTimeZone localTimeZone];
        calendar.locale = [NSLocale currentLocale];
    });
    
    return calendar;
}



#pragma mark -
#pragma mark - Helper

+ (NSDate *)dateFromString:(NSString *)string withFormat:(NSString *)format
{
    [[NSDate dateFormatter] setDateFormat:format];
    
    NSDate *date = [[NSDate dateFormatter] dateFromString:string];
    return date;
}

+ (NSString *)stringFromDate:(NSDate *)date withFormat:(NSString *)format
{
    [[NSDate dateFormatter] setDateFormat:format];
    
    NSString *string = [[NSDate dateFormatter] stringFromDate:date];
    return string;
}

+ (NSString *)stringFromTimeStamp:(double)timeStamp withFormat:(NSString *)format
{
    NSString *strDate = [NSDate stringFromDate:[NSDate dateWithTimeIntervalSince1970:timeStamp] withFormat:format];
    return strDate.length?strDate:@"";
}

+ (NSString *)hmsSegmentFromTimeInterval:(NSInteger)interval
{
    NSInteger ti = interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    
    if (hours > 0)
        return [NSString stringWithFormat:@"%2ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
    else
        return [NSString stringWithFormat:@"%2ld:%02ld", (long)minutes, (long)seconds];
}

+ (NSString *)hoursFromInterval:(NSInteger)interval
{
    NSString *sHours = [NSString stringWithFormat:@"%.1f", ((float)interval / 3600)];
    NSArray *arrComp = [sHours componentsSeparatedByString:@"."];
    
    return [NSString stringWithFormat:@"%@ %@", ([[arrComp lastObject] integerValue] == 0)?[arrComp firstObject]:sHours, ([[arrComp firstObject] integerValue] == 1)?@"hour":@"hours"];
}

+ (NSString *)timeAgoFromTimestamp:(NSString *)timestamp
{
    NSUInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfYear | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    NSDate *preDay = [NSDate dateWithTimeIntervalSince1970:[timestamp doubleValue]];
    NSDate *today = [NSDate date];
    
    NSDateComponents *dateComponents = [[NSDate calendar] components:unitFlags fromDate:preDay toDate:today options:0];
    
    NSInteger years     = [dateComponents year];
    NSInteger month     = [dateComponents month];
    NSInteger weeks     = [dateComponents weekOfYear];
    NSInteger days      = [dateComponents day];
    NSInteger hours     = [dateComponents hour];
    NSInteger minutes   = [dateComponents minute];
//    NSInteger seconds   = [dateComponents second];
    
    NSString *duration;
    
    if (years > 0)
        duration = [NSString stringWithFormat:@"%ld y", (long)years];
    else if (month > 0)
        duration = [NSString stringWithFormat:@"%ld m", (long)month];
    else if (weeks > 0)
        duration = [NSString stringWithFormat:@"%ld w", (long)weeks];
    else if (days > 0)
        duration = [NSString stringWithFormat:@"%ld d", (long)days];
    else if (hours > 0)
        duration = [NSString stringWithFormat:@"%ld h", (long)hours];
    else if (minutes > 0)
        duration = [NSString stringWithFormat:@"%ld m", (long)minutes];
    else
    {
        duration = @"Just now";
        
//        if (seconds <= 0)
//            duration = @"Just now";
//        else
//            duration = [NSString stringWithFormat:@"%ld sec", (long)seconds];
    }
    
    return [NSString stringWithFormat:@"%@", duration];
}

+ (NSString *)dayOrDateFromTimestamp:(NSString *)timestamp
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[timestamp doubleValue]];
    
    NSDateComponents *components = [[NSDate calendar] components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[NSDate date]];
    
    [components setHour:-[components hour]];
    [components setMinute:-[components minute]];
    [components setSecond:-[components second]];
    
    //This variable should now be pointing at a date object that is the start of today (midnight);
    NSDate *today = [[NSDate calendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
    
    NSString *duration;
    if ([[NSDate date] date:today isTheSameDayThan:date])
        duration = [NSString stringWithFormat:@"Today %@", [NSDate stringFromTimeStamp:[timestamp doubleValue] withFormat:@"hh:mm a"]];
    else
        duration =  [NSDate stringFromTimeStamp:[timestamp doubleValue] withFormat:@"dd MMM ,yyyy hh:mm a"];
    
    return  duration;
}


#pragma mark -
#pragma mark - ComprareDate

- (BOOL)date:(NSDate *)dateA isTheSameDayThan:(NSDate *)dateB
{
    NSDateComponents *componentsA = [[NSDate calendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:dateA];
    NSDateComponents *componentsB = [[NSDate calendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:dateB];
    
    return (componentsA.year == componentsB.year) && (componentsA.month == componentsB.month) && (componentsA.day == componentsB.day);
}

- (BOOL)date:(NSDate *)dateA isEqualOrBefore:(NSDate *)dateB
{
    if ([dateA compare:dateB] == NSOrderedAscending || [self date:dateA isTheSameDayThan:dateB])
        return YES;
    
    return NO;
}

- (BOOL)date:(NSDate *)dateA isEqualOrAfter:(NSDate *)dateB
{
    if ([dateA compare:dateB] == NSOrderedDescending || [self date:dateA isTheSameDayThan:dateB])
        return YES;
    
    return NO;
}

- (BOOL)date:(NSDate *)date isEqualOrAfter:(NSDate *)startDate andEqualOrBefore:(NSDate *)endDate
{
    if ([self date:date isEqualOrAfter:startDate] && [self date:date isEqualOrBefore:endDate])
        return YES;
    
    return NO;
}

@end
