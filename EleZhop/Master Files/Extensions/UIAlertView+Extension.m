//
//  UIAlertView+Extension.m
//  MyPatients
//
//  Created by mac-0010 on 4/2/15.
//  Copyright (c) 2015 Robots and Pencils Inc. All rights reserved.
//

#import "UIAlertView+Extension.h"
#import <objc/runtime.h>

@implementation UIAlertView (Extension)

//Runtime association key.
static NSString *handlerRunTimeAccosiationKey = @"alertViewBlocksDelegate";

- (void)showAlerViewWithHandlerBlock:(UIActionAlertViewCallBackHandler)handler
{
    //set runtime accosiation of object
    //param -  sourse object for association, association key, association value, policy of association
    objc_setAssociatedObject(self, (__bridge  const void *)(handlerRunTimeAccosiationKey), handler, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    [self setDelegate:self];
    [self show];
}
#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UIActionAlertViewCallBackHandler completionHandler = objc_getAssociatedObject(self, (__bridge  const void *)(handlerRunTimeAccosiationKey));
    
    if (completionHandler != nil) {
        
        completionHandler(alertView, buttonIndex);
    }
}

-(BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    if (alertView.alertViewStyle == UIAlertViewStylePlainTextInput)
    {
        return ([[[alertView textFieldAtIndex:0] text] length]>0)?YES:NO;
    }
    else
    {
        return YES;
    }
}



@end
