//
//  UIImageView+LoadingView.m
//  Patch
//
//  Created by mac-00015 on 12/4/15.
//  Copyright © 2015 mindinventory. All rights reserved.
//

#import "UIImageView+LoadingView.h"

@implementation UIImageView (LoadingView)

+(id)loadingView
{
    id view = [self loadingImageView];
    
    if(!view)
        view = [[[self class] alloc] init];
    
    return view;
}

+(id)loadingImageView
{
    NSMutableArray *arr = [@[] mutableCopy];
    
    for (int i=2 ; i < 32; i++)
    {
        UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"%d",i]];
        [arr addObject:img];
    }
    
    UIImageView *loadingImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    loadingImageView.image = [UIImage imageNamed:@"1"];
    loadingImageView.contentMode = UIViewContentModeCenter;
    [loadingImageView setAnimationImages:arr];
    [loadingImageView setAnimationDuration:8];
    [loadingImageView startAnimating];
    return loadingImageView;
}

@end
