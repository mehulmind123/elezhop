//
//  UILabel+Extension.h
//  Master
//
//  Created by mac-0001 on 12/06/15.
//  Copyright (c) 2015 mac-0001. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Extension)

- (void)detectHashTagClickHandler:(void(^)(NSString *hashTag))block;;

- (void)detectLinkClickHandler:(void(^)(NSString *stringURL))block;

@end
