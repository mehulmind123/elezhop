//
//  UINavigationController+Extension.h
//  MI API Example
//
//  Created by mac-0001 on 25/11/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Extension)


+(UINavigationController *)navigationController;
+(UINavigationController *)navigationControllerWithRootViewController:(UIViewController *)rootViewController;




-(UIViewController *)viewControllerOfClass:(Class)class;
-(BOOL)popToViewControllerOfClass:(Class)class animated:(BOOL)animated;

@end


// To-Do - If removing backbar button text is not working or is applyed to common classes (PhotoLibrary, MessageComposer) than add in UIViewcontroller