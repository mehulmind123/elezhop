//
//  UITextField+MITextField.m
//  EdSmart
//
//  Created by mac-0006 on 14/07/2016.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import "UITextField+MITextField.h"

@implementation UITextField (MITextField)

- (void)addLeftPaddingWithWidth:(CGFloat)width
{
    UIView *textFieldPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 0)];
    self.leftView = textFieldPadding;
    self.leftViewMode = UITextFieldViewModeAlways;
}

- (void)addRightPaddingWithWidth:(CGFloat)width
{
    UIView *textFieldPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 0)];
    self.rightView = textFieldPadding;
    self.rightViewMode = UITextFieldViewModeAlways;
}

-(void)setLeftImage:(UIImage *)img withSize:(CGSize)sizeImg
{
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, sizeImg.width, sizeImg.height)];
    imgView.image = img;
    
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    self.leftView = imgView;
    self.leftViewMode = UITextFieldViewModeAlways;
}

-(void)setRightImage:(UIImage *)img withSize:(CGSize)sizeImg
{
    UIButton *imgView = [UIButton buttonWithType:UIButtonTypeCustom];
    imgView.frame = CGRectMake(0 , 0, sizeImg.width, sizeImg.height);
    [imgView setImage:img forState:UIControlStateNormal];;
    [imgView setUserInteractionEnabled:NO];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    self.rightView = imgView;
    self.rightViewMode = UITextFieldViewModeAlways;
}

@end
