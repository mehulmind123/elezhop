//
//  UIViewController+Helper.h
//  MI API Example
//
//  Created by mac-0001 on 13/11/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Master.h"
#import "MIPopUpOverlay.h"

@interface UIViewController (Helper)

+(UIViewController *)viewController;

- (BOOL)isVisible;
- (BOOL)isDismissed;
- (BOOL)isPresented;


-(void)presentOnTop;

- (void)presentPopUp:(UIView *)view from:(PresentType)presentType shouldCloseOnTouchOutside:(BOOL)isClosable;
- (void)dismissPopUp:(UIView *)view;
- (void)alertWithAPIErrorTitle:(NSString *)title message:(NSString *)message handler:(void (^)(NSInteger index, NSString *btnTitle))handler;


- (void)alertWithTitle:(NSString *)title message:(NSString *)message;
- (void)alertWithTitle:(NSString *)title message:(NSString *)message handler:(void (^)())handler;
- (void)alertWithTitle:(NSString *)title message:(NSString *)message okTitle:(NSString*)ok cancleTitle:(NSString*)cancle handler:(void (^)(NSString *title))handler;

@end
