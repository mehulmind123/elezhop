//
//  UIViewController+ImagePicker.h
//  Engage
//
//  Created by mac-0007 on 03/12/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import <UIKit/UIKit.h>


#define ChoosePhotoOption   @"Select Source Type"
#define PhotoLibraryOption  @"Photo Library"
#define PhotoCameraOption   @"Take Photo"


typedef void (^CompletionHandler)(UIImage *image, NSDictionary *info);
typedef void (^CancelHandler)(UIAlertAction * action);

@interface UIViewController (ImagePicker) <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

- (void)presentImagePickerSource:(CompletionHandler)handler;
- (void)presentImagePickerSource:(CompletionHandler)handler cancelHandler:(CancelHandler)cancelHandler;
- (void)presentPhotoLibray:(CompletionHandler)handler;
- (void)presentPhotoCamera:(CompletionHandler)handler;


@end
