//
//  UIViewController+ImagePicker.m
//  Engage
//
//  Created by mac-0007 on 03/12/16.
//  Copyright © 2016 Jignesh-0007. All rights reserved.
//

#import "UIViewController+ImagePicker.h"

static NSString *const COMPLETIONHANDLER = @"COMPLETIONHANDLER";

@implementation UIViewController (ImagePicker)

- (void)presentImagePickerSource:(CompletionHandler)handler
{
    //.....ActionSheet.....
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:PhotoCameraOption style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
    {
        [self presentPhotoCamera:handler];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:PhotoLibraryOption style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
    {
        [self presentPhotoLibray:handler];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void)presentImagePickerSource:(CompletionHandler)handler cancelHandler:(CancelHandler)cancelHandler
{
    //.....ActionSheet.....
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:PhotoCameraOption style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                            {
                                [self presentPhotoCamera:handler];
                            }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:PhotoLibraryOption style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                            {
                                [self presentPhotoLibray:handler];
                            }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:cancelHandler]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void)presentPhotoLibray:(CompletionHandler)handler
{
    [self setObject:handler forKey:COMPLETIONHANDLER];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.delegate = self;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

- (void)presentPhotoCamera:(CompletionHandler)handler
{
    [self setObject:handler forKey:COMPLETIONHANDLER];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.delegate = self;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Camera Unavailable" message:@"Unable to find a camera on your device." preferredStyle:UIAlertControllerStyleAlert];
        
        [alertView addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alertView animated:YES completion:nil];
    }
}



#pragma mark -
#pragma mark - UIImagePickerController Delagate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        CompletionHandler handler = [self objectForKey:COMPLETIONHANDLER];
        if (handler)
            handler(image, info);
    }];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
        CompletionHandler handler = [self objectForKey:COMPLETIONHANDLER];
        
        if (handler)
            handler(nil, nil);
        
        
        //..... When click cancel redirect to Home screen.
        
//        [appDelegate.tabBarViewController.tabBarView btnTabBarClicked:appDelegate.tabBarViewController.tabBarView.btnTab3];
        
    }];
}

@end
