//
//  UIViewController+LoaderAndAlerts.h
//  MI API Example
//
//  Created by mac-0001 on 22/11/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomAlertView.h"

@interface UIViewController (LoaderAndAlerts)

- (void)iOSAlert:(NSString *)title withMessage:(NSString *)message withDelegate:(id)delegate;
- (void)showStatusBarAlert:(NSString *)message;

//-(void)ShowHudLoaderWithText:(NSString *)text;
//-(void)ShowHudLoader;
//-(void)HideHudLoader;



@end
