//
//  UIViewController+LoaderAndAlerts.m
//  MI API Example
//
//  Created by mac-0001 on 22/11/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import "UIViewController+LoaderAndAlerts.h"

@implementation UIViewController (LoaderAndAlerts)

#pragma mark - Alert Methods

- (void)iOSAlert:(NSString *)title withMessage:(NSString *)message withDelegate:(id)delegate
{
    [CustomAlertView iOSAlert:title withMessage:message onView:self];
}

- (void)showStatusBarAlert:(NSString *)message
{
    [CustomAlertView showStatusBarAlert:message];
}


#pragma mark - Loader Methods

//-(void)ShowHudLoaderWithText:(NSString *)text
//{
//    [[PPLoader sharedLoader] ShowHudLoaderWithText:text];
//}
//
//-(void)ShowHudLoader
//{
//    [[PPLoader sharedLoader] ShowHudLoader];
//}
//
//-(void)HideHudLoader
//{
//    [[PPLoader sharedLoader] HideHudLoader];
//}

@end
