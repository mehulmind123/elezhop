//
//  MILoginViewController.h
//  EleZhop
//
//  Created by mac-0005 on 6/15/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MILoginViewController : ParentViewController
{
    IBOutlet UIButton *btnLoginWithGoogle;
    IBOutlet UIButton *btnLoginWithFacebook;
}


- (IBAction)btnLoginWithGoogleClicked:(id)sender;
- (IBAction)btnLoginWithFacebookClicked:(id)sender;

@end
