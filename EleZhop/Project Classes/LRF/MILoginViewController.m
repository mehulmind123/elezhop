//
//  MILoginViewController.m
//  EleZhop
//
//  Created by mac-0005 on 6/15/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MILoginViewController.h"
#import "MIHomeViewController.h"
#import "MIAllCategoryViewController.h"

@interface MILoginViewController ()

@end

@implementation MILoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Login";
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark
# pragma mark - Initialize

- (void)initialize
{
    btnLoginWithGoogle.layer.cornerRadius = 3;
    btnLoginWithFacebook.layer.cornerRadius = 3;
}

# pragma mark
# pragma mark - Action Events

- (IBAction)btnLoginWithGoogleClicked:(id)sender
{
    [[APIRequest request] signUpWithGoogleFromViewController:self withCompletion:^(FIRUser *userInfo, NSError *error)
    {
        [[GIDSignIn sharedInstance] signOut];
        
        if (userInfo)
        {
            NSString *profileImage = [userInfo valueForKey:@"photoURL"] ? [userInfo valueForKey:@"photoURL"] : @"";
            
            [self socialLoginWithSocialID:[userInfo valueForKey:@"userID"] andLoginType:2 andEmail:[userInfo valueForKey:@"email"] ? [userInfo valueForKey:@"email"] : @"" andDateOfBirth:@"" andPhoneNumber:[userInfo valueForKey:@"phoneNumber"] ? [userInfo valueForKey:@"phoneNumber"] : @"" andFullName:[userInfo valueForKey:@"displayName"] andProfileImage:profileImage];
        }
        else if (error)
        {
            /*
             Do not prompt error message for following error.
             
             Error Domain=com.google.GIDSignIn Code=-5 "The user canceled the sign-in flow." UserInfo={NSLocalizedDescription=The user canceled the sign-in flow.}
             */
            if (error.code == -5) return;
            
            NSString *title = [NSString stringWithFormat:@"Google(%ld)", (long)error.code];
            [CustomAlertView iOSAlert:title withMessage:error.localizedDescription onView:self];
        }
    }];
}

- (IBAction)btnLoginWithFacebookClicked:(id)sender
{
    [[APIRequest request] signUpWithFacebookFromViewController:self withCompletion:^(id userInfo, NSError *error)
     {
         if (userInfo)
         {
             NSString *email = [userInfo[@"email"] isBlankValidationPassed]?userInfo[@"email"]:@"";
             NSString *dateOfBirth = [userInfo[@"date_of_birth"] isBlankValidationPassed]?userInfo[@"date_of_birth"]:@"";
             NSString *phoneNumber = [userInfo[@"phone_number"] isBlankValidationPassed]?userInfo[@"phone_number"]:@"";
             
             NSString *profileImage = [[[userInfo[@"picture"] valueForKey:@"data"] valueForKey:@"url"] isBlankValidationPassed] ? [[userInfo[@"picture"] valueForKey:@"data"] valueForKey:@"url"] : @"";
             
             NSLog(@"Facebook login successfully \n %@", userInfo);
             
             [self socialLoginWithSocialID:userInfo[@"id"] andLoginType:1 andEmail:email andDateOfBirth:dateOfBirth andPhoneNumber:phoneNumber andFullName:userInfo[@"name"] andProfileImage:profileImage];
         }
         else if (error)
         {
             NSString *title = [NSString stringWithFormat:@"Facebook(%ld)", (long)error.code];
             [CustomAlertView iOSAlert:title withMessage:error.localizedDescription onView:self];
         }
     }];
}





# pragma mark
# pragma mark - API Method

- (void)socialLoginWithSocialID:(NSString *)socialID andLoginType:(NSInteger)loginType andEmail:(NSString *)email andDateOfBirth:(NSString *)dateOfBirth andPhoneNumber:(NSString *)phoneNumber andFullName:(NSString *)fullName andProfileImage:(NSString *)profileImage
{
    [[APIRequest request] socialLoginWithSocialID:socialID andLoginType:loginType andEmail:email andDateOfBirth:dateOfBirth andPhoneNumber:phoneNumber andFullName:fullName andProfileImage:profileImage completed:^(id responseObject, NSError *error)
    {
        if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
        {
            [appDelegate signinUser];
        }
    }];
}


@end
