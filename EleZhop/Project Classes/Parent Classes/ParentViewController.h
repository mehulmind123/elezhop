//
//  ParentViewController.h
//  EdSmart
//
//  Created by mac-0007 on 09/03/16.
//  Copyright © 2016 mac-0007. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "GAITrackedViewController.h"

@interface ParentViewController : GAITrackedViewController

@property (nonatomic, assign) BOOL isNavigateFromSidePanel;

@property (nonatomic, assign) BOOL showTabBar;

@property (nonatomic, assign) BOOL isPresentedVC;

@property (nonatomic, strong) id iObject;

@property (nonatomic, strong) NSString * screeName;

- (void)leftBurgerMenuClicked;



- (void)startLoadingAnimationInView:(UIView *)view;

- (void)stopLoadingAnimationInView:(UIView *)view;

@end
