//
//  ParentViewController.m
//  EdSmart
//
//  Created by mac-0007 on 09/03/16.
//  Copyright © 2016 mac-0007. All rights reserved.
//

#import "ParentViewController.h"
#import "MISideMenuViewController.h"
#import "GAIDictionaryBuilder.h"

@interface ParentViewController ()

@end



@implementation ParentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.topItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"back"]];
    [self.navigationController.navigationBar setBackIndicatorImage:[UIImage imageNamed:@"back"]];
    [self.navigationController.navigationBar setBarTintColor:ColorNavigationbar];
    [self.navigationController.navigationBar setTintColor:ColorWhite];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self setNavigationStatusBarStyle:UIStatusBarStyleLightContent];
    
    if ([appDelegate tabBarViewControllerInitiate] && ![self isKindOfClass:[MISideMenuViewController class]])
    {
        appDelegate.tabBarViewController.tabBarView.hidden = !self.showTabBar;
    }
    
    
    if (self.view.tag == 200)
    {
        //...Screen in which back button replaced with menu for side panel.
        
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBurgerMenuClicked)]];
        [self.navigationItem setHidesBackButton:YES];
    }
    
    
    
   
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if (self.showTabBar)
    {
        CGFloat sHeight = self.navigationController.navigationBar.hidden || self.navigationController.navigationBar.translucent?0.0f:[[UIApplication sharedApplication] statusBarFrame].size.height;
        CGFloat nHeight = self.navigationController.navigationBar.hidden || self.navigationController.navigationBar.translucent?0.0f:44.0f;
        CGFloat tHeight = 49.0f;
        
        CViewSetHeight(self.view, CScreenHeight - sHeight - nHeight - tHeight);
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [appDelegate resignKeyboard];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:self.screenName];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)dealloc
{
}

#pragma mark -
#pragma mark - Action Event

- (void)leftBurgerMenuClicked
{
    [appDelegate resignKeyboard];
    [appDelegate.sideMenuViewController openMenuAnimated:YES completion:nil];
}


# pragma mark -
# pragma mark - Helper Method

- (void)setNavigationStatusBarStyle:(UIStatusBarStyle)statusBarStyle
{
    [[UIApplication sharedApplication] setStatusBarStyle:statusBarStyle];
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)startLoadingAnimationInView:(UIView *)view
{
    UIView *animationView = (UIView *)[view viewWithTag:1000];

    if (!animationView)
    {
        animationView = [[UIView alloc] initWithFrame:CGRectZero];
        animationView.translatesAutoresizingMaskIntoConstraints = NO;
        animationView.backgroundColor = [UIColor whiteColor];
        animationView.tag = 1000;

        //....
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [activityIndicator setTranslatesAutoresizingMaskIntoConstraints:NO];
        [activityIndicator setColor:ColorNavigationbar];
        [activityIndicator setCenter:animationView.center];
        [activityIndicator startAnimating];

        [animationView addSubview:activityIndicator];
        [animationView addConstraint:[NSLayoutConstraint constraintWithItem:activityIndicator attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:animationView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
        [animationView addConstraint:[NSLayoutConstraint constraintWithItem:activityIndicator attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:animationView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];


        //....
        [view addSubview:animationView];
        [view addConstraint:[NSLayoutConstraint constraintWithItem:animationView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeLeft multiplier:1 constant:0]];
        [view addConstraint:[NSLayoutConstraint constraintWithItem:animationView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
        [view addConstraint:[NSLayoutConstraint constraintWithItem:animationView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeWidth multiplier:1 constant:0]];
        [view addConstraint:[NSLayoutConstraint constraintWithItem:animationView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeHeight multiplier:1 constant:0]];
    }
}

- (void)stopLoadingAnimationInView:(UIView *)view
{
    UIView *animationView = (UIView *)[view viewWithTag:1000];
    if (animationView)
        [animationView removeFromSuperview];
}


@end
