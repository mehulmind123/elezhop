//
//  MICMSViewController.h
//  EleZhop
//
//  Created by mac-0005 on 6/23/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger
{
    FAQ,
    TermsCondition,
    PrivacyPolicy
} CMSType;

@interface MICMSViewController : ParentViewController
{
    IBOutlet UIWebView *webView;
}

@property (nonatomic, assign) CMSType cmsType;

@end
