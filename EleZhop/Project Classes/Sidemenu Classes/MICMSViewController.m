//
//  MICMSViewController.m
//  EleZhop
//
//  Created by mac-0005 on 6/23/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MICMSViewController.h"

@interface MICMSViewController ()

@end

@implementation MICMSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"CMS Screen";
    
    if (_cmsType == FAQ)
        self.title = CFAQ;
    else if (_cmsType == TermsCondition)
    {
        self.title = CTermsAndCondition;
        [self loadCMSDataFromServer];
    }
    else
    {
        self.title = CPrivacyPolicy;
        [self loadCMSDataFromServer];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

# pragma mark
# pragma mark - API Events

- (void)loadCMSDataFromServer
{
    [[APIRequest request] getCMS:_cmsType == FAQ ? CFAQRequest : _cmsType == TermsCondition ? CTermsAndConditionRequest : CPrivacyPolicyRequest completed:^(id responseObject, NSError *error)
    {
        if (responseObject && !error)
        {
            if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
            {
                [webView loadHTMLString:[[responseObject valueForKey:CJsonData] stringValueForJSON:@"description"] baseURL:nil];
            }
        }
    }];
}


@end
