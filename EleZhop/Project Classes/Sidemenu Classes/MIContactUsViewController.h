//
//  MIContactUsViewController.h
//  EleZhop
//
//  Created by mac-0005 on 6/17/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIContactUsViewController : ParentViewController
{
    IBOutlet MIGenericTextField *txtName;
    IBOutlet MIGenericTextField *txtEmail;
    IBOutlet MIGenericTextField *txtPhoneNo;
    
    IBOutlet UITextView *txtVDescription;
    
    IBOutlet UIButton *btnSend;
}


- (IBAction)btnSendClicked:(id)sender;

@end
