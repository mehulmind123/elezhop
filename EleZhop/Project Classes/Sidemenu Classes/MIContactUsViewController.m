//
//  MIContactUsViewController.m
//  EleZhop
//
//  Created by mac-0005 on 6/17/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIContactUsViewController.h"
#import "MISettingsViewController.h"

@interface MIContactUsViewController ()

@end

@implementation MIContactUsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.screenName = self.title = @"Contact Us";
    
    txtVDescription.layer.cornerRadius = 3;
    txtVDescription.layer.borderWidth = 1;
    txtVDescription.layer.borderColor = ColorNavigationbar.CGColor;
    [txtVDescription setPlaceholder:@"Write to us.."];
    [txtVDescription setPlaceholderColor:ColorLightGray_B5B4B1];
    
    [self loadDataFromLocal];
}

- (void)loadDataFromLocal
{
    [txtName setText:appDelegate.loginUser.full_name];
    [txtEmail setText:appDelegate.loginUser.email];
    [txtPhoneNo setText:appDelegate.loginUser.phone_number];
}

# pragma mark
# pragma mark - Action Events

- (IBAction)btnSendClicked:(id)sender
{
    if (![txtName.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankName onView:self];

    else if (![txtEmail.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankEmail onView:self];
    
    else if (![txtEmail.text isValidEmailAddress])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageInvalidEmail onView:self];
    
    else if (![txtPhoneNo.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankMobileNumber onView:self];
    
    else if (![txtVDescription.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankDescription onView:self];
    
    else
    {
        [appDelegate resignKeyboard];
        
        [[APIRequest request] contactUsWithName:txtName.text andEmail:txtEmail.text andPhoneNumber:txtPhoneNo.text andDescription:txtVDescription.text completed:^(id responseObject, NSError *error)
         {
             if (responseObject && !error)
             {
                 if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
                 {
                     [UIAlertController alertControllerWithOneButtonWithStyle:UIAlertControllerStyleAlert title:@"" message:[responseObject stringValueForJSON:CJsonMessage] buttonTitle:@"Ok" handler:^(UIAlertAction *action)
                    {
                        [appDelegate.tabBarViewController setSelectedIndex:1];
                        [appDelegate.sideMenuViewController setMainViewController:appDelegate.tabBarViewController animated:YES closeMenu:YES];
                     } inView:self];
                 }
             }
         }];
    }
}

@end
