//
//  MIEditProfileViewController.h
//  EleZhop
//
//  Created by mac-0005 on 6/17/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIEditProfileViewController : ParentViewController
{
    IBOutlet UIImageView *imgVUserProfile;
    
    IBOutlet MIGenericTextField *txtName;
    IBOutlet MIGenericTextField *txtDateOfBirth;
    IBOutlet MIGenericTextField *txtEmail;
    IBOutlet MIGenericTextField *txtPhoneNo;
    IBOutlet MIGenericTextField *txtAddress;
    
    IBOutlet UIButton *btnSend;
}


- (IBAction)btnProfileImgClicked:(id)sender;
- (IBAction)btnSendClicked:(id)sender;

@end
