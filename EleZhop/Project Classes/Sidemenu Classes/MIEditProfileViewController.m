//
//  MIEditProfileViewController.m
//  EleZhop
//
//  Created by mac-0005 on 6/17/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIEditProfileViewController.h"

@interface MIEditProfileViewController ()
{
    NSData *imgData;
}
@end

@implementation MIEditProfileViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    [imgVUserProfile sd_setImageWithURL:[NSURL URLWithString:appDelegate.loginUser.user_profile_image]];
    imgVUserProfile.layer.cornerRadius = CViewWidth(imgVUserProfile) / 2;
    imgVUserProfile.layer.borderWidth = 3;
    imgVUserProfile.layer.borderColor = ColorNavigationbar.CGColor;
    imgVUserProfile.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.screenName = self.title = @"Edit Profile";
    
    [txtDateOfBirth setRightImage:[UIImage imageNamed:@"calendar"] withSize:CGSizeMake(40, 30)];
    
    NSDateFormatter *dateFormete = [NSDateFormatter new];
    [dateFormete setDateFormat:@"dd/MM/yyyy"];
    
    NSDate *dateOfBirth = [dateFormete dateFromString:appDelegate.loginUser.birth_date];
    
    [txtDateOfBirth setDatePickerWithDateFormat:@"dd/MM/yyyy" defaultDate: dateOfBirth ? dateOfBirth : [NSDate date] changed:^(NSDate *date)
    {
        
    }];
    [txtDateOfBirth setMaximumDate:[NSDate date]];
    [txtDateOfBirth setDatePickerMode:UIDatePickerModeDate];
    
    
    // ..... Bar Button .....
    
    if (self.isNavigateFromSidePanel)
    {
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBurgerMenuClicked)]];
        [self.navigationItem setHidesBackButton:YES];
    }
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"notification_white"] style:UIBarButtonItemStylePlain target:self action:@selector(btnNotificationClicked)];
    
    [self loadUserDetailFromServer];
    [self loadProfileDataFromLocal];
}

# pragma mark
# pragma mark - API Events

- (void)loadUserDetailFromServer
{
    [[APIRequest request] userDetails:^(id resonseObject, NSError *error)
     {
         if ([[APIRequest request] isJSONDataValidWithResponse:resonseObject])
         {
             [self loadProfileDataFromLocal];
         }
     }];
}

- (void)loadProfileDataFromLocal
{
    [txtName setText:appDelegate.loginUser.full_name];
    [txtEmail setText:appDelegate.loginUser.email];
    [txtPhoneNo setText:appDelegate.loginUser.phone_number];
    [txtDateOfBirth setText:appDelegate.loginUser.birth_date];
    
    if ([appDelegate.loginUser.address isBlankValidationPassed])
        [txtAddress setText:appDelegate.loginUser.address];
    else
        [txtAddress setText:appDelegate.userCurrentLocation];
}






# pragma mark
# pragma mark - Action Events

- (void)btnNotificationClicked
{
    MINotificationViewController *notificationVC = [[MINotificationViewController alloc] initWithNibName:@"MINotificationViewController" bundle:nil];
    [self.navigationController pushViewController:notificationVC animated:YES];
}

- (IBAction)btnProfileImgClicked:(id)sender
{
//    [self presentImagePickerSource:^(UIImage *image, NSDictionary *info)
//    {
//        if (image)
//        {
//            imgVUserProfile.image = image;
//            imgData = UIImageJPEGRepresentation(image, 0.5);
//        }
//    }];
}

- (IBAction)btnSendClicked:(id)sender
{
    if (![txtName.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankName onView:self];

    else if (![txtEmail.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankEmail onView:self];
    
    else if (![txtEmail.text isValidEmailAddress])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageInvalidEmail onView:self];
    
    else if (![txtPhoneNo.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankMobileNumber onView:self];
    
    else if (![txtAddress.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:CMessageBlankAddress onView:self];
    
    else
    {
        [appDelegate resignKeyboard];
        
        if (imgData)
            [self uploadProfileImage];
        
        [[APIRequest request] editProfileWithName:txtName.text andDateOfBirth:txtDateOfBirth.text andEmail:txtEmail.text andPhoneNumber:txtPhoneNo.text andAddress:txtAddress.text completed:^(id responseObject, NSError *error)
        {
            if (responseObject && !error)
            {
                appDelegate.userCurrentLocation = txtAddress.text;
                
                if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
                {
                    [[APIRequest request] saveLoginUserToLocal:[responseObject valueForKey:CJsonData]];
                    
                    [CustomAlertView iOSAlert:@"" withMessage:[responseObject stringValueForJSON:CJsonMessage] onView:self];
                }
            }
        }];
    }
}

- (void)uploadProfileImage
{
    [[APIRequest request] uploadProfileImage:imgData completed:^(id responseObject, NSError *error)
     {
         if (responseObject && !error)
         {
             if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
                 [CustomAlertView iOSAlert:@"" withMessage:[responseObject stringValueForJSON:CJsonMessage] onView:self];
         }
     }];
}

@end
