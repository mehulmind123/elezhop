//
//  MIMyBillViewController.m
//  EleZhop
//
//  Created by mac-0005 on 6/17/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIMyBillViewController.h"

#import "MIMyBillsPopup.h"

#import "MIHistoryHopEarnedTableViewCell.h"

@interface MIMyBillViewController ()
{
    NSMutableArray *arrMyBills;
    NSInteger iOffset;
}
@end

@implementation MIMyBillViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //..... API Call .....
    
    [self loadAPI];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.screenName = self.title = @"My Bills";
    
    if (self.isNavigateFromSidePanel)
        [self.navigationItem setHidesBackButton:YES];
    
    
    // ..... Register Cell .....
    
    [tblVMyBills registerNib:[UINib nibWithNibName:@"MIHistoryHopEarnedTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIHistoryHopEarnedTableViewCell"];
    tblVMyBills.contentInset = UIEdgeInsetsMake(10, 0, 50, 0);
    
    
    
    // ..... API Call .....
    arrMyBills = [NSMutableArray new];
}

# pragma mark
# pragma mark - UITableview Delegate & Datasource

- (void)loadAPI
{
    iOffset = 0;
    [self loadMyBillFromServer];
}

- (void)loadMyBillFromServer
{
    [[APIRequest request] myBillsWithOffset:iOffset completed:^(id responseObject, NSError *error)
    {
        if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
        {
            NSArray *arrData = [responseObject valueForKey:CJsonData];
            
            if (iOffset == 0)
                [arrMyBills removeAllObjects];
            
            if (arrData.count > 0)
            {
                [arrMyBills addObjectsFromArray:arrData];
                iOffset = [responseObject intForKey:CJsonNewOffset];
                [tblVMyBills reloadData];
            }
        }
        
        if (arrMyBills.count == 0)
            [self displayNoData];
    }];
}

- (void)displayNoData
{
    if (arrMyBills.count == 0)
    {
        [tblVMyBills setHidden:YES];
        [lblNoData setHidden:NO];
    }
    else
    {
        [tblVMyBills setHidden:NO];
        [lblNoData setHidden:YES];
    }
}






# pragma mark
# pragma mark - UITableview Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrMyBills.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MIHistoryHopEarnedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MIHistoryHopEarnedTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    //..... Set Data .....
    
    NSDictionary *dictRaffle = [arrMyBills objectAtIndex:indexPath.row];
    
    cell.lblAddress.text = [dictRaffle stringValueForJSON:@"store_address"];
    cell.lblGiftName.text = [[dictRaffle stringValueForJSON:@"store_name"] capitalizedString];
    cell.lblDate.text = [dictRaffle stringValueForJSON:@"bill_upload_date"];
    cell.lblHops.text = [NSString stringWithFormat:@"€ %@", [dictRaffle stringValueForJSON:@"bill_amount"]];
    [cell.imgVHistoryType setImage:[UIImage imageNamed: [[dictRaffle numberForJson:@"bill_status"] isEqual:@5] ? @"gift_red" : [[dictRaffle numberForJson:@"bill_status"] isEqual:@6] ? @"gift_green" : @"gift_light_red"]];
    
    
    //..... Load More .....
    
    if (indexPath.row == (arrMyBills.count - 1))
        [self loadMyBillFromServer];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MIMyBillsPopup *myBillPopup = [MIMyBillsPopup customMyBillPopup];
    
    [myBillPopup.imgVBill setImageWithURL:[NSURL URLWithString:[[arrMyBills valueForKey:@"bill_image"] objectAtIndex:indexPath.row]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    myBillPopup.center = CScreenCenter;
    
    [myBillPopup.btnClose touchUpInsideClicked:^{
       
        [self dismissPopUp:myBillPopup];
        
    }];
    
    [self presentPopUp:myBillPopup from:PresentTypeCenter shouldCloseOnTouchOutside:YES];
}

@end
