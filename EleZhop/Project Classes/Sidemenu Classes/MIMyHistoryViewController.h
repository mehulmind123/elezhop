//
//  MIMyHistoryViewController.h
//  EleZhop
//
//  Created by mac-0005 on 6/16/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIMyHistoryViewController : ParentViewController
{
    IBOutlet UIButton *btnHopEarned;
    IBOutlet UIButton *btnHopRedeemed;
    IBOutlet UIButton *btnMyRaffles;
    IBOutlet UIButton *btnFilter;
    
    IBOutlet UIView *viewSelectedTab;
    
    IBOutlet UICollectionView *collVTab;
    
    IBOutlet UITableView *tblVMyHistory;
    
    IBOutlet UILabel *lblNoData;
}

@property (nonatomic, assign)NSInteger selectedIndex;

- (IBAction)btnFilterClicked:(id)sender;

@end
