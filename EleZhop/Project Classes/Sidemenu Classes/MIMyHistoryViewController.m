//
//  MIMyHistoryViewController.m
//  EleZhop
//
//  Created by mac-0005 on 6/16/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIMyHistoryViewController.h"
#import "MIRedeemHopsViewController.h"

#import "MIStoreFilterPopup.h"

#import "MIHistoryTabCollectionViewCell.h"
#import "MIHistoryHopEarnedTableViewCell.h"
#import "MIHistoryMyRafflesTableViewCell.h"


//.....TAB .....
#define HopsEarned          1
#define HopsRedeemed        2
#define MyRaffle            3

//.....SORTING .....
#define filterByNone          0
#define filterByApproved      1
#define filterByPending       2


@interface MIMyHistoryViewController ()
{
    NSMutableArray *arrHistoryData;
    
    NSArray *arrTab;
    
    NSArray *arrHopsEarned;
    NSArray *arrHopsRedeemed;
    NSArray *arrMyRaffle;
    
    NSInteger iOffset;
//    NSInteger selectedIndex;
    NSInteger iFilterBy;
    
}
@end

@implementation MIMyHistoryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //..... CALL API .....
    [self loadHistoryData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




# pragma mark
# pragma mark - General Method

- (void)initialize
{
    self.screenName = self.title = @"My History";
    
    // ..... Bar Button .....
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"notification_white"] style:UIBarButtonItemStylePlain target:self action:@selector(btnNotificationClicked)];
    
    
    arrTab = @[@"Hop Earned", @"Hop Redeemed", @"My Raffles"];
    
    
    // ..... UISwipeGestureRecognizer .....
    
    UISwipeGestureRecognizer *swipeleft = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeftToRight:)];
    swipeleft.direction = UISwipeGestureRecognizerDirectionLeft;
    [tblVMyHistory addGestureRecognizer:swipeleft];
    
    UISwipeGestureRecognizer *swiperight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRightToLeft:)];
    swiperight.direction = UISwipeGestureRecognizerDirectionRight;
    [tblVMyHistory addGestureRecognizer:swiperight];
    
    
    // ..... Register Cell .....
    
    [collVTab registerNib:[UINib nibWithNibName:@"MIHistoryTabCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MIHistoryTabCollectionViewCell"];
    
    [tblVMyHistory registerNib:[UINib nibWithNibName:@"MIHistoryHopEarnedTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIHistoryHopEarnedTableViewCell"];
    
    [tblVMyHistory registerNib:[UINib nibWithNibName:@"MIHistoryMyRafflesTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIHistoryMyRafflesTableViewCell"];
    
    tblVMyHistory.contentInset = UIEdgeInsetsMake(10, 0, 140, 0);
    
    
    arrHistoryData = [NSMutableArray new];
}

- (void)hideFilterBtn
{
    btnFilter.hidden = (_selectedIndex == 0) ? NO : YES;
}

# pragma mark
# pragma mark - API Events

- (void)clearData
{
    iOffset = 0;
    [arrHistoryData removeAllObjects];
    [tblVMyHistory reloadData];
}

- (void)loadHistoryData
{
    iOffset = 0;
    [self loadHistoryDataFromServerWithSortBy:filterByNone];
}

- (void)loadHistoryDataFromServerWithSortBy:(NSInteger)sortBy
{
    [[APIRequest request] historyHopsEarnedRedeemedRaffleWithType:_selectedIndex == 0 ? HopsEarned : _selectedIndex == 1 ? HopsRedeemed : MyRaffle andOffset:iOffset andSortBY:sortBy completed:^(id responseObject, NSError *error)
    {
        if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
        {
            if (iOffset == 0)
                [arrHistoryData removeAllObjects];
            
            NSArray *arrData = [responseObject valueForKey:CJsonData];
            
            if (arrData.count > 0)
            {
                [arrHistoryData addObjectsFromArray:arrData];
                iOffset = [responseObject intForKey:CJsonNewOffset];
                [tblVMyHistory reloadData];
            }
        }
        
        [self displayNoData];
        
    }];
}

- (void)loadDataWithFilterType:(NSInteger)filterBy
{
    [self clearData];
    [self loadHistoryDataFromServerWithSortBy:filterBy];
}

- (void)loadMore:(NSIndexPath *)indexPath
{
    if (indexPath.row == (arrHistoryData.count - 1))
        [self loadHistoryDataFromServerWithSortBy:iFilterBy == 2 ? filterByPending : iFilterBy == 1 ? filterByApproved : filterByNone];
}

- (void)displayNoData
{
    if (arrHistoryData.count == 0)
        [lblNoData setHidden:NO];
    else
        [lblNoData setHidden:YES];
}






# pragma mark
# pragma mark - Action Events

- (void)btnNotificationClicked
{
    MINotificationViewController *notificationVC = [[MINotificationViewController alloc] initWithNibName:@"MINotificationViewController" bundle:nil];
    notificationVC.showTabBar = YES;
    [self.navigationController pushViewController:notificationVC animated:YES];
}

- (IBAction)btnFilterClicked:(UIButton *)sender
{
    sender.selected = !sender.selected;
    
    MIStoreFilterPopup *filterPopup = [MIStoreFilterPopup customStoreFilterPopup];
    
    [filterPopup.btnDistance setImage:[UIImage imageNamed:@"filterby_approved"] forState:UIControlStateNormal];
    [filterPopup.btnPopularity setImage:[UIImage imageNamed:@"filter_pending"] forState:UIControlStateNormal];
    
    filterPopup.lblTitle1.text = @"Approved";
    filterPopup.lblTitle2.text = @"Pending";
    
    
    // ..... Click Events .....
    
    [filterPopup.btnClose touchUpInsideClicked:^{
        
        sender.selected = NO;
        [self dismissPopUp:filterPopup];
        iFilterBy = 0;
        [self loadDataWithFilterType:filterByNone];
        
    }];
    
    [filterPopup.btnDistance touchUpInsideClicked:^{
        
        sender.selected = NO;
        [self dismissPopUp:filterPopup];
        iFilterBy = 1;
        [self loadDataWithFilterType:filterByApproved];
        
    }];
    
    [filterPopup.btnPopularity touchUpInsideClicked:^{
        
        sender.selected = NO;
        [self dismissPopUp:filterPopup];
        iFilterBy = 2;
        [self loadDataWithFilterType:filterByPending];
        
    }];
    
    [self presentPopUp:filterPopup from:PresentTypeBottom shouldCloseOnTouchOutside:YES];
}





# pragma mark
# pragma mark - UICollectionview Delagate & Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrTab.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CScreenWidth / 3 , 40);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MIHistoryTabCollectionViewCell *cell = (MIHistoryTabCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"MIHistoryTabCollectionViewCell" forIndexPath:indexPath];
    
    [cell.btnHistoryTab setTitle:[arrTab objectAtIndex:indexPath.item] forState:UIControlStateNormal];
    
    cell.btnHistoryTab.tag = indexPath.item;
    
    if (_selectedIndex == indexPath.item)
    {
        cell.btnHistoryTab.selected = YES;
        cell.viewSelectedTab.hidden = NO;
        
        [UIView animateWithDuration:0.4 animations:^{
            
            [cell.viewSelectedTab setConstraintConstant:CViewX(cell.btnHistoryTab) toAutoLayoutEdge:ALEdgeLeading toAncestor:YES];
            [cell.viewSelectedTab.superview layoutIfNeeded];
            
        }];
    }
    else
    {
        cell.btnHistoryTab.selected = NO;
        cell.viewSelectedTab.hidden = YES;
    }

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.item == _selectedIndex)
        return;
    
    _selectedIndex = indexPath.item;
    
    [self reloadData];
}





# pragma mark
# pragma mark - UITableview Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrHistoryData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (_selectedIndex)
    {
        case 0: // ..... HOPS EARNED .....
        {
            MIHistoryHopEarnedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MIHistoryHopEarnedTableViewCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            // ..... SET DATA .....
            
            NSDictionary *dictHopsEarned = [arrHistoryData objectAtIndex:indexPath.row];
            
            cell.lblDate.text = [dictHopsEarned stringValueForJSON:@"date"];
            cell.lblAddress.text = [dictHopsEarned stringValueForJSON:@"address"];
            cell.lblGiftName.text = [dictHopsEarned stringValueForJSON:@"item_name"];
            
            
            switch ([dictHopsEarned intForKey:@"bill_status"])
            {
                case 5:
                {
                    [cell.imgVHistoryType setImage:[UIImage imageNamed:@"gift_red"]];
                    cell.lblHops.text = @"Bill Under Approval";
                    break;
                }
                    
                case 6:
                {
                    [cell.imgVHistoryType setImage:[UIImage imageNamed:@"gift_green"]];
                    cell.lblHops.text = [NSString stringWithFormat:@"%@ Hops Earned", [dictHopsEarned stringValueForJSON:@"hops_earned"]];
                    break;
                }
                    
                case 7:
                {
                    [cell.imgVHistoryType setImage:[UIImage imageNamed:@"gift_light_red"]];
                    cell.lblHops.text = [NSString stringWithFormat:@"%@ Bill Rejected", [dictHopsEarned stringValueForJSON:@"hops_earned"]];
                    break;
                }
            }
            
            //..... LOAD MORE .....
            
            [self loadMore:indexPath];
            
            return cell;
        }
            break;
            
        case 1: // ..... HOPS REDEEMED .....
        {
            MIHistoryHopEarnedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MIHistoryHopEarnedTableViewCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            // ..... SET DATA .....
            
            NSDictionary *dictHopsRedeemed = [arrHistoryData objectAtIndex:indexPath.row];
            
            [cell.imgVHistoryType setImage:[UIImage imageNamed:@"gift"]];
            cell.lblDate.text = [dictHopsRedeemed stringValueForJSON:@"date"];
            
            cell.lblAddress.text = [NSString stringWithFormat:@"Voucher code: %@ of Worth € %@", [dictHopsRedeemed stringValueForJSON:@"voucher_code"], [dictHopsRedeemed stringValueForJSON:@"gift_price"]];
            
            cell.lblGiftName.text = [dictHopsRedeemed stringValueForJSON:@"item_name"];
            cell.lblHops.text = [NSString stringWithFormat:@"%@\nPoints", [dictHopsRedeemed stringValueForJSON:@"points"]];
            
            
            //..... LOAD MORE .....
            [self loadMore:indexPath];
            
            
            return cell;
        }
            break;
            
        case 2: // ..... MY RAFFLES .....
        {
            MIHistoryMyRafflesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MIHistoryMyRafflesTableViewCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            // ..... SET DATA .....
            
            NSDictionary *dictMyRaffle = [arrHistoryData objectAtIndex:indexPath.row];
            
            cell.lblDate.text = [NSString stringWithFormat:@"Draw Date: %@",[dictMyRaffle stringValueForJSON:@"date"]];
            cell.lblCouponCode.text = [dictMyRaffle stringValueForJSON:@"coupon_code"];
            cell.lblRaffleName.text = [dictMyRaffle stringValueForJSON:@"item_name"];
            
            
            //..... LOAD MORE .....
            [self loadMore:indexPath];
            
            return cell;
        }
            break;
            
        default:
            break;
    }
    
    return nil;
}





# pragma mark
# pragma mark - UISwipeGestureRecognizer Events

- (void)swipeRightToLeft:(UISwipeGestureRecognizer *)gestureLeft
{
    if (_selectedIndex == 0)
        return;
    
    _selectedIndex--;
    
    [tblVMyHistory.layer addAnimation:[appDelegate swipeAnimationWithSubType:kCATransitionFromLeft] forKey:@"fadeTransition"];
    
    [self reloadData];
}

- (void)swipeLeftToRight:(UISwipeGestureRecognizer *)gestureRight
{
    if (_selectedIndex == (arrTab.count - 1))
        return;
    
    _selectedIndex++;
    
    [tblVMyHistory.layer addAnimation:[appDelegate swipeAnimationWithSubType:kCATransitionFromRight] forKey:@"fadeTransition"];
    
    [self reloadData];
}

- (void)reloadData
{
    [self hideFilterBtn];
    [collVTab reloadData];
    
    [self loadDataWithFilterType:filterByNone];
}

@end
