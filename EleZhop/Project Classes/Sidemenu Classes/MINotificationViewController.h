//
//  MINotificationViewController.h
//  EleZhop
//
//  Created by mac-0005 on 6/17/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "ParentViewController.h"

@interface MINotificationViewController : ParentViewController
{
    IBOutlet UITableView *tblVNotification;
    IBOutlet UILabel *lblNoData;
}
@end
