//
//  MINotificationViewController.m
//  EleZhop
//
//  Created by mac-0005 on 6/17/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MINotificationViewController.h"

#import "MINotificationTableViewCell.h"

@interface MINotificationViewController ()
{
    NSMutableArray *arrNotification;
    NSInteger iOffset;
}
@end

@implementation MINotificationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self loadNotification];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




# pragma mark
# pragma mark - General Method

- (void)initialize
{
   self.screenName = self.title = @"Notifications";
    
    if (self.isNavigateFromSidePanel)
        [self.navigationItem setHidesBackButton:YES];
    
    
    //..... REGISTER CELL
    
    [tblVNotification registerNib:[UINib nibWithNibName:@"MINotificationTableViewCell" bundle:nil] forCellReuseIdentifier:@"MINotificationTableViewCell"];
    tblVNotification.contentInset = UIEdgeInsetsMake(10, 0, 50, 0);
    [tblVNotification setRowHeight:UITableViewAutomaticDimension];
    [tblVNotification setEstimatedRowHeight:70];
    
    
    //..... API CALl .....
    arrNotification = [NSMutableArray new];
}

# pragma mark
# pragma mark - API Events

- (void)loadNotification
{
    iOffset = 0;
    [self loadNotificationFromServer];
}

- (void)loadNotificationFromServer
{
    [[APIRequest request] notificationListWithOffset:iOffset completed:^(id responseObject, NSError *error)
    {
        if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
        {
            if (iOffset == 0)
                [arrNotification removeAllObjects];
            
            NSArray *arrData = [responseObject valueForKey:CJsonData];
            
            if (arrData.count > 0)
            {
                [arrNotification addObjectsFromArray:arrData];
                iOffset = [responseObject intForKey:CJsonNewOffset];
                [tblVNotification reloadData];
            }
        }
        
        //..... NO DATA .....
        [self displayNoData];
        
    }];
}

- (void)displayNoData
{
    if (arrNotification.count == 0)
    {
        [tblVNotification setHidden:YES];
        [lblNoData setHidden:NO];
    }
    else
    {
        [tblVNotification setHidden:NO];
        [lblNoData setHidden:YES];
    }
}






# pragma mark
# pragma mark - UITableview Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrNotification.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MINotificationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MINotificationTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //..... SET DATA .....
    
    NSDictionary *dictNotification = [arrNotification objectAtIndex:indexPath.row];
    
    cell.lblMessageTime.text = [dictNotification stringValueForJSON:@"date"];
    cell.lblNotificationMessage.text = [dictNotification stringValueForJSON:@"notification_message"];
    
    
    //..... LOAD MORE .....
    
    if (indexPath.row == (arrNotification.count - 1))
        [self loadNotificationFromServer];
    
    return cell;
}

@end
