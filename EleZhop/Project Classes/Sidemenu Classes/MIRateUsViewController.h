//
//  MIRateUsViewController.h
//  EleZhop
//
//  Created by mac-0005 on 6/17/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIRateUsViewController : ParentViewController
{
    IBOutlet MIGenericRateView *viewRate;
    IBOutlet UITextView *txtVReview;
    IBOutlet UILabel *lblThankyouMsg;
    IBOutlet UIButton *btnSubmit;
    IBOutlet UILabel *lblMessage;
}


@property (nonatomic , strong) NSDictionary *dictData;

- (IBAction)btnSubmitClicked:(id)sender;

@end
