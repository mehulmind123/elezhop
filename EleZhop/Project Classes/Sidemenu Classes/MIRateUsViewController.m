//
//  MIRateUsViewController.m
//  EleZhop
//
//  Created by mac-0005 on 6/17/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIRateUsViewController.h"
#import "MICaptureBillViewController.h"
#import "MICongratulationView.h"

@interface MIRateUsViewController ()

@end

@implementation MIRateUsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




# pragma mark
# pragma mark - General Method

- (void)initialize
{
   self.screenName = self.title = @"Rate Us";
    
    [viewRate setRateViewWithEditing];
    
    if ([[_dictData stringValueForJSON:@"store_name"] isBlankValidationPassed])
    {
        lblMessage.text = [NSString stringWithFormat:@"Your ratings will be helpful to others for further shoppings. Please rate %@", [[_dictData stringValueForJSON:@"store_name"] capitalizedString]];
        lblThankyouMsg.text = [NSString stringWithFormat:@"Thanks... for rating %@", [[_dictData stringValueForJSON:@"store_name"] capitalizedString]];
    }
    
    [txtVReview setPlaceholder:@"Write your review here.."];
}

# pragma mark
# pragma mark - Action Events

- (IBAction)btnSubmitClicked:(id)sender
{
    if (viewRate.rating == 0)
        [CustomAlertView iOSAlert:@"" withMessage:@"Please give rating." onView:self];
    
    else if (![txtVReview.text isBlankValidationPassed])
        [CustomAlertView iOSAlert:@"" withMessage:@"Please write review." onView:self];
    
    else
    {
        [appDelegate resignKeyboard];
        
        [[APIRequest request] uploadBill:[_dictData valueForKey:@"bill_image"] andStoreID:[_dictData stringValueForJSON:@"store_id"] andStoreName:[_dictData stringValueForJSON:@"store_name"] andBillAmount:[_dictData stringValueForJSON:@"bill_amount"] andRate:viewRate.rating andReview:txtVReview.text completed:^(id responseObject, NSError *error)
        {
            if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                [self congratulationPopup:[responseObject valueForKey:CJsonData]];
            else if (!error)
                [CustomAlertView iOSAlert:@"" withMessage:[responseObject stringValueForJSON:CJsonMessage] onView:self];
        }];
    }
}

- (void)congratulationPopup:(NSDictionary *)dictResponse
{
    if (dictResponse)
    {
        MICongratulationView *congratulationPopup = [MICongratulationView customCongratulationPopup];
        
        congratulationPopup.lblCongratulationMessage.text = [dictResponse stringValueForJSON:CJsonMessage];
        
        congratulationPopup.imgVBG.hidden = YES;
        
        [congratulationPopup.btnOk touchUpInsideClicked:^{
            
            [self dismissPopUp:congratulationPopup];
            
            [self.navigationController popToRootViewControllerAnimated:YES];
            
        }];
        
        [self presentPopUp:congratulationPopup from:PresentTypeCenter shouldCloseOnTouchOutside:NO];
    }
}

@end
