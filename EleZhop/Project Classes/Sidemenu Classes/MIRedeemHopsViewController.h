//
//  MIRedeemHopsViewController.h
//  EleZhop
//
//  Created by mac-0005 on 6/16/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIRedeemHopsViewController : ParentViewController
{
    IBOutlet UITableView *tblVRedeemHops;
    IBOutlet UILabel *lblNoData;
}
@end
