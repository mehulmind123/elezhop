//
//  MIRedeemHopsViewController.m
//  EleZhop
//
//  Created by mac-0005 on 6/16/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIRedeemHopsViewController.h"
#import "MIEditProfileViewController.h"
#import "MILuckyDrawViewController.h"


#import "MIRedeemHopsHeader.h"
#import "MICongratulationView.h"


#import "MIRedeemTableViewCell.h"

@interface MIRedeemHopsViewController ()
{
    MIRedeemHopsHeader *redeemHopHeader;
    
    NSInteger iOffset;
    
    NSMutableArray *arrGiftVoucher;
}
@end

@implementation MIRedeemHopsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //..... API Call .....
    
    [self setDataFromLocal];
    [self loadUserDetailFromServer];
    [self loadGiftVoucher];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark
# pragma mark - General Method

- (void)initialize
{
   self.screenName = self.title = @"Redeem Hops";
    
    
    // ..... Bar Button .....
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"notification_white"] style:UIBarButtonItemStylePlain target:self action:@selector(btnNotificationClicked)];
    
    
    // ..... UITable Headerview .....
    
    CGFloat headerHeight = (CScreenWidth * 235) / 375;
    UIView *viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CScreenWidth, headerHeight)];
    redeemHopHeader = [MIRedeemHopsHeader customHopsRedeemHeader];
    
    [redeemHopHeader.btnEdit touchUpInsideClicked:^{
       
        MIEditProfileViewController *editProfileVC = [[MIEditProfileViewController alloc] initWithNibName:@"MIEditProfileViewController" bundle:nil];
        [self.navigationController pushViewController:editProfileVC animated:YES];
        
    }];
    
    [viewHeader addSubview:redeemHopHeader];
    tblVRedeemHops.tableHeaderView = viewHeader;
    
    
    // ..... Register Cell .....
    
    [tblVRedeemHops registerNib:[UINib nibWithNibName:@"MIRedeemTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIRedeemTableViewCell"];
    [tblVRedeemHops setContentInset:UIEdgeInsetsMake(0, 0, 50, 0)];
    
    arrGiftVoucher = [NSMutableArray new];
}

# pragma mark
# pragma mark - API Method

- (void)setDataFromLocal
{
    [redeemHopHeader.imgVUser sd_setImageWithURL:[NSURL URLWithString:appDelegate.loginUser.user_profile_image]];
    
    redeemHopHeader.lblUserName.text = appDelegate.loginUser.full_name;
    redeemHopHeader.lblHopsEarned.text = appDelegate.loginUser.hops_earned;
    redeemHopHeader.lblHopsRedeemed.text = appDelegate.loginUser.hops_redeemed;
    redeemHopHeader.lblBillsUploaded.text = appDelegate.loginUser.bills_uploaded;
}

- (void)loadGiftVoucher
{
    iOffset = 0;
    [self loadGiftVoucherListFromServer];
}

- (void)loadGiftVoucherListFromServer
{
    [[APIRequest request] giftVoucherListWithOffset:iOffset completed:^(id responseObject, NSError *error)
    {
        if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
        {
            if (iOffset == 0)
                [arrGiftVoucher removeAllObjects];
            
            NSArray *arrData = [responseObject valueForKey:CJsonData];
            
            if (arrData.count > 0)
            {
                [arrGiftVoucher addObjectsFromArray:arrData];
                iOffset = [responseObject intForKey:CJsonNewOffset];
                [tblVRedeemHops reloadData];
            }
        }
        
        if (arrGiftVoucher.count == 0)
        {
            [lblNoData setHidden:NO];
            [tblVRedeemHops reloadData];
            [tblVRedeemHops setScrollEnabled:NO];
        }
        else
        {
            [lblNoData setHidden:YES];
            [tblVRedeemHops setScrollEnabled:YES];
        }
    }];
}

- (void)loadUserDetailFromServer
{
    [[APIRequest request] userDetails:^(id resonseObject, NSError *error)
    {
        if (resonseObject && !error)
        {
            if ([[APIRequest request] isJSONDataValidWithResponse:resonseObject])
            {
                [self setDataFromLocal];
            }
        }
    }];
}







# pragma mark
# pragma mark - Action Events

- (void)btnNotificationClicked
{
    MINotificationViewController *notificationVC = [[MINotificationViewController alloc] initWithNibName:@"MINotificationViewController" bundle:nil];
    notificationVC.showTabBar = YES;
    [self.navigationController pushViewController:notificationVC animated:YES];
}




# pragma mark
# pragma mark - UITableview Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrGiftVoucher.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MIRedeemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MIRedeemTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    //..... Set Data .....
    
    NSDictionary *dictGiftVoucher = [arrGiftVoucher objectAtIndex:indexPath.row];
    
    cell.lblGiftName.text = [dictGiftVoucher stringValueForJSON:@"gift_name"];
    cell.lblVoucherPrice.text = [NSString stringWithFormat:@"Voucher worth € %@", [dictGiftVoucher stringValueForJSON:@"gift_cost"]];
    cell.lblHops.text = [NSString stringWithFormat:@"%@ Hops", [dictGiftVoucher stringValueForJSON:@"gift_hops_need"]];
    
    
    //..... Load More .....
    
    if (indexPath.row == (arrGiftVoucher.count - 1))
        [self loadGiftVoucherListFromServer];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dictGiftVoucher = [arrGiftVoucher objectAtIndex:indexPath.row];
    
    [self alertWithTitle:@"" message:CMessageRedeemGiftVoucher okTitle:@"Yes" cancleTitle:@"No" handler:^(NSString *title)
    {
        if ([title isEqualToString:@"Yes"])
        {
            [[APIRequest request] getGiftVoucher:[dictGiftVoucher stringValueForJSON:@"gift_id"] completed:^(id responseObject, NSError *error)
            {
                if (responseObject && !error)
                {
                    if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
                    {
                        MICongratulationView *congratulationPopup = [MICongratulationView customCongratulationPopup];
                        
                        NSDictionary *dictData = [responseObject valueForKey:CJsonData];
                        
                        congratulationPopup.lblCongratulationMessage.text = [dictData stringValueForJSON:CJsonMessage];
                        
                        redeemHopHeader.lblHopsRedeemed.text = [dictData stringValueForJSON:@"hops_redeemed"];
                        
                        appDelegate.loginUser.hops_redeemed = [dictData stringValueForJSON:@"hops_redeemed"];
                        [[[Store sharedInstance] mainManagedObjectContext] save];
                        
                        
                        //..... Click Events .....
                        
                        [congratulationPopup.btnOk touchUpInsideClicked:^{
                            
                            [self dismissPopUp:congratulationPopup];
                            
                        }];
                        
                        [self presentPopUp:congratulationPopup from:PresentTypeCenter shouldCloseOnTouchOutside:YES];
                    }
                    else if (![[APIRequest request] isJSONDataValidWithResponse:responseObject])
                    {
                        [CustomAlertView iOSAlert:@"" withMessage:[responseObject stringValueForJSON:CJsonMessage] onView:self];
                    }
                }
            }];
        }
    }];
}

@end
