//
//  MISettingsViewController.m
//  EleZhop
//
//  Created by mac-0005 on 6/17/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MISettingsViewController.h"

@interface MISettingsViewController ()

@end

@implementation MISettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




# pragma mark
# pragma mark - General Method

- (void)initialize
{
   self.screenName = self.title = @"Settings";
    
    viewNotification.layer.shadowColor = CRGB(224, 224, 224).CGColor;
    viewNotification.layer.shadowOffset = CGSizeMake(0, 2);
    viewNotification.layer.shadowRadius = 1;
    viewNotification.layer.shadowOpacity = 1.0;
    
    
    switchNotification.transform = CGAffineTransformMakeScale(.6, .6);
    [switchNotification setOn:appDelegate.loginUser.notification ? YES : NO];
}

# pragma mark
# pragma mark - Action Events

- (IBAction)switchNotificationChanged:(UISwitch *)sender
{
    [[APIRequest request] notificationSetting:[sender isOn] ? 1 : 0 completed:^(id responseObject, NSError *error)
    {
        if (responseObject && !error)
        {
            if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
            {
                appDelegate.loginUser.notification = [responseObject intForKey:@"status"];
                [[[Store sharedInstance] mainManagedObjectContext] save];
            }
        }
    }];
}

@end
