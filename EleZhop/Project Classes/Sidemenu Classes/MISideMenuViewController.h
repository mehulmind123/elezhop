//
//  MISideMenuViewController.h
//  EleZhop
//
//  Created by mac-0005 on 6/21/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "ParentViewController.h"

@interface MISideMenuViewController : ParentViewController<TWTSideMenuViewControllerDelegate>
{
    IBOutlet UIImageView *imgVUserProfile;
    
    IBOutlet UIView *viewEdit;
    
    IBOutlet UILabel *lblUserName;
    IBOutlet UILabel *lblHopsEarned;
    IBOutlet UILabel *lblHopsRedeemed;
    IBOutlet UILabel *lblBillsUploaded;
    
    IBOutlet UITableView *tblVSideMenu;
}

- (IBAction)btnCloseClicked:(id)sender;
- (IBAction)btnEditClicked:(id)sender;
- (IBAction)btnLogoutClicked:(id)sender;

@end
