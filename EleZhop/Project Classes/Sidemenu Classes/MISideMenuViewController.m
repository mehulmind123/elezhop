//
//  MISideMenuViewController.m
//  EleZhop
//
//  Created by mac-0005 on 6/21/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MISideMenuViewController.h"
#import "MIRedeemHopsViewController.h"
#import "MIMyBillViewController.h"
#import "MIMyHistoryViewController.h"
#import "MISettingsViewController.h"
#import "MINotificationViewController.h"
#import "MIContactUsViewController.h"
#import "MIRateUsViewController.h"
#import "MIEditProfileViewController.h"
#import "MILoginViewController.h"
#import "MICMSViewController.h"
#import "MIWebViewController.h"

#import "MISideMenuTableViewCell.h"

@interface MISideMenuViewController ()
{
    NSArray *arrSideMenu;
}
@end

@implementation MISideMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self intitialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    imgVUserProfile.layer.cornerRadius = CViewHeight(imgVUserProfile) / 2;;
    imgVUserProfile.layer.borderWidth = 3;
    imgVUserProfile.layer.borderColor = ColorNavigationbar.CGColor;
    imgVUserProfile.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




# pragma mark
# pragma mark - General Method

- (void)intitialize
{
    [self.navigationController setNavigationBarHidden:YES];
    
    [appDelegate.sideMenuViewController setDelegate:self];
    
    [viewEdit.layer setCornerRadius:CViewHeight(viewEdit) / 2];
    [viewEdit.layer setShadowColor:[UIColor blackColor].CGColor];
    [viewEdit.layer setShadowOpacity:0.5];
    [viewEdit.layer setShadowRadius:1.0];
    [viewEdit.layer setShadowOffset:CGSizeZero];
    
    arrSideMenu = [[NSArray alloc] initWithObjects:
                                    @{@"image":@"sidemenu_reedem_hops", @"title":@"Redeem Hops"},
                                    @{@"image":@"sidemenu_mybill", @"title":@"My Bills"},
                                    @{@"image":@"sidemenu_history", @"title":@"History"},
                                    @{@"image":@"sidemenu_setting", @"title":@"Settings"},
                                    @{@"image":@"sidemenu_notification", @"title":@"Notifications"},
                                    @{@"image":@"sidemenu_contactus", @"title":@"Contact Us"},
                                    @{@"image":@"sidemenu_faqs", @"title":@"FAQ's"},
                                    @{@"image":@"sidemenu_rateus", @"title":@"Rate Us"},
                                    @{@"image":@"sidemenu_terms_condition", @"title":@"Terms & Conditions"},
                                    @{@"image":@"sidemenu_privacy_policy", @"title":@"Privacy Policy"},
                                    nil];
    
    [tblVSideMenu registerNib:[UINib nibWithNibName:@"MISideMenuTableViewCell" bundle:nil] forCellReuseIdentifier:@"MISideMenuTableViewCell"];
    [tblVSideMenu setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
}





# pragma mark
# pragma mark - API Method

- (void)loadData
{
    if ([UIApplication userId])
    {
        [self setDataFromLocal];
        [self loadUserDetailFromServer];
    }
}

- (void)setDataFromLocal
{
    [imgVUserProfile sd_setImageWithURL:[NSURL URLWithString:appDelegate.loginUser.user_profile_image]];
    lblUserName.text = appDelegate.loginUser.full_name;
    lblHopsEarned.text = appDelegate.loginUser.hops_earned;
    lblHopsRedeemed.text = appDelegate.loginUser.hops_redeemed;
    lblBillsUploaded.text = appDelegate.loginUser.bills_uploaded;
}


- (void)loadUserDetailFromServer
{
    [[APIRequest request] userDetails:^(id resonseObject, NSError *error)
     {
         if ([[APIRequest request] isJSONDataValidWithResponse:resonseObject])
         {
             [self setDataFromLocal];
         }
     }];
}






# pragma mark
# pragma mark - Action Events

- (IBAction)btnCloseClicked:(id)sender
{
    [appDelegate.tabBarViewController.tabBarView btnTabBarClicked:appDelegate.tabBarViewController.tabBarView.btnTab3];
    [appDelegate.sideMenuViewController setMainViewController:appDelegate.tabBarViewController animated:YES closeMenu:YES];
}

- (IBAction)btnEditClicked:(id)sender
{
    MIEditProfileViewController *editProfileVC = [[MIEditProfileViewController alloc] initWithNibName:@"MIEditProfileViewController" bundle:nil];
    editProfileVC.showTabBar = NO;
    editProfileVC.isNavigateFromSidePanel = YES;
    [appDelegate.sideMenuViewController setMainViewController:[UINavigationController navigationControllerWithRootViewController:editProfileVC] animated:YES closeMenu:YES];
}

- (IBAction)btnLogoutClicked:(id)sender
{
    [UIAlertController alertControllerWithTwoButtonsWithStyle:UIAlertControllerStyleAlert title:@"" message:CMessageLogout firstButton:@"No" firstHandler:nil secondButton:@"Yes" secondHandler:^(UIAlertAction *action)
    {
        [appDelegate logoutUser];
    }
    inView:self];
}





# pragma mark
# pragma mark - UITableview Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrSideMenu.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MISideMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MISideMenuTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    NSDictionary *dictSidemenu = [arrSideMenu objectAtIndex:indexPath.row];
    
    cell.imgVSidemenuItem.image = [UIImage imageNamed:[dictSidemenu stringValueForJSON:@"image"]];
    cell.lblSidemenuTitle.text = [dictSidemenu stringValueForJSON:@"title"];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row)
    {
        case 0:
        {
            // ..... Redeem Hops .....
            
            [appDelegate.tabBarViewController setSelectedIndex:3];
            [appDelegate.sideMenuViewController setMainViewController:appDelegate.tabBarViewController animated:YES closeMenu:YES];
            
        }
        break;
            
        case 1:
        {
            // ..... My Bills .....
            
            [appDelegate.tabBarViewController setSelectedIndex:4];
            [appDelegate.sideMenuViewController setMainViewController:appDelegate.tabBarViewController animated:YES closeMenu:YES];
            
        }
        break;
            
        case 2:
        {
            // ..... History .....
            
            [appDelegate.tabBarViewController.tabBarView btnTabBarClicked:appDelegate.tabBarViewController.tabBarView.btnTab2];
            [appDelegate.sideMenuViewController setMainViewController:appDelegate.tabBarViewController animated:YES closeMenu:YES];
            
        }
        break;
            
        case 3:
        {
            // ..... Settings .....
            
            MISettingsViewController *settingsVC = [[MISettingsViewController alloc] initWithNibName:@"MISettingsViewController" bundle:nil];
            [appDelegate.sideMenuViewController setMainViewController:[UINavigationController navigationControllerWithRootViewController:settingsVC] animated:YES closeMenu:YES];
            
        }
        break;
            
        case 4:
        {
            // ..... Notifications .....
            
            [appDelegate.tabBarViewController setSelectedIndex:5];
            [appDelegate.sideMenuViewController setMainViewController:appDelegate.tabBarViewController animated:YES closeMenu:YES];
            
        }
        break;
            
        case 5:
        {
            // ..... Contact Us .....
            
            MIContactUsViewController *cmsVC = [[MIContactUsViewController alloc] initWithNibName:@"MIContactUsViewController" bundle:nil];
            [appDelegate.sideMenuViewController setMainViewController:[UINavigationController navigationControllerWithRootViewController:cmsVC] animated:YES closeMenu:YES];
            
        }
        break;
            
        case 6:
        {
            // ..... FAQ's .....
            
            [appDelegate openUrlInWebviewWithURL:appDelegate.loginUser.faq];
            
        }
        break;
            
        case 7:
        {
            // ..... Rate Us .....
            [appDelegate openURL:AppStoreURL];
            
        }
        break;
            
        case 8:
        {
            // ..... Terms & Conditions .....
            
            MICMSViewController *cmsVC = [[MICMSViewController alloc] initWithNibName:@"MICMSViewController" bundle:nil];
            cmsVC.cmsType = TermsCondition;
            [appDelegate.sideMenuViewController setMainViewController:[UINavigationController navigationControllerWithRootViewController:cmsVC] animated:YES closeMenu:YES];
            
        }
        break;
            
        case 9:
        {
            // ..... Privacy Policy .....
            
            MICMSViewController *cmsVC = [[MICMSViewController alloc] initWithNibName:@"MICMSViewController" bundle:nil];
            cmsVC.cmsType = PrivacyPolicy;
            [appDelegate.sideMenuViewController setMainViewController:[UINavigationController navigationControllerWithRootViewController:cmsVC] animated:YES closeMenu:YES];
            
        }
        break;
    }
}





# pragma mark
# pragma mark - TWTSideMenuViewControllerDelegate

- (void)sideMenuViewControllerWillOpenMenu:(TWTSideMenuViewController *)sideMenuViewController
{
    [self loadData];
}

- (void)sideMenuViewControllerWillCloseMenu:(TWTSideMenuViewController *)sideMenuViewController
{
    [appDelegate postNotificationForUpdateHomeData];
}

@end
