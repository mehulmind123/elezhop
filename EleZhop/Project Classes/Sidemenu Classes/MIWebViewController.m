//
//  MIWebViewController.m
//  Engage
//
//  Created by mac-00014 on 8/18/17.
//  Copyright © 2017 Jignesh-0007. All rights reserved.
//

#import "MIWebViewController.h"
#import <WebKit/WebKit.h>

@interface MIWebViewController ()<WKUIDelegate,WKNavigationDelegate>
{
    WKWebView *webView;
}
@end

@implementation MIWebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initliaze];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




# pragma mark
# pragma mark - General Method

- (void)initliaze
{
    self.title = @"Web";
    
    NSURLRequest *request = [NSURLRequest requestWithURL:self.iObject];
    
    self.navigationController.navigationBar.tintColor = CRGB(63, 124, 255);
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:self action:@selector(barBtnDoneClicked:)];
    
    WKWebViewConfiguration *theConfiguration = [[WKWebViewConfiguration alloc] init];
    
    webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, CScreenWidth, CScreenHeight - 64) configuration:theConfiguration];

    webView.navigationDelegate = self;

    [webView loadRequest:request];
    
    [self.view addSubview:webView];
    
    [self startLoadingAnimationInView:self.view];
}

- (IBAction)barBtnDoneClicked:(UIBarButtonItem*)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}





# pragma mark
# pragma mark - WKWebViewDelegate

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    if (navigationAction.navigationType == WKNavigationTypeLinkActivated)
        decisionHandler(WKNavigationActionPolicyAllow); //..... Allow click event of webpage.
    else
        decisionHandler(WKNavigationActionPolicyAllow);
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation
{
    [self stopLoadingAnimationInView:self.view];
}

- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error
{
    [self stopLoadingAnimationInView:self.view];
}


@end
