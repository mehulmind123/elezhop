//
//  MIWinnerViewController.m
//  EleZhop
//
//  Created by mac-0005 on 6/16/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIWinnerViewController.h"

#import "MIWinnerTableViewCell.h"

@interface MIWinnerViewController ()
{
    NSMutableArray *arrWinnerList;
    NSInteger iOffset;
}
@end

@implementation MIWinnerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark
# pragma mark - General Method

- (void)initialize
{
   self.screenName = self.title = @"Winner";
    
    //....
    [lblNoData setHidden:YES];

   
    // ..... Register Cell .....
    [tblVWinner registerNib:[UINib nibWithNibName:@"MIWinnerTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIWinnerTableViewCell"];
    tblVWinner.contentInset = UIEdgeInsetsMake(10, 0, 10, 0);
    
    
    // ..... API Call .....
    arrWinnerList = [[NSMutableArray alloc] init];
    [self loadAPI];
}

# pragma mark
# pragma mark - API Event

- (void)loadAPI
{
    iOffset = 0;
    [self loadRaffleWinnerListFromServer];
}


- (void)loadRaffleWinnerListFromServer
{
    if (![self.iObject isBlankValidationPassed])
        return;
    
    [[APIRequest request] raffleWinnerListWithRaffleID:self.iObject andOffset:iOffset completed:^(id responseObject, NSError *error)
    {
        if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
        {
            if (iOffset == 0) [arrWinnerList removeAllObjects];
            
            NSArray *arr = [responseObject valueForKey:CJsonData];
            
            if (arr.count > 0)
            {
                [arrWinnerList addObjectsFromArray:arr];
                iOffset = [responseObject intForKey:@"new_offset"];
                [tblVWinner reloadData];
            }
        }
        
        if (arrWinnerList.count == 0) // ..... No Data .....
        {
            [tblVWinner setHidden:YES];
            [lblNoData setHidden:NO];
        }
        else
        {
            [tblVWinner setHidden:NO];
            [lblNoData setHidden:YES];
        }
        
    }];
}




# pragma mark
# pragma mark - UITableview Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrWinnerList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MIWinnerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MIWinnerTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    //..... Set Data .....
    
    NSDictionary *dictWinner = [arrWinnerList objectAtIndex:indexPath.row];
    
    [cell.imgVUserProfile sd_setImageWithURL:[NSURL URLWithString:[dictWinner stringValueForJSON:@"user_image"]] placeholderImage:nil];
    cell.lbluserName.text = [dictWinner stringValueForJSON:@"user_name"];
    cell.lblCouponCode.text = [dictWinner stringValueForJSON:@"raffle_code"];
    
    if (indexPath.row == (arrWinnerList.count - 1))
        [self loadRaffleWinnerListFromServer];
    
    return cell;
}

@end
