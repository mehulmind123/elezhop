//
//  MIAllCategoryViewController.h
//  EleZhop
//
//  Created by mac-0005 on 6/19/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^getSelectedAddress)(NSDictionary *);


@interface MIAllCategoryViewController : ParentViewController<GMSPlacePickerViewControllerDelegate>
{
    IBOutlet UITextField *txtSearchStore;
    IBOutlet UIView *viewSelectedCategory;
    IBOutlet UIImageView *imgVCategory;
    IBOutlet UILabel *lblCategoryName;
    IBOutlet UILabel *lblNoData;
    IBOutlet UICollectionView *colVCategoryList;
    IBOutlet UITableView *tblVStoreList;
    IBOutlet UIButton *btnFilter;
    IBOutlet UIButton *btnCategoryDropDown;
}


@property (nonatomic , copy) getSelectedAddress selectedAddress;
@property (nonatomic, strong) NSDictionary *dictCategory;
@property (nonatomic, strong) NSString *address;
//@property double userLatitude;
//@property double userLongitude;


- (IBAction)btnCategoryClicked:(id)sender;
- (IBAction)btnFilterClicked:(id)sender;

@end
