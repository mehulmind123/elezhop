//
//  MIAllCategoryViewController.m
//  EleZhop
//
//  Created by mac-0005 on 6/19/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIAllCategoryViewController.h"
#import "MIStoreDetailViewController.h"
#import "MIEditProfileViewController.h"

#import "MIStoreFilterPopup.h"
#import "MINavAddressView.h"

#import "MICategoryCollectionViewCell.h"
#import "MIStoreTableViewCell.h"



#define sortByDistance      1
#define sortByPopularity    2


@interface MIAllCategoryViewController ()
{
    NSArray *arrCategory;
    NSMutableArray *arrStoreList;
    
    MINavAddressView *navAddress;
    
    NSInteger iOffset;
    
    NSURLSessionDataTask *sessionDataTask;
    
    BOOL isSortByPopularity;
    
    NSString *categoryID;
}
@end

@implementation MIAllCategoryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"All Category Screen";
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [appDelegate displayLocationAlert];
    navAddress.lblAddress.text = appDelegate.userAddress;
    [self loadCategoryListFromServer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




# pragma mark
# pragma mark - General Method

- (void)initialize
{

    [self setNavigationTitleView];
    
    txtSearchStore.layer.cornerRadius = CViewHeight(txtSearchStore) / 2;
    [txtSearchStore setLeftImage:[UIImage imageNamed:@"search"] withSize:CGSizeMake(35, 15)];
    [txtSearchStore setPlaceHolderColor:ColorLightGray_B5B4B1];
    [txtSearchStore addTarget:self action:@selector(textfieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    
    viewSelectedCategory.layer.shadowColor = [[UIColor blackColor] CGColor];
    viewSelectedCategory.layer.shadowOffset = CGSizeMake(1, 2);
    viewSelectedCategory.layer.shadowOpacity = 0.2f;
    viewSelectedCategory.layer.shadowRadius = 1.0f;
    
    
    
    // ..... Right Bar Button .....
    
    UIBarButtonItem *btnNotification = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"notification_white"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(btnNotificationClicked)];
    
    UIBarButtonItem *btnUserProfile = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"user"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(btnUserProfileClicked)];
    
    [self.navigationItem setRightBarButtonItems:@[btnUserProfile, btnNotification]];
    
    
    
    
    // ..... REGISTER CELL
    
    [colVCategoryList registerNib:[UINib nibWithNibName:@"MICategoryCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MICategoryCollectionViewCell"];
    colVCategoryList.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
    
    [tblVStoreList registerNib:[UINib nibWithNibName:@"MIStoreTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIStoreTableViewCell"];
    tblVStoreList.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
    
    
    
    // ..... API CALL
    
    [appDelegate animationForHideAndShow:colVCategoryList andOtion:UIViewAnimationOptionTransitionCrossDissolve andIsHide:YES];
    btnCategoryDropDown.selected = YES;
    arrStoreList = [[NSMutableArray alloc] init];
    [self displaySelectedCategory:_dictCategory];
    [self loadStoreListFromServerWithSearchText:@"" andSortBy:sortByDistance];
}

- (void)setNavigationTitleView
{
    navAddress = [MINavAddressView viewFromXib:NO];
    
    navAddress.lblAddress.text = _address;
    
    [navAddress.btnAddress touchUpInsideClicked:^{
       
        CLLocationCoordinate2D center = CLLocationCoordinate2DMake(appDelegate.current_lat, appDelegate.current_long);
        CLLocationCoordinate2D northEast = CLLocationCoordinate2DMake(center.latitude + 0.001,
                                                                      center.longitude + 0.001);
        CLLocationCoordinate2D southWest = CLLocationCoordinate2DMake(center.latitude - 0.001,
                                                                      
                                                                      center.longitude - 0.001);
        GMSCoordinateBounds *viewport = [[GMSCoordinateBounds alloc] initWithCoordinate:northEast
                                                                             coordinate:southWest];
        
        GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:viewport];
        
        GMSPlacePickerViewController *placePicker =
        [[GMSPlacePickerViewController alloc] initWithConfig:config];
        placePicker.delegate = self;
        
        [[UISearchBar appearance] setBarStyle:UIBarStyleBlack];
        
        [self presentViewController:placePicker animated:YES completion:nil];
        
    }];
    
    self.navigationItem.titleView = navAddress;
}

- (void)displaySelectedCategory:(NSDictionary *)dictCategory
{
    categoryID = [dictCategory stringValueForJSON:@"category_id"];
    [imgVCategory sd_setImageWithURL:[NSURL URLWithString:[dictCategory stringValueForJSON:@"category_image"]]];
    lblCategoryName.text = [dictCategory stringValueForJSON:@"category_name"];
}

# pragma mark
# pragma mark - GMSPlace Picker Delegate

- (void)placePicker:(GMSPlacePickerViewController *)viewController didPickPlace:(GMSPlace *)place
{
    // Dismiss the place picker, as it cannot dismiss itself.
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place attributions %@", place.attributions.string);
    
    navAddress.lblAddress.text = place.formattedAddress;
    appDelegate.userAddress = place.formattedAddress;
    
    appDelegate.current_lat = place.coordinate.latitude;
    appDelegate.current_long = place.coordinate.longitude;
    
    NSMutableDictionary *dictAddress = [NSMutableDictionary new];
    
    [dictAddress setValue:[NSNumber numberWithDouble:place.coordinate.latitude] forKey:@"latitude"];
    [dictAddress setValue:[NSNumber numberWithDouble:place.coordinate.longitude] forKey:@"longitude"];
    [dictAddress setValue:place.formattedAddress forKey:@"address"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NotificationAddressUpdate object:nil userInfo:dictAddress];
    
    [self loadAPI];
}

- (void)placePickerDidCancel:(GMSPlacePickerViewController *)viewController
{
    // Dismiss the place picker, as it cannot dismiss itself.
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"No place selected");
}





# pragma mark
# pragma mark - API Methods

- (void)loadCategoryListFromServer
{
    [[APIRequest request] categroyList:^(id resonseObject, NSError *error)
     {
         if (resonseObject && !error)
         {
             if ([[APIRequest request] isJSONDataValidWithResponse:resonseObject])
             {
                 NSArray *arrData = [resonseObject valueForKey:CJsonData];
                 
                 if (arrData.count > 0)
                 {
                     arrCategory = arrData;
                     [colVCategoryList reloadData];
                 }
             }
         }
     }];
}

- (void)loadAPI
{
    iOffset = 0;
    categoryID = [_dictCategory stringValueForJSON:@"category_id"];
    [self loadStoreListFromServerWithSearchText:@"" andSortBy:sortByDistance];
}

- (void)cancelRequest
{
    if (sessionDataTask && sessionDataTask.state == NSURLSessionTaskStateRunning)
        [sessionDataTask cancel];
}

- (void)loadStoreListFromServerWithSearchText:(NSString *)searchTxt andSortBy:(NSInteger)sortBy
{
    //..... Cancel API Request .....
    [self cancelRequest];
    
    sessionDataTask = [[APIRequest request] storeListWithCategoryID:categoryID andOffset:iOffset andSearchText:searchTxt andSortBY:sortBy completed:^(id responseObject, NSError *error)
     {
         if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
         {
             if (iOffset == 0)
                 [arrStoreList removeAllObjects];
             
             [lblNoData setHidden:YES];
             
             NSArray *arrData = [responseObject valueForKey:CJsonData];
             
             if (arrData.count > 0)
             {
                 [arrStoreList addObjectsFromArray:arrData];
                 iOffset = [responseObject intForKey:CJsonNewOffset];
                 [tblVStoreList reloadData];
             }
         }
         else
             isSortByPopularity = NO;
         
         
         // ..... Display No Data .....
         [self displayNoData];

     }];
}

- (void)displayNoData
{
    if (arrStoreList.count == 0)
    {
        [tblVStoreList setHidden:YES];
        [lblNoData setHidden:NO];
    }
    else
    {
        [tblVStoreList setHidden:NO];
        [lblNoData setHidden:YES];
    }
}

- (void)clearStoreData
{
    iOffset = 0;
    [arrStoreList removeAllObjects];
    [tblVStoreList reloadData];
}

- (void)loadDataWithSorting:(NSInteger)sortBy
{
    [self clearStoreData];
    [self loadStoreListFromServerWithSearchText:txtSearchStore.text andSortBy:sortBy];
}

-(void)textfieldDidChange:(UITextField *)textField
{
    [self clearStoreData];
    [self loadStoreListFromServerWithSearchText:textField.text andSortBy:isSortByPopularity ? sortByPopularity : sortByDistance];
}






# pragma mark
# pragma mark - Action Events

- (void)btnUserProfileClicked
{
    if ([UIApplication userId])
    {
        MIEditProfileViewController *editProfileVC = [[MIEditProfileViewController alloc] initWithNibName:@"MIEditProfileViewController" bundle:nil];
        editProfileVC.showTabBar = NO;
        [self.navigationController pushViewController:editProfileVC animated:YES];
    }
    else
        [appDelegate displayAlert:self];
}

- (void)btnNotificationClicked
{
    if ([UIApplication userId])
    {
        MINotificationViewController *notificationVC = [[MINotificationViewController alloc] initWithNibName:@"MINotificationViewController" bundle:nil];
        notificationVC.showTabBar = YES;
        [self.navigationController pushViewController:notificationVC animated:YES];
    }
    else
        [appDelegate displayAlert:self];
}

- (IBAction)btnCategoryClicked:(UIButton *)sender
{
    sender.selected = !sender.selected;
    
    [appDelegate animationForHideAndShow:colVCategoryList andOtion:UIViewAnimationOptionTransitionCrossDissolve andIsHide:sender.selected];
}

- (IBAction)btnFilterClicked:(UIButton *)sender
{
    sender.selected = !sender.selected;
    
    MIStoreFilterPopup *filterPopup = [MIStoreFilterPopup customStoreFilterPopup];
    
    [filterPopup.btnPopularity touchUpInsideClicked:^{
        
        sender.selected = NO;
        [self dismissPopUp:filterPopup];
        
        
        isSortByPopularity = YES;
        [self loadDataWithSorting:sortByPopularity];
        
    }];
    
    [filterPopup.btnDistance touchUpInsideClicked:^{
        
        sender.selected = NO;
        [self dismissPopUp:filterPopup];
        
        isSortByPopularity = NO;
        [self loadDataWithSorting:sortByDistance];
        
    }];
    
    [filterPopup.btnClose touchUpInsideClicked:^{
        
        sender.selected = NO;
        [self dismissPopUp:filterPopup];
        
        if (!filterPopup.btnDistance.selected)
        {
            isSortByPopularity = NO;
            [self loadDataWithSorting:sortByDistance];
        }
        
    }];
    
    [self.navigationController presentPopUp:filterPopup from:PresentTypeBottom shouldCloseOnTouchOutside:YES];
}





# pragma mark
# pragma mark - UICollectionview Delagate & Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrCategory.count;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 7.0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((CScreenWidth * 61) / 375 , (CScreenHeight * 92) / 667);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MICategoryCollectionViewCell *cell = (MICategoryCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"MICategoryCollectionViewCell" forIndexPath:indexPath];
    
    
    //..... Set Data .....
    
    NSDictionary *dictCategory = [arrCategory objectAtIndex:indexPath.item];
    
    [cell.imgVCategory sd_setImageWithURL:[NSURL URLWithString:[dictCategory stringValueForJSON:@"category_image"]] placeholderImage:nil];
    cell.lblCategoryName.text = [dictCategory stringValueForJSON:@"category_name"];
    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [appDelegate animationForHideAndShow:colVCategoryList andOtion:UIViewAnimationOptionTransitionCrossDissolve andIsHide:YES];
    btnCategoryDropDown.selected = YES;
    
    NSDictionary *dictCategory = [arrCategory objectAtIndex:indexPath.row];
    
    //..... Category Already Selected .....
    if ([categoryID isEqualToString:[dictCategory stringValueForJSON:@"category_id"]])
        return;
    
    [self displaySelectedCategory:arrCategory[indexPath.row]];
    [self loadDataWithSorting:isSortByPopularity ? sortByPopularity : sortByDistance];
}





# pragma mark
# pragma mark - UITableview Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrStoreList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MIStoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MIStoreTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    // ..... Set Data .....
    
    NSDictionary *dictStore = [arrStoreList objectAtIndex:indexPath.row];
    
    [cell.imgVStore sd_setImageWithURL:[NSURL URLWithString:[dictStore stringValueForJSON:@"store_image"]] placeholderImage:nil];
    cell.lblStoreName.text = [[dictStore stringValueForJSON:@"store_name"] capitalizedString];
    cell.viewRate.rating = [[dictStore stringValueForJSON:@"store_average_rate"] floatValue];
    cell.lblRate.text = [NSString stringWithFormat:@"(%.1f/5)", [[dictStore stringValueForJSON:@"store_average_rate"] floatValue]];
    cell.lblStoreType.text = [[dictStore numberForJson:@"store_type"] boolValue] ? @"Web" : @"Instore";
    cell.lblStoreType.backgroundColor = [[dictStore numberForJson:@"store_type"] boolValue] ? ColorGreen_299712 : ColorBlue_3184BC;
    
    cell.btnDistance.hidden = NO;
    
    if ([[dictStore numberForJson:@"store_type"] boolValue])
        cell.btnDistance.hidden = YES;
    else
        [cell.btnDistance setTitle:[NSString stringWithFormat:@"%@ KM", [dictStore stringValueForJSON:@"store_distance"]] forState:UIControlStateNormal];
    
    
    [cell.btnOffers setTitle:[NSString stringWithFormat:@"%@ Offers", [dictStore stringValueForJSON:@"offers_count"]] forState:UIControlStateNormal];
    
    [cell.btnRuffleTicket setTitle:[NSString stringWithFormat:@"%@ Raffle Ticket", [dictStore stringValueForJSON:@"raffle_tickets_count"]] forState:UIControlStateNormal];
    
    [cell.btnBounties setTitle:[NSString stringWithFormat:@"%@ € Hops", [dictStore stringValueForJSON:@"hops_count"]] forState:UIControlStateNormal];
    
    
    
    // ..... Load More .....
    
    if (indexPath.row == (arrStoreList.count - 1))
        [self loadStoreListFromServerWithSearchText:txtSearchStore.text andSortBy:isSortByPopularity ? sortByPopularity : sortByDistance];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MIStoreDetailViewController *storeDetailVC = [[MIStoreDetailViewController alloc] initWithNibName:@"MIStoreDetailViewController" bundle:nil];
    storeDetailVC.showTabBar = YES;
    storeDetailVC.dictStore = [arrStoreList objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:storeDetailVC animated:YES];
}

@end
