//
//  MIBillPreviewViewController.h
//  EleZhop
//
//  Created by Mac-0009 on 07/09/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIBillPreviewViewController : ParentViewController
{
    IBOutlet UIImageView *imgVBill;
}


@property (nonatomic, assign) BOOL isFromSearch;
@property (nonatomic, assign) BOOL isBillUploaded;
@property (nonatomic, strong) NSData *imgData;

@end
