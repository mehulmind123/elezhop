//
//  MIBillPreviewViewController.m
//  EleZhop
//
//  Created by Mac-0009 on 07/09/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIBillPreviewViewController.h"
#import "MIUploadBillViewController.h"

@interface MIBillPreviewViewController ()

@end

@implementation MIBillPreviewViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = self.title = @"Preview";
    
    imgVBill.image = [UIImage imageWithData:_imgData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

# pragma mark
# pragma mark - Action Events

- (IBAction)btnConfirmClicked:(id)sender
{
    MIUploadBillViewController *uploadBillVC = [[MIUploadBillViewController alloc] initWithNibName:@"MIUploadBillViewController" bundle:nil];
    uploadBillVC.isBillUploaded = YES;
    uploadBillVC.isFromSearch = _isFromSearch;
    uploadBillVC.imgData = _imgData;
    [uploadBillVC setShowTabBar:YES];
    [self.navigationController pushViewController:uploadBillVC animated:YES];
}

@end
