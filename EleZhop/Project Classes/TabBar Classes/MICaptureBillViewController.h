//
//  MICaptureBillViewController.h
//  EleZhop
//
//  Created by mac-0005 on 6/17/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MICaptureBillViewController : ParentViewController
{
    IBOutlet UIButton *btnCheck;
    IBOutlet UIButton *btnCaptureBill;
}

@property (nonatomic, assign) BOOL isFromSearch;


- (IBAction)btnCaptureBill:(id)sender;

@end
