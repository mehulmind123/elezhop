//
//  MICaptureBillViewController.m
//  EleZhop
//
//  Created by mac-0005 on 6/17/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MICaptureBillViewController.h"
#import "MIUploadBillViewController.h"
#import "MICongratulationView.h"
#import "MIUploadPopUp.h"
#import "MIRateUsViewController.h"
#import "MIBillPreviewViewController.h"

@interface MICaptureBillViewController ()
{
}
@end

@implementation MICaptureBillViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




# pragma mark
# pragma mark - General Method

- (void)initialize
{
    [self.navigationController setNavigationBarHidden:NO];
    
    self.screenName = self.title = @"Capture Bill";
    
    [btnCaptureBill setConstraintConstant: Is_iPhone_5 || Is_iPhone_4 ? 12 : Is_iPhone_6_PLUS ? 40 : 30 toAutoLayoutEdge:ALEdgeTop toAncestor:NO];
}

# pragma mark
# pragma mark - Action Events

- (IBAction)btnCheckClicked:(UIButton *)sender
{
    sender.selected = !sender.selected;
}

- (IBAction)btnCaptureBill:(id)sender
{
    [self presentImagePickerSource:^(UIImage *image, NSDictionary *info)
     {
         if (image)
         {
             image = [image fixOrientation];
             
             if (btnCheck.selected)
             {
                 [CUserDefaults setValue:[NSNumber numberWithBool:YES] forKey:UserDefaultGotIT];
                 [CUserDefaults synchronize];
             }
             
             MIBillPreviewViewController *billPreviewVC = [[MIBillPreviewViewController alloc] initWithNibName:@"MIBillPreviewViewController" bundle:nil];
             [billPreviewVC setShowTabBar:YES];
             billPreviewVC.isBillUploaded = YES;
             billPreviewVC.isFromSearch = _isFromSearch;
             billPreviewVC.imgData = UIImageJPEGRepresentation(image, 0.8);
             [self.navigationController pushViewController:billPreviewVC animated:YES];
         }
     } cancelHandler:^(UIAlertAction *action)
     {
//         [appDelegate.tabBarViewController.tabBarView btnTabBarClicked:appDelegate.tabBarViewController.tabBarView.btnTab3];
     }];
}

@end
