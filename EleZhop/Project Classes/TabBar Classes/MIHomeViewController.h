//
//  MIHomeViewController.h
//  EleZhop
//
//  Created by mac-0005 on 6/20/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIHomeViewController : ParentViewController<GMSPlacePickerViewControllerDelegate>
{
    IBOutlet UITextField *txtSearch;
    IBOutlet UICollectionView *colVHeader;
    IBOutlet UIButton *btnCreditDebitCard;
    IBOutlet UIView *viewUploadBill;
    IBOutlet UIButton *btnUploadBill;
    IBOutlet UIView *viewEarnRedeemBillUploaded;
    IBOutlet UICollectionView *colVCategory;
    IBOutlet UIView *viewBottom;
    IBOutlet UILabel *lblNextRaffle;
    IBOutlet UILabel *lblHopsEarned;
    IBOutlet UILabel *lblHopsRedeemed;
    IBOutlet UILabel *lblBillsUploaded;
    IBOutlet UIButton *btnRaffle;
}

@property (nonatomic, assign) BOOL isLogin;

- (IBAction)btnSearchClicked:(id)sender;
- (IBAction)btnDebitCreditCardClicked:(id)sender;
- (IBAction)btnUploadBillClicked:(id)sender;
- (IBAction)btnPrizeClicked:(id)sender;

@end
