//
//  MIHomeViewController.m
//  EleZhop
//
//  Created by mac-0005 on 6/20/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIHomeViewController.h"
#import "MIEditProfileViewController.h"
#import "MIAllCategoryViewController.h"
#import "MIUploadBillViewController.h"
#import "MICaptureBillViewController.h"
#import "MILuckyDrawViewController.h"
#import "MILoginViewController.h"

#import "MINavAddressView.h"

#import "MIHomeHeaderCollectionViewCell.h"
#import "MICategoryCollectionViewCell.h"



#define CItemSpacing    (CScreenWidth * 10 / 375)
#define CItemWidth      (CScreenWidth * 325 / 375)
#define CItemHeight     (CItemWidth * 120 / 325)


@interface MIHomeViewController ()
{
    NSArray *arrFeatures;
    NSArray *arrCategory;

    NSTimer *timer;
    NSInteger visibleIndex;
    
    MINavAddressView *navAddress;
    
    NSString *creditDebitCardURL;
}
@end

@implementation MIHomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Home";
    [self initialize];
    
    [UIApplication applicationDidBecomeActive:^{
        
        [appDelegate getCurrentLocation];
        navAddress.lblAddress.text = appDelegate.userAddress;
        
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [appDelegate displayLocationAlert];
    
    navAddress.lblAddress.text = appDelegate.userAddress;
    
    [self loadData];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    [viewEarnRedeemBillUploaded hideByHeight: [UIApplication userId] ? NO : YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // .... Remove Notification Observer
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NotificationHomeDataUpdate object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NotificationCurrentLocation object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}






# pragma mark
# pragma mark - General Method

- (void)initialize
{
    [self setNavigationTitleView];
    
    txtSearch.layer.cornerRadius = CViewHeight(txtSearch) / 2;
    [txtSearch setLeftImage:[UIImage imageNamed:@"search"] withSize:CGSizeMake(35, 15)];
    [txtSearch setPlaceHolderColor:ColorLightGray_B5B4B1];
    
    
    viewUploadBill.layer.cornerRadius = 3;
    viewUploadBill.layer.masksToBounds = YES;
    
    
    // .... Add Notification Observer
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatecurrentLocation:) name:NotificationCurrentLocation object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateHomeData) name:NotificationHomeDataUpdate object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateAddress:) name:NotificationAddressUpdate object:nil];
    
    
    
    // ..... Bar Button .....
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"ticket"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(btnRaffleClicked)];
    
    UIBarButtonItem *btnUser = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"user"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(btnUserClicked)];
    
    UIBarButtonItem *btnNotification = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"notification_white"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(btnNotificationClicked)];
    
    [self.navigationItem setRightBarButtonItems:@[btnUser, btnNotification]];
    
    
    // ..... Register Cell .....
    
    [colVHeader registerNib:[UINib nibWithNibName:@"MIHomeHeaderCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MIHomeHeaderCollectionViewCell"];

    [colVCategory registerNib:[UINib nibWithNibName:@"MICategoryCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MICategoryCollectionViewCell"];
    colVCategory.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
    
    
    // ..... API Call .....
    
    [self loadFeaturedImagesFromServer];
}

- (void)setNavigationTitleView
{
    navAddress = [MINavAddressView viewFromXib:NO];
    
    navAddress.lblAddress.text = appDelegate.userAddress;
    
    [navAddress.btnAddress touchUpInsideClicked:^{
        
        CLLocationCoordinate2D center = CLLocationCoordinate2DMake(appDelegate.current_lat, appDelegate.current_long);
        CLLocationCoordinate2D northEast = CLLocationCoordinate2DMake(center.latitude + 0.001,
                                                                      center.longitude + 0.001);
        CLLocationCoordinate2D southWest = CLLocationCoordinate2DMake(center.latitude - 0.001,
                                                                      
                                                                      center.longitude - 0.001);
        GMSCoordinateBounds *viewport = [[GMSCoordinateBounds alloc] initWithCoordinate:northEast
                                                                             coordinate:southWest];
        
        GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:viewport];
        
        GMSPlacePickerViewController *placePicker =
        [[GMSPlacePickerViewController alloc] initWithConfig:config];
        placePicker.delegate = self;
        
        [[UISearchBar appearance] setBarStyle:UIBarStyleBlack];
        
        [self presentViewController:placePicker animated:YES completion:nil];
        
    }];
    
    self.navigationItem.titleView = navAddress;
}

- (void)updatecurrentLocation:(NSNotification *)notification
{
    if (notification.userInfo.allKeys.count > 0)
    {
        navAddress.lblAddress.text = [notification.userInfo valueForKey:@"CurrentLocation"];
    }
}

- (void)updateAddress:(NSNotification *)notification
{
    if (notification.userInfo.allKeys.count > 0)
    {
        appDelegate.current_lat = [[notification.userInfo valueForKey:@"latitude"] doubleValue];
        appDelegate.current_long = [[notification.userInfo valueForKey:@"longitude"] doubleValue];
        navAddress.lblAddress.text = [notification.userInfo valueForKey:@"address"];
    }
}

# pragma mark
# pragma mark - GMSPlace Picker Delegate

- (void)placePicker:(GMSPlacePickerViewController *)viewController didPickPlace:(GMSPlace *)place
{
    // Dismiss the place picker, as it cannot dismiss itself.
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place placeID %@", place.placeID);
    NSLog(@"Place attributions %@", place.attributions.string);
    
    [CUserDefaults setValue:[NSNumber numberWithDouble:place.coordinate.latitude] forKey:UserDefaultCurrentLatitude];
    [CUserDefaults setValue:[NSNumber numberWithDouble:place.coordinate.longitude] forKey:UserDefaultCurrentLongitude];
    
    appDelegate.current_lat = place.coordinate.latitude;
    appDelegate.current_long = place.coordinate.longitude;
    appDelegate.userAddress = place.formattedAddress;
    
    navAddress.lblAddress.text = place.formattedAddress;
}

- (void)placePickerDidCancel:(GMSPlacePickerViewController *)viewController
{
    // Dismiss the place picker, as it cannot dismiss itself.
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"No place selected");
}





# pragma mark
# pragma mark - Auto Scroll

- (void)autoScroll
{
    visibleIndex++;
    
    if (visibleIndex == [arrFeatures count])
        visibleIndex = 0;
    
    float pageWidth = CItemWidth + CItemSpacing;
    [colVHeader setContentOffset:CGPointMake(pageWidth * visibleIndex, 0) animated:YES];
}






# pragma mark
# pragma mark - API Methods

- (void)loadFeaturedImagesFromServer
{
    [[APIRequest request] featuredOffer:^(id resonseObject, NSError *error)
    {
        if (resonseObject && !error)
        {
            if ([[APIRequest request] isJSONDataValidWithResponse:resonseObject])
            {
                NSArray *arrData = [resonseObject valueForKey:CJsonData];
                
                if (arrData.count > 0)
                {
                    [colVHeader hideByHeight:NO];
                    arrFeatures = arrData;
                    
                    visibleIndex = 0;
                    timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(autoScroll) userInfo:nil repeats:YES];
                    [colVHeader reloadData];
                }
            }
            else
            {
                [colVHeader hideByHeight:YES];
                [btnCreditDebitCard setConstraintConstant:16 toAutoLayoutEdge:ALEdgeTop toAncestor:YES];
            }
        }
    }];
}

- (void)loadData
{
    [self loadCategoryListFromServer];
    
    [viewEarnRedeemBillUploaded hideByHeight: [UIApplication userId] ? NO : YES];
    
    if ([UIApplication userId])
    {
        [self setDataFromLocal];
        [self loadUserDetailFromServer];
    }
}

- (void)updateHomeData
{
    [self loadData];
}

- (void)loadCategoryListFromServer
{
    [[APIRequest request] categroyList:^(id resonseObject, NSError *error)
    {
        if (resonseObject && !error)
        {
            if ([[APIRequest request] isJSONDataValidWithResponse:resonseObject])
            {
                NSArray *arrData = [resonseObject valueForKey:CJsonData];
                
                //..... SET NEXT RAFFLE TIME
                
                if ([resonseObject stringValueForJSON:@"next_raffle"])
                {
                    [viewBottom hideByHeight:NO];
                    
                    NSString *strRaffleTimeRemain = [NSString stringWithFormat:@"%@ hours remaining", [resonseObject stringValueForJSON:@"next_raffle"]];
                    
                    NSString *strStatic = @"Exclusive Prizes to Win in Weekly Raffles Next Raffle:";
                    
                    lblNextRaffle.text = [NSString stringWithFormat:@"%@ %@", strStatic, strRaffleTimeRemain];
                    
                    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:lblNextRaffle.text];
                    
                    NSRange range = [lblNextRaffle.text rangeOfString:strRaffleTimeRemain options:NSCaseInsensitiveSearch];
                    
                    if (range.location != NSNotFound)
                    {
                        [attributedString addAttribute:NSForegroundColorAttributeName value:ColorNavigationbar range:NSMakeRange(range.location, strRaffleTimeRemain.length)];
                        [attributedString addAttribute:NSFontAttributeName value:CFontGilroyRegular(14) range:NSMakeRange(range.location, strRaffleTimeRemain.length)];
                    }
                    
                    lblNextRaffle.attributedText = attributedString;
                    
                    
                    [btnCreditDebitCard setTitle:[resonseObject stringValueForJSON:@"button_name"] forState:UIControlStateNormal];
                    creditDebitCardURL = [resonseObject stringValueForJSON:@"button_url"];
                }
                else
                {
                    [viewBottom hideByHeight:YES];
                }
                    
                if (arrData.count > 0)
                {
                    arrCategory = arrData;
                    [colVCategory reloadData];
                }
            }
        }
    }];
}

- (void)loadUserDetailFromServer
{
    [[APIRequest request] userDetails:^(id resonseObject, NSError *error)
     {
         if (resonseObject && !error)
         {
             if ([[APIRequest request] isJSONDataValidWithResponse:resonseObject])
             {
                 [self setDataFromLocal];
             }
         }
     }];
}

- (void)setDataFromLocal
{
    lblHopsEarned.text = appDelegate.loginUser.hops_earned;
    lblHopsRedeemed.text = appDelegate.loginUser.hops_redeemed;
    lblBillsUploaded.text = appDelegate.loginUser.bills_uploaded;
}





# pragma mark
# pragma mark - Action Events

- (void)btnRaffleClicked
{
    [self pushToRaffleScreen];
}

- (void)btnUserClicked
{
    if ([UIApplication userId])
    {
        MIEditProfileViewController *editProfileVC = [[MIEditProfileViewController alloc] initWithNibName:@"MIEditProfileViewController" bundle:nil];
        editProfileVC.showTabBar = NO;
        [self.navigationController pushViewController:editProfileVC animated:YES];
    }
    else
        [appDelegate displayAlert:self];
}

- (void)btnNotificationClicked
{
    if ([UIApplication userId])
    {
        MINotificationViewController *notificationVC = [[MINotificationViewController alloc] initWithNibName:@"MINotificationViewController" bundle:nil];
        notificationVC.showTabBar = YES;
        [self.navigationController pushViewController:notificationVC animated:YES];
    }
    else
        [appDelegate displayAlert:self];
}

- (IBAction)btnSearchClicked:(id)sender
{
    MIUploadBillViewController *uploadBillVC = [[MIUploadBillViewController alloc] initWithNibName:@"MIUploadBillViewController" bundle:nil];
    uploadBillVC.isBillUploaded = YES;
    uploadBillVC.isFromSearch = YES;
    [uploadBillVC setShowTabBar:YES];
    [self.navigationController pushViewController:uploadBillVC animated:YES];
}

- (IBAction)btnDebitCreditCardClicked:(id)sender
{
    if ([creditDebitCardURL isBlankValidationPassed])
        [appDelegate openUrlInWebviewWithURL:creditDebitCardURL];
    else
        [CustomAlertView iOSAlert:@"" withMessage:@"There is not URL available right now" onView:self];
}

- (IBAction)btnUploadBillClicked:(id)sender
{
    if ([UIApplication userId])
    {
        MICaptureBillViewController *captureBillVC = [[MICaptureBillViewController alloc] initWithNibName:@"MICaptureBillViewController" bundle:nil];
        [captureBillVC setShowTabBar:YES];
        [self.navigationController pushViewController:captureBillVC animated:YES];
    }
    else
        [appDelegate displayAlert:self];
}

- (IBAction)btnPrizeClicked:(id)sender
{
    [self pushToRaffleScreen];
}

- (void)pushToRaffleScreen
{
    if ([UIApplication userId])
    {
        MILuckyDrawViewController *luckyDrawVC = [[MILuckyDrawViewController alloc] initWithNibName:@"MILuckyDrawViewController" bundle:nil];
        [self.navigationController pushViewController:luckyDrawVC animated:YES];
    }
    else
        [appDelegate displayAlert:self];
}






# pragma mark
# pragma mark - UICollectionview Delagate & Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([collectionView isEqual:colVHeader])
        return arrFeatures.count;
    return arrCategory.count;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return CItemSpacing;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if ([collectionView isEqual:colVHeader])
    {
        CGFloat inset = (CScreenWidth - CItemWidth) / 2;
        return UIEdgeInsetsMake(0, inset, 0, inset);
    }
    return UIEdgeInsetsZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:colVHeader])
        return CGSizeMake(CItemWidth, CItemHeight);
    return CGSizeMake((CScreenWidth * 61) / 375 , (CScreenHeight * 92) / 667);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:colVHeader])
    {
        MIHomeHeaderCollectionViewCell *cell = (MIHomeHeaderCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"MIHomeHeaderCollectionViewCell" forIndexPath:indexPath];
        
        // ..... Set Data .....
        
        [cell.imgVHeader sd_setImageWithURL:[NSURL URLWithString:[arrFeatures objectAtIndex:indexPath.row]] placeholderImage:nil];
        
        return cell;
    }
    
    MICategoryCollectionViewCell *cell = (MICategoryCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"MICategoryCollectionViewCell" forIndexPath:indexPath];
    
    NSDictionary *dictCategory = [arrCategory objectAtIndex:indexPath.item];
    
    // ..... Set Data .....
    
    [cell.imgVCategory sd_setImageWithURL:[NSURL URLWithString:[dictCategory stringValueForJSON:@"category_image"]]];
    [cell.imgVCategory setShowActivityIndicatorView:YES];
    cell.lblCategoryName.text = [dictCategory stringValueForJSON:@"category_name"];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:colVCategory])
    {
        MIAllCategoryViewController *allCategoryVC = [[MIAllCategoryViewController alloc] initWithNibName:@"MIAllCategoryViewController" bundle:nil];
        allCategoryVC.showTabBar = YES;
        allCategoryVC.dictCategory = arrCategory[indexPath.row];
        allCategoryVC.address = navAddress.lblAddress.text;
        [self.navigationController pushViewController:allCategoryVC animated:YES];
    }
}

@end
