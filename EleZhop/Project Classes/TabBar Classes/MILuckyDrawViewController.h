//
//  MILuckyDrawViewController.h
//  EleZhop
//
//  Created by mac-0005 on 6/19/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+animatedGIF.h"

@interface MILuckyDrawViewController : ParentViewController
{
    IBOutlet UILabel *lblNextDrawDate;
    IBOutlet UILabel *lblCountDown;
    IBOutlet UILabel *lblUserName;
    IBOutlet UILabel *lblCoupon;
    
    IBOutlet UIImageView *imgVUserProfile;
    IBOutlet UIImageView *imgVTime;

    IBOutlet UICollectionView *collVPrize;
    IBOutlet UICollectionView *collVCoupon;
    IBOutlet UICollectionView *collVWinner;
    
    IBOutlet UIView *viewPrizes;
    IBOutlet UIView *viewYourCoupon;
    IBOutlet UIView *viewWinner;
    IBOutlet UIView *viewGradient;
    
    IBOutlet UIButton *btnSeeHow;
    
    IBOutlet UILabel *lblRaffleTime;
}


- (IBAction)btnSeeHowClicked:(id)sender;

@end
