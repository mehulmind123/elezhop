//
//  MILuckyDrawViewController.m
//  EleZhop
//
//  Created by mac-0005 on 6/19/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MILuckyDrawViewController.h"
#import "MIWinnerViewController.h"
#import "MINotificationViewController.h"

#import "MIPrizesTableViewCell.h"
#import "MIPrizeCollectionViewCell.h"
#import "MICouponCollectionViewCell.h"
#import "MIRaffleWinnerCollectionViewCell.h"

@interface MILuckyDrawViewController ()
{
    NSArray *arrPrizes;
    NSArray *arrWinner;
    NSArray *arrCoupon;
    
    NSString *raffleID;
    NSString *strSeeHow;
    NSTimer *timer;
    NSInteger visibleIndex;
}
@end

@implementation MILuckyDrawViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self loadRaffleDataFromServer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




# pragma mark
# pragma mark - General Method

- (void)initialize
{
   self.screenName = self.title = @"Lucky Draw";
    
    viewPrizes.layer.shadowColor = viewYourCoupon.layer.shadowColor = ColorNavigationbar.CGColor;
    viewPrizes.layer.shadowOffset = viewYourCoupon.layer.shadowOffset = CGSizeMake(0, 2);
    viewPrizes.layer.shadowRadius = viewYourCoupon.layer.shadowRadius = 1;
    viewPrizes.layer.shadowOpacity = viewYourCoupon.layer.shadowOpacity = 0.2;
    viewPrizes.layer.masksToBounds = viewYourCoupon.layer.masksToBounds = NO;
    
    
    viewWinner.layer.shadowColor = [UIColor blackColor].CGColor;
    viewWinner.layer.shadowOffset = CGSizeMake(0, 2);
    viewWinner.layer.shadowRadius = 1;
    viewWinner.layer.shadowOpacity = 0.1;
    
    
    imgVUserProfile.layer.cornerRadius = CViewHeight(imgVUserProfile) / 2;;
    imgVUserProfile.layer.borderWidth = 2;
    imgVUserProfile.layer.borderColor = ColorNavigationbar.CGColor;
    imgVUserProfile.layer.masksToBounds = YES;
    
    
    // ..... Set GIF image in imageview .....
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"Counter" withExtension:@"gif"];
    imgVTime.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    
    viewWinner.hidden = YES;
    btnSeeHow.layer.cornerRadius = 4;
    
    visibleIndex = 0;
    timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(autoScroll) userInfo:nil repeats:YES];
    
    
    // ..... Bar Button .....
    
    UIBarButtonItem *btnWinner = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"winner_white"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(btnWinnerClicked)];
    
    UIBarButtonItem *btnNotification = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"notification_white"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(btnNotificationClicked)];
    
    [self.navigationItem setRightBarButtonItems:@[btnNotification, btnWinner]];
    
    
    // ..... Register Cell .....
    
    [collVPrize registerNib:[UINib nibWithNibName:@"MIPrizeCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MIPrizeCollectionViewCell"];
    
    [collVCoupon registerNib:[UINib nibWithNibName:@"MICouponCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MICouponCollectionViewCell"];
    
    [collVWinner registerNib:[UINib nibWithNibName:@"MIRaffleWinnerCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MIRaffleWinnerCollectionViewCell"];
}

# pragma mark
# pragma mark - Auto Scroll

- (void)autoScroll
{
    visibleIndex++;
    
    if (visibleIndex == [arrPrizes count])
        visibleIndex = 0;
    
    [collVPrize setContentOffset:CGPointMake(CScreenWidth * visibleIndex, 0) animated:YES];
}





# pragma mark
# pragma mark - API Events

- (void) loadRaffleDataFromServer
{
    [[APIRequest request] raffle:^(id responseObject, NSError *error)
    {
        if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
        {
            NSDictionary *dictData = [responseObject valueForKey:CJsonData];
            
            raffleID = [dictData stringValueForJSON:@"raffle_id"];
            lblCountDown.text = [dictData stringValueForJSON:@"count_down"];
            lblNextDrawDate.text = [NSString stringWithFormat:@"%@", [dictData stringValueForJSON:@"current_draw"]];
            lblRaffleTime.text = [dictData stringValueForJSON:@"count_down"];
            
            strSeeHow = [dictData stringValueForJSON:@"raffle_see_how"];
            
            [collVWinner setHidden:YES];
            [viewGradient setHidden:NO];
            
            if ([dictData intForKey:@"is_winner"] == 1) //..... Winner .....
            {
                [collVWinner flashScrollIndicators];
                [collVWinner setHidden:NO];
                [viewGradient setHidden:YES];
                
                arrWinner = [dictData valueForKey:@"raffle_winner"];
                [collVWinner reloadData];
            }
            
            arrPrizes = [dictData valueForKey:@"prizes"];
            arrCoupon = [dictData valueForKey:@"raffle_code"];
            [collVPrize reloadData];
            [collVCoupon reloadData];
        }
    }];
}







# pragma mark
# pragma mark - Action Events

- (void)btnWinnerClicked
{
    MIWinnerViewController *winnerVC = [[MIWinnerViewController alloc] initWithNibName:@"MIWinnerViewController" bundle:nil];
    [winnerVC setIObject:raffleID];
    [self.navigationController pushViewController:winnerVC animated:YES];
}

- (void)btnNotificationClicked
{
    MINotificationViewController *notificationVC = [[MINotificationViewController alloc] initWithNibName:@"MINotificationViewController" bundle:nil];
    [self.navigationController pushViewController:notificationVC animated:YES];
}

- (IBAction)btnSeeHowClicked:(id)sender
{
    if (strSeeHow)
        [appDelegate openUrlInWebviewWithURL:strSeeHow];
}





# pragma mark
# pragma mark - UICollectionview Delagate & Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [collectionView isEqual:collVPrize] ? arrPrizes.count : [collectionView isEqual:collVWinner] ? arrWinner.count : arrCoupon.count;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return [collectionView isEqual:collVPrize] || [collectionView isEqual:collVWinner] ? 0 : 10;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return [collectionView isEqual:collVPrize] || [collectionView isEqual:collVWinner] ? 0 : 7;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [collectionView isEqual:collVPrize] ? CGSizeMake(CScreenWidth , 130) : [collectionView isEqual:collVWinner] ? CGSizeMake(CScreenWidth , 78) : CGSizeMake(103 , 42);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (collectionView.tag)
    {
        case 1: //..... PRIZE
        {
            MIPrizeCollectionViewCell *cell = (MIPrizeCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"MIPrizeCollectionViewCell" forIndexPath:indexPath];
            
            NSDictionary *dictPrizes = [arrPrizes objectAtIndex:indexPath.row];
            
            cell.lblItemName.text = [dictPrizes stringValueForJSON:@"prize_name"];
            cell.lblItemDesc.text = [dictPrizes stringValueForJSON:@"prize_description"];
            [cell.imgVItem sd_setImageWithURL:[NSURL URLWithString:[dictPrizes stringValueForJSON:@"prize_image"]] placeholderImage:nil];
            
            return cell;
        }
        break;
            
        case 2: //..... WINNER
        {
            MIRaffleWinnerCollectionViewCell *cell = (MIRaffleWinnerCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"MIRaffleWinnerCollectionViewCell" forIndexPath:indexPath];
            
            NSDictionary *dictWinner = [arrWinner objectAtIndex:indexPath.item];
            
            [cell.imgVRaffleWinner sd_setImageWithURL:[NSURL URLWithString:[dictWinner stringValueForJSON:@"user_image"]] placeholderImage:nil];
            cell.lblUserName.text = [dictWinner stringValueForJSON:@"user_name"];
            cell.lblCouponCode.text = [dictWinner stringValueForJSON:@"raffle_code"];
            cell.lblWinnerNumber.text = [NSString stringWithFormat:@"Winner: %@", [dictWinner stringValueForJSON:@"winner_rank"]];
            
            return cell;
        }
        break;
            
            
        case 3: //..... COUPON CODE
        {
            MICouponCollectionViewCell *cell = (MICouponCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"MICouponCollectionViewCell" forIndexPath:indexPath];
            
            cell.lblCouponCode.text = [arrCoupon objectAtIndex:indexPath.item];
            
            return cell;
        }
        break;
    }
    
    return nil;
}

@end
