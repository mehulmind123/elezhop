//
//  MIStoreDetailViewController.h
//  EleZhop
//
//  Created by mac-0005 on 6/19/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIStoreDetailViewController : ParentViewController
{
    IBOutlet UITableView *tblVStoreDetail;
    IBOutlet UILabel *lblNoData;
}

@property (nonatomic , strong) NSDictionary *dictStore;

@end
