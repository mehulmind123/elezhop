//
//  MIStoreDetailViewController.m
//  EleZhop
//
//  Created by mac-0005 on 6/19/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIStoreDetailViewController.h"

#import "MIStoreDetailHeader.h"
#import "MIStoreOfferPopup.h"
#import "MIMyBillsPopup.h"

#import "MIDetailOfferTableViewCell.h"
#import "MIDetailStoreTableViewCell.h"
#import "MIDetailReviewTableViewCell.h"

@interface MIStoreDetailViewController ()
{
    MIStoreDetailHeader *headerStoreDetail;
    
    NSInteger iOffset;
    
    NSMutableArray *arrStoreDetail;
    
    NSURLSessionDataTask *sessionDataTask;
    
    float fAverageRate;
}
@end

@implementation MIStoreDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Store Detail Screen";
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





# pragma mark
# pragma mark - General Methods

- (void)initialize
{
    // ..... UITableview Header View .....
    
    CGFloat headerHeight = (CScreenWidth * 305) / 375;
    UIView *viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CScreenWidth, headerHeight)];
    headerStoreDetail = [MIStoreDetailHeader customStoreDetailHeader];
    
    [headerStoreDetail.btnOffers addTarget:nil action:@selector(btnHeaderTabClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [headerStoreDetail.btnDetails addTarget:nil action:@selector(btnHeaderTabClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [headerStoreDetail.btnReviews addTarget:nil action:@selector(btnHeaderTabClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self btnHeaderTabClicked:headerStoreDetail.btnOffers];
    
    [viewHeader addSubview:headerStoreDetail];
    tblVStoreDetail.tableHeaderView = viewHeader;
    
    
    if (_dictStore)
    {
        self.title = [[_dictStore stringValueForJSON:@"store_name"] capitalizedString];
        [self setSoreDetailData:_dictStore];
    }
    
    
    // ..... Register Cell .....
    
    [tblVStoreDetail registerNib:[UINib nibWithNibName:@"MIDetailOfferTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIDetailOfferTableViewCell"];
    
    [tblVStoreDetail registerNib:[UINib nibWithNibName:@"MIDetailStoreTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIDetailStoreTableViewCell"];
    
    [tblVStoreDetail registerNib:[UINib nibWithNibName:@"MIDetailReviewTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIDetailReviewTableViewCell"];
    
    tblVStoreDetail.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
    tblVStoreDetail.rowHeight = UITableViewAutomaticDimension;
    
    
    // ..... API Call .....
    
    arrStoreDetail = [NSMutableArray new];
    [self loadStoreDetailFromServer];
    [self btnHeaderTabClicked:headerStoreDetail.btnOffers];
}

# pragma mark
# pragma mark - Action Events

- (void)btnHeaderTabClicked:(UIButton *)sender
{
    if (sender.selected)
        return;
    
    headerStoreDetail.btnOffers.selected = headerStoreDetail.btnDetails.selected = headerStoreDetail.btnReviews.selected = NO;
    
    sender.selected = YES;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [headerStoreDetail.viewTabSelected setConstraintConstant:CViewX(sender) toAutoLayoutEdge:ALEdgeLeading toAncestor:YES];
        [headerStoreDetail.viewTabSelected.superview layoutIfNeeded];
        
    }];
    
    [self clearData];
    
    switch (sender.tag)
    {
        case 1:
        {
            [self loadStoreOfferListFromServer];
            break;
        }
            
        case 2:
        {
            [lblNoData setHidden:YES];
            [tblVStoreDetail setScrollEnabled:YES];
            break;
        }
            
        case 3:
        {
            [self loadStoreReviewListFromServer];
            break;
        }
            
        default:
            break;
    }
    
    [tblVStoreDetail reloadData];
}






# pragma mark
# pragma mark - API Method

- (void)clearData
{
    iOffset = 0;
    [arrStoreDetail removeAllObjects];
    [tblVStoreDetail reloadData];
}

- (void)cancelRequest
{
    if (sessionDataTask && sessionDataTask.state == NSURLSessionTaskStateRunning)
        [sessionDataTask cancel];
}

- (void)loadStoreDetailFromServer
{
    if (_dictStore)
    {
        [self cancelRequest];
        
        [[APIRequest request] storeDetailWithStoreID:[_dictStore stringValueForJSON:@"store_id"] completed:^(id responseObject, NSError *error)
         {
             if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
             {
                 _dictStore = [responseObject valueForKey:CJsonData];
                 [self setSoreDetailData:_dictStore];
             }
         }];
    }
}

- (void)loadOfferList
{
    iOffset = 0;
    [self loadStoreOfferListFromServer];
}

- (void)loadStoreOfferListFromServer
{
    if (_dictStore)
    {
        [self cancelRequest];
        
        [[APIRequest request] storeDetailOfferListWithStoreID:[_dictStore stringValueForJSON:@"store_id"] andOffset:iOffset  completed:^(id responseObject, NSError *error)
         {
             if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
             {
                 if (iOffset == 0)
                     [arrStoreDetail removeAllObjects];
                 
                 NSArray *arrData = [responseObject valueForKey:CJsonData];
                 
                 if (arrData.count > 0)
                 {
                     [arrStoreDetail addObjectsFromArray:arrData];
                     iOffset = [responseObject intForKey:CJsonNewOffset];
                     [tblVStoreDetail reloadData];
                 }
             }
             
             [self setNoDataWithMessage:[responseObject stringValueForJSON:CJsonMessage]];
             
         }];
    }
}

- (void)loadStoreReviewListFromServer
{
    if (_dictStore)
    {
        [[APIRequest request] storeDetailReviewListWithStoreID:[_dictStore stringValueForJSON:@"store_id"] andOffset:iOffset completed:^(id responseObject, NSError *error)
         {
             if (responseObject && !error)
             {
                 if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
                 {
                     if (iOffset == 0)
                         [arrStoreDetail removeAllObjects];
                     
                     NSArray *arrData = [responseObject valueForKey:CJsonData];
                     
                     if (arrData.count > 0)
                     {
                         [arrStoreDetail addObjectsFromArray:arrData];
                         iOffset = [responseObject intForKey:CJsonNewOffset];
                         fAverageRate = [[responseObject stringValueForJSON:@"over_all_rating"] floatValue];
                         headerStoreDetail.viewRate.rating = fAverageRate;
                         [tblVStoreDetail reloadData];
                     }
                 }
             }
             
             [self setNoDataWithMessage:[responseObject stringValueForJSON:CJsonMessage]];
             
         }];
    }
}

- (void)setSoreDetailData:(NSDictionary *)dictData
{
    if (dictData)
    {
        [headerStoreDetail.imgVStore sd_setImageWithURL:[NSURL URLWithString:[_dictStore stringValueForJSON:@"store_image"]] placeholderImage:nil];
        
        headerStoreDetail.viewRate.rating = [[_dictStore stringValueForJSON:@"store_average_rate"] floatValue];
        
        headerStoreDetail.lblRate.text = [NSString stringWithFormat:@"(%.1f/5)", [[_dictStore stringValueForJSON:@"store_average_rate"] floatValue]];
        
        if ([[_dictStore numberForJson:@"store_type"] boolValue])   // .... WebStore
        {
            headerStoreDetail.lblInstore.hidden = YES;
            [headerStoreDetail.btnDistance setImage:[UIImage imageNamed:@"detail_web"] forState:UIControlStateNormal];
            [headerStoreDetail.btnDistance setTitle:@"" forState:UIControlStateNormal];
        }
        else    // .... InStore
        {
            headerStoreDetail.lblInstore.hidden = NO;
            [headerStoreDetail.btnDistance setTitle:[NSString stringWithFormat:@"%@ KM", [_dictStore stringValueForJSON:@"store_distance"]] forState:UIControlStateNormal];
        }
        
        
        if ([[_dictStore numberForJson:@"store_type"] boolValue])
        {
            [headerStoreDetail.btnDistance touchUpInsideClicked:^{
               
                [appDelegate openUrlInWebviewWithURL:[_dictStore stringValueForJSON:@"store_website"]];
                
            }];
        }
        
        
        [headerStoreDetail.btnTotalOffers setTitle:[NSString stringWithFormat:@"%@ Offers", [_dictStore stringValueForJSON:@"offers_count"]] forState:UIControlStateNormal];
        
        [headerStoreDetail.btnTotalRaffleTickets setTitle:[NSString stringWithFormat:@"%@ Raffle Ticket", [_dictStore stringValueForJSON:@"raffle_tickets_count"]] forState:UIControlStateNormal];
        
        [headerStoreDetail.btnTotalBouties setTitle:[NSString stringWithFormat:@"%@ € Hops", [_dictStore stringValueForJSON:@"hops_count"]] forState:UIControlStateNormal];
    }
}

- (void)setNoDataWithMessage:(NSString *)message
{
    if (arrStoreDetail.count == 0)
    {
        [lblNoData setHidden:NO];
        [tblVStoreDetail setScrollEnabled:NO];
        lblNoData.text = message ? message : headerStoreDetail.btnOffers.selected ? CMessageNoOffsers : CMessageNoReviews;
    }
    else
    {
        [lblNoData setHidden:YES];
        [tblVStoreDetail setScrollEnabled:YES];
    }
}







# pragma mark
# pragma mark - UITableview Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (headerStoreDetail.btnOffers.selected || headerStoreDetail.btnReviews.selected)
        return arrStoreDetail.count;
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{   
    if (headerStoreDetail.btnOffers.selected)   //..... OFFER TAB
    {
        MIDetailOfferTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MIDetailOfferTableViewCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSDictionary *dictOffer = arrStoreDetail[indexPath.row];
        
        
        // ..... Set Data .....
        
        [cell.imgVOffer sd_setImageWithURL:[NSURL URLWithString:[dictOffer stringValueForJSON:@"offer_image"]] placeholderImage:nil];
        cell.lblOfferName.text = [dictOffer stringValueForJSON:@"offer_title"];
        
        
        // ..... Load More .....
        
        if (indexPath.row == (arrStoreDetail.count - 1))
            [self loadStoreOfferListFromServer];
        
        return cell;
    }
    else if (headerStoreDetail.btnDetails.selected)     //..... DETAILS TAB
    {
        MIDetailStoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MIDetailStoreTableViewCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        // ..... Set Data .....
        
        cell.lblStoreTime.text = [_dictStore stringValueForJSON:@"store_open_close_time"];
        cell.lblDescription.text = [_dictStore stringValueForJSON:@"store_description"];
        cell.lblStoreAddress.text = [_dictStore stringValueForJSON:@"store_address"];
        
        cell.dictStoreDetails = _dictStore;
        
        
        [cell.btnMap touchUpInsideClicked:^{

            if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]])
            {
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps-x-callback://?saddr=%f,%f&daddr=%f,%f",appDelegate.current_lat, appDelegate.current_long, [_dictStore doubleForKey:@"store_latitude"], [_dictStore doubleForKey:@"store_longitude"]]];
                
                if (![[UIApplication sharedApplication] canOpenURL:url])
                {
                    NSLog(@"Invalid location details");
                }
                else
                {
                    [[UIApplication sharedApplication] openURL:url];
                }
            }
            else
            {
                NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f", appDelegate.current_lat,appDelegate.current_long,[_dictStore doubleForKey:@"store_latitude"],[_dictStore doubleForKey:@"store_longitude"]]];
                
                if (![[UIApplication sharedApplication] canOpenURL:URL])
                {
                    [CustomAlertView iOSAlert:@"" withMessage:@"Routes could not be found for this address" onView:self];
                }
                else
                {
                    [[UIApplication sharedApplication] openURL:URL];
                }
            }
            
        }];
        
        return cell;
    }
    else    //..... REVIEW TAB
    {
        MIDetailReviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MIDetailReviewTableViewCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        // ..... Set Data .....
        
        NSDictionary *dictReview = [arrStoreDetail objectAtIndex:indexPath.row];
        
        [cell.imgVUserProfile sd_setImageWithURL:[NSURL URLWithString:[dictReview stringValueForJSON:@"user_profile_image"]] placeholderImage:nil];
        
        cell.lblUserName.text = [dictReview stringValueForJSON:@"user_name"];
        
        cell.viewRate.rating = [[dictReview stringValueForJSON:@"user_rating"] floatValue];
        
        cell.lblRate.text = [NSString stringWithFormat:@"(%.1f/5)",[[dictReview stringValueForJSON:@"user_rating"] floatValue]];
        
        cell.lblReview.text = [dictReview stringValueForJSON:@"review_message"];
        
        cell.viewAverageRating.rating = fAverageRate;
        
        [cell.viewAverageRate hideByHeight:(indexPath.row == 0) ? NO : YES];
        
        cell.lblAverageRating.text = [NSString stringWithFormat:@"(%.1f/5)",fAverageRate];
        
        
        // ..... Load More .....
        
        if (indexPath.row == (arrStoreDetail.count - 1))
            [self loadStoreReviewListFromServer];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (headerStoreDetail.btnOffers.selected)
    {
        MIStoreOfferPopup *storeOffserPopup = [MIStoreOfferPopup customStoreOfferPopup];
        
        
        NSDictionary *dictStoreOffer = arrStoreDetail[indexPath.row];
        
        
        // ..... Set Data .....
        
        storeOffserPopup.lblOfferTitle.text = [dictStoreOffer stringValueForJSON:@"offer_title"];
        
        [storeOffserPopup.imgVOffer sd_setImageWithURL:[NSURL URLWithString:[dictStoreOffer stringValueForJSON:@"offer_image"]] placeholderImage:nil];
        
        storeOffserPopup.txtVOfferDescription.text = [dictStoreOffer stringValueForJSON:@"offer_description"];
        
        storeOffserPopup.lblValidUpto.text = [dictStoreOffer stringValueForJSON:@"offer_valid_date"];
        
        storeOffserPopup.btnOk.hidden = storeOffserPopup.viewBottom.hidden = YES;
        
        
        if ([[_dictStore numberForJson:@"store_type"] boolValue])   // ..... Webstore .....
            storeOffserPopup.viewBottom.hidden = NO;
        else    // ..... Instore .....
            storeOffserPopup.btnOk.hidden = NO;
        
        
        
        //..... Click Events .....
        
        [storeOffserPopup.btnOk touchUpInsideClicked:^{     // ..... OK .....
           
            [self dismissPopUp:storeOffserPopup];
            
        }];
        
        [storeOffserPopup.btnCancel touchUpInsideClicked:^{     // ..... Cancel .....
            
            [self dismissPopUp:storeOffserPopup];
            
        }];
        
        [storeOffserPopup.btnBuy touchUpInsideClicked:^{    // ..... Buy .....
            
            [self dismissPopUp:storeOffserPopup];
            [appDelegate openUrlInWebviewWithURL:[_dictStore stringValueForJSON:@"store_website"]];
            
        }];
        
        [self presentPopUp:storeOffserPopup from:PresentTypeCenter shouldCloseOnTouchOutside:YES];
    }
}





# pragma mark
# pragma mark - Scrollview Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat collVOffset = CViewHeight(headerStoreDetail) - CViewHeight(headerStoreDetail.viewBottom);
    
    if (scrollView.contentOffset.y > collVOffset)
    {
        if (headerStoreDetail.viewTab.superview != self.view)
        {
            [headerStoreDetail.viewTab removeFromSuperview];
            [self.view addSubview:headerStoreDetail.viewTab];
            
            [self.view addConstraint:[NSLayoutConstraint constraintWithItem:headerStoreDetail.viewTab attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1 constant:0]];
            [self.view addConstraint:[NSLayoutConstraint constraintWithItem:headerStoreDetail.viewTab attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
            [self.view addConstraint:[NSLayoutConstraint constraintWithItem:headerStoreDetail.viewTab attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeRight multiplier:1 constant:0]];
        }
    }
    else
    {
        if (headerStoreDetail.viewTab.superview != headerStoreDetail)
        {
            [headerStoreDetail.viewTab removeFromSuperview];
            [headerStoreDetail addSubview:headerStoreDetail.viewTab];
            
            [headerStoreDetail addConstraint:[NSLayoutConstraint constraintWithItem:headerStoreDetail.viewTab attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:headerStoreDetail attribute:NSLayoutAttributeLeft multiplier:1 constant:0]];
            [headerStoreDetail addConstraint:[NSLayoutConstraint constraintWithItem:headerStoreDetail.viewTab attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:headerStoreDetail attribute:NSLayoutAttributeBottom multiplier:1 constant:-10]];
            [headerStoreDetail addConstraint:[NSLayoutConstraint constraintWithItem:headerStoreDetail.viewTab attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:headerStoreDetail attribute:NSLayoutAttributeRight multiplier:1 constant:0]];
        }
    }
}

@end
