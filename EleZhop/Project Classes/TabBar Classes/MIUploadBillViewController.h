//
//  MIUploadBillViewController.h
//  EleZhop
//
//  Created by mac-0005 on 6/15/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIUploadBillViewController : ParentViewController
{
    IBOutlet UITextField *txtStoreSearch;
    IBOutlet UITableView *tblVStoreList;
    IBOutlet UIButton *btnOtherShop;
    IBOutlet UILabel *lblNoData;
}


@property (nonatomic, assign) BOOL isFromSearch;
@property (nonatomic, assign) BOOL isBillUploaded;
@property (nonatomic, strong) NSData *imgData;

- (IBAction)btnOtherShopClicked:(id)sender;

@end
