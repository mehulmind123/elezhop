//
//  MIUploadBillViewController.m
//  EleZhop
//
//  Created by mac-0005 on 6/15/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MIUploadBillViewController.h"
#import "MICaptureBillViewController.h"
#import "MIRateUsViewController.h"
#import "MIStoreDetailViewController.h"

#import "MIUploadPopUp.h"
#import "MICongratulationView.h"

#import "MIStoreTableViewCell.h"



#define sortByDistance      1


@interface MIUploadBillViewController ()
{
    NSMutableArray *arrStoreList;
    
    NSInteger iOffset;
    
    NSURLSessionDataTask *sessionDataTask;
}
@end

@implementation MIUploadBillViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




# pragma mark
# pragma mark - General Method

- (void)initialize
{
    [self.navigationController setNavigationBarHidden:NO];
    
   self.screenName = self.title = _isFromSearch ? @"Stores" : @"Upload Bill";
    
    txtStoreSearch.layer.cornerRadius = CViewHeight(txtStoreSearch) / 2;
    [txtStoreSearch setLeftImage:[UIImage imageNamed:@"search"] withSize:CGSizeMake(35, 15)];
    [txtStoreSearch setPlaceHolderColor:ColorLightGray_B5B4B1];
    [txtStoreSearch addTarget:self action:@selector(textfieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    
    [btnOtherShop hideByHeight:NO];
    btnOtherShop.layer.cornerRadius = 3;
    
    if (_isFromSearch)
    {
        //..... REMOVE SPACE BETWEEN TABLE AND TABBAR
        
        [btnOtherShop setConstraintConstant:0 toAutoLayoutEdge:ALEdgeBottom toAncestor:YES];
        [btnOtherShop hideByHeight:YES];
    }
    
    
    //..... REGISTER CELL
    
    [tblVStoreList registerNib:[UINib nibWithNibName:@"MIStoreTableViewCell" bundle:nil] forCellReuseIdentifier:@"MIStoreTableViewCell"];
    tblVStoreList.contentInset = UIEdgeInsetsMake(0, 0, 5, 0);
    
    
    //..... API CALL
    
    arrStoreList = [NSMutableArray new];
    [self loadAPI];
}

# pragma mark
# pragma mark - Action Events

- (IBAction)btnOtherShopClicked:(id)sender
{
    btnOtherShop.selected = YES;
    
    MIUploadPopUp *uploadPopup = [MIUploadPopUp customUploadPopup];
    
    
    //..... CLICK EVENTS
    
    [uploadPopup.btnCancel touchUpInsideClicked:^{
        [self dismissPopUp:uploadPopup];
    }];
    
    [uploadPopup.btnSubmit touchUpInsideClicked:^{
        
        if (![uploadPopup.txtCommon.text isBlankValidationPassed])
            [UIAlertController alertViewWithOneButtonWithTitle:@"" withMessage:CMessageBlankStoreName buttonTitle:@"Ok" withDelegate:nil];
        else
        {
            [self dismissPopUp:uploadPopup];
            [self displayUploadBillAmountPopupForOtherShop:uploadPopup.txtCommon.text];
        }
        
    }];
    
    [self.navigationController presentPopUp:uploadPopup from:Is_iPhone_5 ? PresentTypeCustom : PresentTypeCenter shouldCloseOnTouchOutside:YES];
}





# pragma mark
# pragma mark - API Method

- (void)loadAPI
{
    iOffset = 0;
    [self loadStoreListFromServerWithSearchText:@""];
}

- (void)cancelRequest
{
    if (sessionDataTask && sessionDataTask.state == NSURLSessionTaskStateRunning)
        [sessionDataTask cancel];
}

- (void)loadStoreListFromServerWithSearchText:(NSString *)searchTxt
{
    [self cancelRequest];   // ..... CANCEL API REQUEST
    
    sessionDataTask = [[APIRequest request] storeListWithCategoryID:@"" andOffset:iOffset andSearchText:searchTxt andSortBY:sortByDistance completed:^(id responseObject, NSError *error)
    {
        if (responseObject && !error)
        {
            if ([[APIRequest request] isJSONDataValidWithResponse:responseObject])
            {
                if (iOffset == 0)
                    [arrStoreList removeAllObjects];
                
                [lblNoData setHidden:YES];
                
                NSArray *arrData = [responseObject valueForKey:CJsonData];
                
                if (arrData.count > 0)
                {
                    [arrStoreList addObjectsFromArray:arrData];
                    iOffset = [responseObject intForKey:CJsonNewOffset];
                    [tblVStoreList reloadData];
                }
            }
            
            if (arrStoreList.count == 0)
            {
                [tblVStoreList setHidden:YES];
                [lblNoData setHidden:NO];
            }
            else
            {
                [tblVStoreList setHidden:NO];
                [lblNoData setHidden:YES];
            }
        }
    }];
}

- (void)clearStoreData
{
    iOffset = 0;
    [arrStoreList removeAllObjects];
    [tblVStoreList reloadData];
}

-(void)textfieldDidChange:(UITextField *)textField
{
    [self clearStoreData];
    [self loadStoreListFromServerWithSearchText:textField.text];
}






# pragma mark
# pragma mark - UITableview Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrStoreList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MIStoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MIStoreTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    //..... Set Data .....
    
    NSDictionary *dictStore = [arrStoreList objectAtIndex:indexPath.row];
    
    [cell.imgVStore sd_setImageWithURL:[NSURL URLWithString:[dictStore stringValueForJSON:@"store_image"]] placeholderImage:nil];
    cell.lblStoreName.text = [[dictStore stringValueForJSON:@"store_name"] capitalizedString];
    cell.viewRate.rating = [[dictStore stringValueForJSON:@"store_average_rate"] floatValue];
    cell.lblRate.text = [NSString stringWithFormat:@"(%.1f/5)", [[dictStore stringValueForJSON:@"store_average_rate"] floatValue]];
    cell.lblStoreType.text = [[dictStore numberForJson:@"store_type"] boolValue] ? @"Web" : @"Instore";
    cell.lblStoreType.backgroundColor = [[dictStore numberForJson:@"store_type"] boolValue] ? ColorGreen_299712 : ColorBlue_3184BC;
    
    cell.btnDistance.hidden = NO;
    
    if ([[dictStore numberForJson:@"store_type"] boolValue])
        cell.btnDistance.hidden = YES;
    else
        [cell.btnDistance setTitle:[NSString stringWithFormat:@"%@ KM", [dictStore stringValueForJSON:@"store_distance"]] forState:UIControlStateNormal];
    
    [cell.btnOffers setTitle:[NSString stringWithFormat:@"%@ Offers", [dictStore stringValueForJSON:@"offers_count"]] forState:UIControlStateNormal];
    
    [cell.btnRuffleTicket setTitle:[NSString stringWithFormat:@"%@ Raffle Ticket", [dictStore stringValueForJSON:@"raffle_tickets_count"]] forState:UIControlStateNormal];
    
    [cell.btnBounties setTitle:[NSString stringWithFormat:@"%@ € Hops", [dictStore stringValueForJSON:@"hops_count"]] forState:UIControlStateNormal];
    
    
    
    // ..... Load More
    
    if (indexPath.row == (arrStoreList.count - 1))
        [self loadStoreListFromServerWithSearchText:txtStoreSearch.text];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [appDelegate resignKeyboard];
    
    if (_isFromSearch)
    {
        MIStoreDetailViewController *storeDetailVC = [[MIStoreDetailViewController alloc] initWithNibName:@"MIStoreDetailViewController" bundle:nil];
        storeDetailVC.showTabBar = YES;
        storeDetailVC.dictStore = [arrStoreList objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:storeDetailVC animated:YES];
    }
    else
    {
        btnOtherShop.selected = NO;
        [self displayUploadBillAmountPopup:arrStoreList[indexPath.row]];
    }
}






# pragma mark
# pragma mark - Popup

- (void)displayUploadBillAmountPopupForOtherShop:(NSString *)storeName
{
    MIUploadPopUp *uploadPopup = [MIUploadPopUp customUploadPopup];
    
    uploadPopup.lblTitle.text = @"Enter the bill amount";
    uploadPopup.lblShopName.text = storeName;
    [uploadPopup.txtCommon setPlaceholder:@"Bill Amount"];
    [uploadPopup.txtCommon setKeyboardType:UIKeyboardTypeDecimalPad];
    
    
    //..... Click Events .....
    
    [uploadPopup.btnCancel touchUpInsideClicked:^{
        [self dismissPopUp:uploadPopup];
    }];
    
    [uploadPopup.btnSubmit touchUpInsideClicked:^{
        
        if (![uploadPopup.txtCommon.text isBlankValidationPassed])
            [self displayBlankBillAmountAlert];
        else
        {
            [self dismissPopUp:uploadPopup];
            
            [[APIRequest request] uploadBill:_imgData andStoreID:@"" andStoreName:storeName andBillAmount:uploadPopup.txtCommon.text andRate:0 andReview:@"" completed:^(id responseObject, NSError *error)
             {
                 if ([[APIRequest request] isJSONStatusValidWithResponse:responseObject])
                 {
                     [self displayCongratulationPopup:[responseObject valueForKey:CJsonData]];
                 }
             }];
        }
        
    }];
    
    [self presentPopUp:uploadPopup from:Is_iPhone_5 ? PresentTypeCustom : PresentTypeCenter shouldCloseOnTouchOutside:YES];
}

- (void)displayUploadBillAmountPopup:(NSDictionary *)dictStore
{
    MIUploadPopUp *uploadPopup = [MIUploadPopUp customUploadPopup];
    
    uploadPopup.lblTitle.text = @"Enter the bill amount";
    uploadPopup.lblShopName.text = [[dictStore stringValueForJSON:@"store_name"] capitalizedString];
    [uploadPopup.txtCommon setPlaceholder:@"Bill Amount"];
    [uploadPopup.txtCommon setKeyboardType:UIKeyboardTypeDecimalPad];
    
    //..... Click Events .....
    
    [uploadPopup.btnCancel touchUpInsideClicked:^{
        [self dismissPopUp:uploadPopup];
    }];
    
    [uploadPopup.btnSubmit touchUpInsideClicked:^{
        
        if (![uploadPopup.txtCommon.text isBlankValidationPassed])
            [self displayBlankBillAmountAlert];
        else
        {
            [self dismissPopUp:uploadPopup];
            
            MIRateUsViewController *rateUsVC = [[MIRateUsViewController alloc] initWithNibName:@"MIRateUsViewController" bundle:nil];
            [rateUsVC setShowTabBar:YES];
            rateUsVC.dictData = @{@"bill_image":_imgData,
                                  @"store_id":[dictStore stringValueForJSON:@"store_id"],
                                  @"store_name":uploadPopup.lblShopName.text,
                                  @"bill_amount":uploadPopup.txtCommon.text};
            [self.navigationController pushViewController:rateUsVC animated:YES];
        }
        
    }];
    
    [self.navigationController presentPopUp:uploadPopup from:Is_iPhone_5 ? PresentTypeCustom : PresentTypeCenter shouldCloseOnTouchOutside:YES];
}

- (void)displayCongratulationPopup:(NSDictionary *)dictResponse
{
    if (dictResponse)
    {
        MICongratulationView *congratulationPopup = [MICongratulationView customCongratulationPopup];
        
        congratulationPopup.lblCongratulationMessage.text = [dictResponse stringValueForJSON:CJsonMessage];
        
        congratulationPopup.imgVBG.hidden = YES;
        
        
        //..... Click Events .....
        
        [congratulationPopup.btnOk touchUpInsideClicked:^{
            
            [self dismissPopUp:congratulationPopup];
            [self.navigationController popToRootViewControllerAnimated:YES];
            
        }];
        
        [self presentPopUp:congratulationPopup from:PresentTypeCenter shouldCloseOnTouchOutside:NO];
    }
}

- (void)displayBlankBillAmountAlert
{
    [UIAlertController alertViewWithOneButtonWithTitle:@"" withMessage:CMessageBlankBillAmount buttonTitle:@"Ok" withDelegate:nil];
}

@end
