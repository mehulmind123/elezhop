//
//  MITutorialViewController.h
//  EleZhop
//
//  Created by mac-0005 on 7/13/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MITutorialViewController : UIViewController
{
    IBOutlet UICollectionView *collVTutorial;
    
    IBOutlet UIPageControl *pageControl;
    
    IBOutlet MIGenericButton *btnSkip;
    IBOutlet MIGenericButton *btnNext;
}


- (IBAction)btnSkipClicked:(id)sender;
- (IBAction)btnNextClicked:(id)sender;
- (IBAction)pageChange:(id)sender;

@end
