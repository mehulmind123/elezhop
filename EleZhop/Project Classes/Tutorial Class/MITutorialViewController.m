//
//  MITutorialViewController.m
//  EleZhop
//
//  Created by mac-0005 on 7/13/17.
//  Copyright © 2017 Mac-0005. All rights reserved.
//

#import "MITutorialViewController.h"
#import "MILoginViewController.h"

#import "MITutorialCollectionViewCell.h"

@interface MITutorialViewController ()
{
    NSArray *arrTutorialData;
}
@end

@implementation MITutorialViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}






# pragma mark
# pragma mark - General Method

- (void)initialize
{
    arrTutorialData = @[@{@"image":@"tutorial_discover.jpg",
                          @"title":@"Discover",
                          @"icon":@"discover",
                          @"desc":@"Authentic Corporate data, summaries and much more at your fingertips."},
                        @{@"image":@"tutorial_shop.jpg",
                          @"title":@"Visit Shop & Shopping",
                          @"icon":@"shop",
                          @"desc":@"Get an insight into Company Structure, Management, Shareholders, Key Financials, Indebtedness, Credit Rating, Auditors and network of Enterprenuers."},
                        @{@"image":@"tutorial_upload_bill.jpg",
                          @"title":@"Upload Bill",
                          @"icon":@"tutorial_upload_bill_icon",
                          @"desc":@"Add to track list, get notified and stay updated about the Company/Director"},
                        @{@"image":@"tutorial_earn_reward.jpg",
                          @"title":@"Earn Rewards",
                          @"icon":@"earn_reward",
                          @"desc":@"Download MCA documents, buy snapshots and update exising company snapshots on Pay-as-you-go basis."},
                        ];
    
    
    //..... Register Cell .....
    
    [collVTutorial registerNib:[UINib nibWithNibName:@"MITutorialCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MITutorialCollectionViewCell"];
}





# pragma mark
# pragma mark - UICollectionView Delegate & Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrTutorialData.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CScreenWidth, CScreenHeight - CViewHeight(pageControl) - CViewHeight(btnNext));
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"MITutorialCollectionViewCell";
    MITutorialCollectionViewCell *cell = [collVTutorial dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    
    //..... Set Data .....
    
    NSDictionary *dictTutorial = [arrTutorialData objectAtIndex:indexPath.row];
    
    cell.imgVScreen.image = [UIImage imageNamed:[dictTutorial valueForKey:@"image"]];
    cell.imgVSceenIcon.image = [UIImage imageNamed:[dictTutorial valueForKey:@"icon"]];
    cell.lblScreenTitle.text = [dictTutorial valueForKey:@"title"];
    cell.lblScreenDescription.text = [dictTutorial valueForKey:@"desc"];
    
    return cell;
}





# pragma mark
# pragma mark - UIScrollview Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
{
    int page = round(collVTutorial.contentOffset.x / collVTutorial.bounds.size.width);
    [pageControl setCurrentPage:page];
}






# pragma mark
# pragma mark - Action Events

- (IBAction)btnSkipClicked:(id)sender
{
    [CUserDefaults setValue:[NSNumber numberWithBool:YES] forKey:UserDefaultTutorialScreen];
    [CUserDefaults synchronize];
    
    [appDelegate signinUser];
}

- (IBAction)btnNextClicked:(id)sender
{
    NSIndexPath *index = [collVTutorial indexPathForCell:[collVTutorial.visibleCells firstObject]];
    
    if (index.row > 2)
    {
        [CUserDefaults setValue:[NSNumber numberWithBool:YES] forKey:UserDefaultTutorialScreen];
        [CUserDefaults synchronize];
        
        [appDelegate signinUser];
        
        return;
    }
    
    [collVTutorial scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:index.item + 1 inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
}

- (IBAction)pageChange:(UIPageControl *)sender
{
    [collVTutorial scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:sender.currentPage inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
}

@end
